SCA overview:

Instead of plain characters, the program works with segments. A segment is one or more characters and represents an indivisable unit.
What this means in practice is that if you want to use digraphs or combining diacritics, you have to define them as segments. Characters that are not parts of either don't need to be defined as segments, but often implicitly are.
Note that the orthography has to be unambiguously segmentable. If, for example, <ng> is /ŋ/ and <gh> is /ɣ/ and <ngh> can be either /ŋh/ or /nɣ/, sound changes including either or both won't be executed correctly.
Groups of segments can be defined for usage in soundchanges, e.g. to delete word-final vowels:
V = a o u e i
V > / _#
Ad-hoc groups are not supported yet, so commands like {a b c} > {d e f} are invalid.

Most commands take the following form:

(source) > (result) (#order) (/condition) (/direction)

 - source: a string of segments and groups to be replaced. If there's no source, the segments in result are generated at the matching place.
 - result: a string of segments to replace the source or be generated. If there is no result, the source is deleted.
 - condition: the condition, which must be met for the sound change to occur. If there's no condition, the sound change always occurs. The condition has to contain _, designating the place of the source in the word.
 ---- The condition may contain *, which matches zero or more of the preceding segment, #, which matches word boundary, or (p|t|k|...), which matches a single segment of the following: p, t, k, ...
 - direction: All segments in the word are tested simultaneously for the condition. This means that l > r / _al would change lalala to rarala and not larala. To get the sometimes more intuitive result, it should be specified if the sound change should affect only odd or only even matches and if it should be executed left-to-right or right-to-left. All this is done with one of /ltr even, /ltr odd, /rtl even and /rtl odd.

Shortcuts for adding affixes also exist:

-suffix (/condition)
prefix- (/condition)
circum-fix (/condition)
circum- -fix (/condition)

Conditions here are global and don't contain an underscore, e.g. / V# adds an affix only if the word ends in a vowel.

Morphology overview:
 - In order to inflect some words, a couple of prerequisites have to be met:
 ---- Definition of Parts of speech and Inflection classes (in Conlangs tab) - the difference between the two is mostly that parts of speech cannot inherit from inflection classes and that no two parts of speech can be displayed in the morphology tab.
 ---- Definition of Categories and Grammemes (e.g. singular, plural, present, past) - in Conlangs tab.
 ---- Addition of a Paradigm table and an Affix slot (in Morphology tab) - paradigm tables are mostly so that finite and infinite verbal affixes don't stack on top of each other.
 ---- Next, each affix slot needs to have its categories specified.
 ---- Finally, each affix is to be entered in the empty table cells.
 - To preview how inflection looks like on a word, a word from the lexicon in the lower right can be selected.
 - To add a word in inflection, use an underscore (works only for commands containing >):
 ---- > preclitic_ / #_
 ---- > _postclitic / _#

Known issues:
 - The double quote character (") is not supported as input anywhere. Entering it may cause bugs or at best it will be converted to a single quote.
 - The dropdown lists in the conlang settings are not automatically updated when creating or deleting a part of speech.
 - If two morphology tables are named the same way, derivation will be buggy.
 - Import functionality is still finicky - if there are empty lines, or missing columns in a line, import will fail.
 ---- If a word type name/abbreviation is not described in the conlangs tab, import functionality may import erroneous data despite its best efforts to report malformed input and errors.
 - Deletion of word types in a derived conlang makes usage of the diachronics panel buggy for that conlang.
 - Sorting in diachronics isn't implemented.
 - Searching in diachronics by column 'word' doesn't relate corresponding rows (it's independent in both tables).
 - Inherent categories are not fully implemented - for e.g. noun classes use different part of speech subtypes instead.
 - Affix overwriting for part of speech subtypes isn't implemented - use different affix slots for each subtype instead.
 - Rearranging tables and slots in subtypes is not fully implemented / buggy.
 - Sometimes the diachronic sound changes textfield has to be modified in order for the derivations table to refresh (adding a space suffices).
 - Only individual segments should be defined in groups - e.g. if this is defined: VC = ak ek at et, none of <a e k t> will be matched anymore.
 - Once a daughterlang is created, there is no way to propagate any changes from the protolang other than lexical changes.
 - Sometimes, when adding new lines in diachronics sound change text area, elements on top get out of view. The workaround so far is to select some other tab and then return to diachroncis tab
 - When adding morphemes in morphology, tables of derived word types are refreshed only on reloading the tab.
 - Categories containing a single grammeme aren't supported yet.
 - Many other things aren't supported yet either, such as defective words, irregular words, holes in paradigms, etc.
