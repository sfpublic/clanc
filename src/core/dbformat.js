"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
var db_type_t;
(function (db_type_t) {
    db_type_t[db_type_t["id"] = 0] = "id";
    db_type_t[db_type_t["txt"] = 1] = "txt";
    db_type_t[db_type_t["ltxt"] = 2] = "ltxt";
    db_type_t[db_type_t["int"] = 3] = "int";
    db_type_t[db_type_t["ref"] = 4] = "ref";
    db_type_t[db_type_t["bool"] = 5] = "bool";
    db_type_t[db_type_t["MtM"] = 8] = "MtM";
})(db_type_t || (db_type_t = {}));
;
const index_p = 1;
const unique_p = 2;
const autoincrement_p = 4;
const notnull_p = 8;
const primarykey_p = 16;
const notnulllength_p = 32;
const ondeletecascade_p = 64;
const ondeletenull_p = 128;
const positive_p = 256;
const ordered_p = 512;
const id_p = primarykey_p | index_p | unique_p | autoincrement_p | notnull_p;
const key_p = index_p | unique_p | notnull_p;
const foreign_key_p = index_p | notnull_p | ondeletecascade_p;
var aliasable;
(function (aliasable) {
    class database_column_t {
        constructor(a, c, d) { this.type = a; this.flags = c; if (d)
            this.references = d; }
    }
    aliasable.database_column_t = database_column_t;
})(aliasable || (aliasable = {}));
var database_column_t = aliasable.database_column_t;
var col = aliasable.database_column_t; //only really meant to be used in the definitions below
// name and value have to match!
// all values have to be defined as variables in TABLE_SCHEMAS below
var table_e;
(function (table_e) {
    table_e["conlang"] = "conlang";
    table_e["entry"] = "entry";
    table_e["miscdata"] = "miscdata";
    table_e["morphology"] = "morphology";
    table_e["morphophonological_rule"] = "morphophonological_rule";
    table_e["category"] = "category";
    table_e["word_type"] = "word_type";
    table_e["paradigm_table"] = "paradigm_table";
    table_e["slot"] = "slot";
    table_e["morpheme"] = "morpheme";
    table_e["grammeme"] = "grammeme";
    table_e["slot_overwritten_categories"] = "slot_overwritten_categories";
    table_e["derivational_morpheme"] = "derivational_morpheme";
})(table_e || (table_e = {}));
;
function mtmname(from, to, name) {
    if ((from).from)
        return mtmname((from).from, (from).to, (from).key);
    else
        return '_' + from + '__' + to + '__' + name;
}
function mtm(from, to, name) {
    return { from: from, to: to, key: name };
}
var entry_state_e;
(function (entry_state_e) {
    entry_state_e[entry_state_e["irregular_derivation"] = 1] = "irregular_derivation";
    entry_state_e[entry_state_e["not_up_to_date"] = 2] = "not_up_to_date";
})(entry_state_e || (entry_state_e = {}));
class TABLE_SCHEMAS {
    constructor() {
        throw "This class is only to be used statically.";
    }
}
UTL.addConst(TABLE_SCHEMAS, table_e.conlang, {
    name: table_e.conlang,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "name": new col(db_type_t.txt, key_p | unique_p | notnulllength_p),
        "descends_from": new col(db_type_t.ref, index_p | ondeletenull_p, table_e.conlang)
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.entry, {
    name: table_e.entry,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p, table_e.conlang),
        "root": new col(db_type_t.txt, index_p | notnull_p | notnulllength_p),
        "translations": new col(db_type_t.txt, index_p | notnull_p),
        "diachronic_id": new col(db_type_t.id, index_p | notnull_p),
        "notes": new col(db_type_t.txt, 0),
        "state": new col(db_type_t.int, notnull_p),
        "part_of_speech": new col(db_type_t.ref, foreign_key_p | notnull_p, table_e.word_type),
        "derivational_morpheme": new col(db_type_t.ref, index_p | ondeletenull_p, table_e.derivational_morpheme)
    },
    unique_keys: {
        'diachronic_key': ['diachronic_id', 'conlang']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.miscdata, {
    name: table_e.miscdata,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p | unique_p, table_e.conlang),
        "spokenfrom": new col(db_type_t.txt, 0),
        "spokento": new col(db_type_t.txt, 0),
        "place": new col(db_type_t.txt, 0),
        "setting": new col(db_type_t.ltxt, 0),
        "segments": new col(db_type_t.ltxt, notnull_p),
        "sound_changes": new col(db_type_t.ltxt, 0)
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.morphology, {
    name: table_e.morphology,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p | unique_p, table_e.conlang),
        "surface_rules": new col(db_type_t.ltxt, 0)
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.morpheme, {
    name: table_e.morpheme,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "slot": new col(db_type_t.ref, foreign_key_p, table_e.slot),
        "SCs": new col(db_type_t.ltxt, notnulllength_p),
        'grammemes': new col(db_type_t.MtM, 0, table_e.grammeme),
        "overwrites": new col(db_type_t.ref, index_p | ondeletecascade_p, table_e.morpheme),
        "descends_from": new col(db_type_t.ref, ondeletenull_p, table_e.morpheme)
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.morphophonological_rule, {
    name: table_e.morphophonological_rule,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p, table_e.conlang),
        "SCs": new col(db_type_t.ltxt, notnulllength_p),
        "number": new col(db_type_t.int, notnull_p)
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.word_type, {
    name: table_e.word_type,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p, table_e.conlang),
        'name': new col(db_type_t.txt, notnull_p | notnulllength_p),
        'abbreviation': new col(db_type_t.txt, 0),
        'assignable': new col(db_type_t.bool, notnull_p),
        // ^ whether words can have this inflection class, or only its subclasses,
        'part_of_speech': new col(db_type_t.bool, notnull_p),
        // ^ whether this row represent a PoS or an inflection class
        'inherent_categories': new col(db_type_t.MtM, 0, table_e.category),
        'missing_inherent_categories': new col(db_type_t.MtM, 0, table_e.category),
        'missing_tables': new col(db_type_t.MtM, 0, table_e.paradigm_table),
        'missing_slots': new col(db_type_t.MtM, 0, table_e.slot),
        'inherits_from': new col(db_type_t.ref, index_p | ondeletenull_p, table_e.word_type),
        'descends_from': new col(db_type_t.ref, ondeletenull_p, table_e.word_type)
    },
    unique_keys: {
        'name_key': ['name', 'conlang'],
        'abbreviation_key': ['abbreviation', 'conlang']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.category, {
    name: table_e.category,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p, table_e.conlang),
        'nullable': new col(db_type_t.bool, notnull_p),
        // ^ whether inflections, wordforms or individual words can lack this category
        'name': new col(db_type_t.txt, notnull_p | notnulllength_p),
    },
    unique_keys: {
        'name_key': ['name', 'conlang']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.grammeme, {
    name: table_e.grammeme,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "category": new col(db_type_t.ref, foreign_key_p, table_e.category),
        'name': new col(db_type_t.txt, notnull_p | notnulllength_p),
        'gloss': new col(db_type_t.txt, notnull_p | notnulllength_p),
        'position': new col(db_type_t.int, notnull_p | positive_p)
    },
    unique_keys: {
        'name_key': ['name', 'category'],
        'gloss_key': ['gloss', 'category']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.paradigm_table, {
    name: table_e.paradigm_table,
    cols: {
        "id": new col(db_type_t.id, id_p),
        'name': new col(db_type_t.txt, notnull_p | notnulllength_p),
        "word_type": new col(db_type_t.ref, index_p | ondeletecascade_p, table_e.word_type)
    },
    unique_keys: {
        'name_key': ['name', 'word_type']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.slot, {
    name: table_e.slot,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "paradigm_table": new col(db_type_t.ref, foreign_key_p, table_e.paradigm_table),
        'categories': new col(db_type_t.MtM, notnull_p | ordered_p, table_e.category),
        'missing_categories': new col(db_type_t.MtM, notnull_p, table_e.category),
        'weight': new col(db_type_t.int, notnull_p),
        'name': new col(db_type_t.txt, notnull_p | notnulllength_p),
        'position': new col(db_type_t.int, notnull_p | positive_p),
        'word_type': new col(db_type_t.ref, index_p | ondeletecascade_p, table_e.word_type)
    },
    unique_keys: {
        'name_key': ['name', 'word_type', 'paradigm_table']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.slot_overwritten_categories, {
    name: table_e.slot_overwritten_categories,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "slot": new col(db_type_t.ref, foreign_key_p, table_e.slot),
        "category": new col(db_type_t.ref, foreign_key_p, table_e.category),
        'word_type': new col(db_type_t.ref, foreign_key_p, table_e.word_type),
        'present': new col(db_type_t.bool, notnull_p)
    },
    unique_keys: {
        'ref_key': ['slot', 'word_type', 'category']
    }
});
UTL.addConst(TABLE_SCHEMAS, table_e.derivational_morpheme, {
    name: table_e.derivational_morpheme,
    cols: {
        "id": new col(db_type_t.id, id_p),
        "conlang": new col(db_type_t.ref, foreign_key_p, table_e.conlang),
        "SCs": new col(db_type_t.ltxt, notnulllength_p),
        'name': new col(db_type_t.txt, notnull_p | notnulllength_p),
        "template": new col(db_type_t.ltxt, 0),
        'from_word_types': new col(db_type_t.MtM, 0, table_e.word_type),
        'to_word_types': new col(db_type_t.MtM, 0, table_e.word_type),
    },
    unique_keys: {
        'name_key': ['name', 'conlang']
    }
});
const dictionary_sch = {
    "root": { name: "word", width: 200 },
    "part_of_speech": { name: "type", width: 80 },
    "translations": { name: "meaning", width: 400 },
    "notes": { name: "notes", width: 400 },
    "id": { name: "", width: 0, hidden: true },
    "state": { name: "", width: 0, hidden: true },
};
const diachronics_sch = {
    "root": { name: "word", width: 200 },
    "part_of_speech": { name: "type", width: 80 },
    "translations": { name: "meaning", width: 400 },
    "diachronic_id": { name: "", width: 0, hidden: true },
    "id": { name: "", width: 0, hidden: true },
};
const morphology_sch = {
    "root": { name: "word", width: 100 },
    "part_of_speech": { name: "type", width: 40 },
    "translations": { name: "meaning", width: 160 },
    "id": { name: "", width: 0, hidden: true },
};
const DBformat = {
    [table_e.conlang]: TABLE_SCHEMAS.conlang,
    [table_e.entry]: TABLE_SCHEMAS.entry,
    [table_e.miscdata]: TABLE_SCHEMAS.miscdata,
    [table_e.morphology]: TABLE_SCHEMAS.morphology,
    [table_e.morpheme]: TABLE_SCHEMAS.morpheme,
    [table_e.morphophonological_rule]: TABLE_SCHEMAS.morphophonological_rule,
    [table_e.word_type]: TABLE_SCHEMAS.word_type,
    [table_e.category]: TABLE_SCHEMAS.category,
    [table_e.grammeme]: TABLE_SCHEMAS.grammeme,
    [table_e.paradigm_table]: TABLE_SCHEMAS.paradigm_table,
    [table_e.slot]: TABLE_SCHEMAS.slot,
    [table_e.slot_overwritten_categories]: TABLE_SCHEMAS.slot_overwritten_categories,
    [table_e.derivational_morpheme]: TABLE_SCHEMAS.derivational_morpheme
};
const DBMTMs = {
    'wt_inherent_categories': mtm(table_e.word_type, table_e.category, 'inherent_categories'),
    'wt_missing_inherent_categories': mtm(table_e.word_type, table_e.category, 'missing_inherent_categories'),
    'm_grammemes': mtm(table_e.morpheme, table_e.grammeme, 'grammemes'),
    'slot_categories': mtm(table_e.slot, table_e.category, 'categories'),
    'slot_missing_categories': mtm(table_e.slot, table_e.category, 'missing_categories'),
    'wt_missing_tables': mtm(table_e.word_type, table_e.paradigm_table, 'missing_tables'),
    'wt_missing_slots': mtm(table_e.word_type, table_e.slot, 'missing_slots'),
    'dm_from_word_types': mtm(table_e.derivational_morpheme, table_e.word_type, 'from_word_types'),
    'dm_to_word_types': mtm(table_e.derivational_morpheme, table_e.word_type, 'to_word_types')
};
//# sourceMappingURL=dbformat.js.map