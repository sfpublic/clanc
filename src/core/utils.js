"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
;
var g;
(function (g) {
    g["error"] = "error";
    g["UIerror"] = "UIerror";
    g["warning"] = "warning";
    g["info"] = "info";
    g["debug"] = "debug";
    g["trace"] = "trace";
    g["DBquery"] = "DBquery";
    g["SCA"] = "SCA";
})(g || (g = {}));
function arjoin(obj, separator = '') {
    return Object.values(obj).join(separator);
}
function arr(obj) {
    return Object.values(obj);
}
function sarr(obj) {
    return Object.values(obj);
}
class UTL {
    constructor() {
        throw "This class is only to be used statically.";
    }
    static newHTMLId() {
        return UTL.counter++;
    }
    static noSpaces(arg) {
        return UTL.replaceAll(arg.replace(/[\r\n\t\f\v]/g, ' '), /  /, ' ').trim();
    }
    static removeAllSpaces(arg) {
        return arg.replace(/[\r\n\t\f\v ]/g, '');
    }
    static itIsALetter(char) {
        return char.toUpperCase() != char.toLowerCase();
    }
    static removelastcomma(str) {
        let commaindex = str.lastIndexOf(",");
        if (commaindex == -1)
            return str;
        else
            return str.slice(0, commaindex) + str.slice(commaindex + 1);
    }
    static base64Decode(str, encoding = 'utf-8') {
        let bytes = base64js.toByteArray(str);
        return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes);
    }
    static isIterable(obj) {
        if (obj == null)
            return false;
        return typeof obj[Symbol.iterator] === 'function';
    }
    static randomstring(length) {
        let ret = '';
        let chars = 'abcdefghijklmnopqrstuvwxyz';
        let numchars = chars.length;
        for (let i = 0; i < length; i++)
            ret += chars.charAt(Math.floor(Math.random() * numchars));
        return ret;
    }
    static randomint(upto) {
        return Math.floor(Math.random() * upto);
    }
    static generateNewId() {
        return UTL.randomint(90000) + 10000;
    }
    static generateNewDBid() {
        UTL.lastUsedDBid++;
        return UTL.lastUsedDBid;
    }
    static resetUsedDBids() {
        UTL.lastUsedDBid = 0;
    }
    static setDBidAsUsed(id) {
        if (id > UTL.lastUsedDBid)
            UTL.lastUsedDBid = id;
    }
    static randomelement(arr) {
        return arr[UTL.randomint(arr.length)];
    }
    static confirmdatadeletion() {
        return window.confirm("This action will result in data deletion, " +
            "including deletion of any dependant data in the process. " +
            "The operation is irrevirsible. Continue?");
    }
    static confirmunsavedchanges() {
        return window.confirm("Unsaved changes are present. Proceed anyway?");
    }
    static dataorempty(data) {
        if (data === undefined || data === null)
            return '';
        return data;
    }
    static throw(what) {
        throw new Error(what);
    }
    static getElem(name, upto = 2, ignore = false) {
        let obj = document.getElementById(name);
        if (obj === null && !ignore) {
            lo(g.error, 'trying to get a non-existent element with an id of: ' + name, 0, upto);
        }
        return obj;
    }
    static getElemOrThrow(name, what, upto = 2) {
        let obj = document.getElementById(name);
        if (obj === null) {
            lo(g.error, 'trying to get a non-existent element with an id of: ' + name, 0, upto);
            throw new Error(what);
        }
        return obj;
    }
    static getInputElem(name, upto = 3, ignore = false) {
        return UTL.getElem(name, upto, ignore);
    }
    static stringToArray(s) { return s.split(''); }
    static randInt0toNincl(n) {
        return Math.floor(Math.random() * Math.floor(n + 1));
    }
    static deleteCharAt(str, index) {
        return str.slice(index, 1);
    }
    static copyToClipboard(text) {
        let foo = document.createElement("textarea");
        foo.value = text;
        document.body.appendChild(foo);
        foo.focus();
        foo.select();
        document.execCommand('copy');
        document.body.removeChild(foo);
    }
    static getClipboardVal(e) {
        e.preventDefault();
        UTL.clipboardval = e.clipboardData.getData('text/plain');
    }
    static getClipboard() {
        if (UTL.clipboardval == '') {
            document.addEventListener('paste', UTL.getClipboardVal);
            INTERFACE.displayInfo('Press CTRL+V, then click this button again');
            return '';
        }
        let returnable = UTL.clipboardval;
        UTL.clipboardval = '';
        document.removeEventListener('paste', UTL.getClipboardVal);
        return returnable;
    }
    static replaceAll(input, search, replacement, nesting) {
        if (nesting === undefined)
            nesting = 0;
        if (nesting > 30) {
            lo(g.error, "too much recursion for '" + input + "' - to be replaced with '" + replacement + "'");
            return input;
        }
        let r = input.replace(search, replacement);
        return r == input ? r : UTL.replaceAll(r, search, replacement, nesting++);
    }
    ;
    static addProp(fnc, props) {
        let keys = Object.keys(props);
        let l = keys.length;
        for (let i = 0; i < l; i++)
            fnc[keys[i]] = props[keys[i]];
        return fnc;
    }
    static subscribeWithProps(subscriber, target, props) {
        let local = function (a, b) { target(local, a, b); };
        subscriber(UTL.addProp(local, props));
    }
    static assignWithProps(target, props) {
        let local = function (a, b) { target(local, a, b); };
        return UTL.addProp(local, props);
    }
    static verifyID(allegedId, maxId) {
        if (allegedId < 0)
            return false;
        if (!Number.isInteger(allegedId))
            return false;
        if (maxId !== undefined && allegedId >= maxId)
            return false;
        return true;
    }
    static loadFile(callback) {
        let input = document.createElement("input");
        input.setAttribute("type", "file");
        input.click();
        let readFile = function (file, callback2) {
            var reader = new FileReader();
            reader.onload = callback2;
            reader.readAsText(file);
        };
        input.onchange = function (e) {
            readFile(input.files[0], function (e) {
                SESSION.file.filename = input.files[0].name;
                input.outerHTML = "";
                return callback(e.target.result);
            });
        };
        input.outerHTML = "";
    }
    static escapeCharacters(input) {
        if (input.indexOf('\\') > -1)
            input = input.replace(/(\\)/g, '\\\\');
        if (input.indexOf("'") > -1)
            input = input.replace(/(\')/g, "\\'");
        if (input.indexOf('"') > -1)
            input = input.replace(/(\")/g, "\\'");
        return input;
    }
    static apostrophesToQuotes(input) {
        if (input.indexOf("'") > -1)
            input = input.replace(/(\')/g, '"');
        return input;
    }
    static addConst(obj, name, value) {
        Object.defineProperty(obj, name, {
            value: value,
            writable: false,
            enumerable: true,
            configurable: true
        });
    }
    static cartProd(input) {
        return input.reduceRight((accum, curVal) => accum.flatMap((accEl) => curVal.flatMap((cvEl) => [[cvEl].concat(accEl)])), [[]]);
    }
    static cartProdLeft(input) {
        return input.reduce((accum, curVal) => accum.flatMap((accEl) => curVal.flatMap((cvEl) => [[cvEl].concat(accEl)])), [[]]);
    }
}
UTL.counter = 1;
UTL.res_success = { error: false, msg: 'success' };
UTL.res_error = { error: true, msg: 'error' };
UTL.devCode = false;
UTL.hints = true;
UTL.clipboardval = '';
UTL.morphologyEditorHint = true; // TODO: hints/options class, options page, etc.
UTL.lastUsedDBid = 0;
///=============================================
function lo(_, message, value, upto, downto) {
    LOG.add(message, _, upto, downto);
    return value;
}
class LOG {
    constructor() {
        throw "This class is only to be used statically.";
    }
    static caughtError(msg = '(no message specified)', jm) {
        if (msg == null)
            msg = '(no message specified)';
        else if (msg == '[object Object]')
            msg = JSON.stringify(msg);
        lo(g.error, "on catch: " + msg.toString(), 0, 4);
        if (jm != null)
            lo(g.error, "more info: " + jm.toString(), 0, 4);
        if (SESSION && SESSION.file && SESSION.file.db)
            lo(g.info, 'last DB query (may or may not be relevant): ' + SESSION.file.db.lastquery);
        return msg;
    }
    static gettime() {
        let a = new Date();
        return a.getHours() + ":" + a.getMinutes() + ":" + a.getSeconds() + "." + a.getMilliseconds() + ": ";
    }
    static trace(info = '') {
        lo(g.trace, info, 0, 3, 2);
    }
    static add(printable, level, upto = 1, downto = 1) {
        if (level != g.error && !this.logEnabled[level]) // ALWAYS print errors!
            return;
        let err = Error();
        let stack = err.stack;
        if (upto < 1)
            upto = 1;
        if (downto < 1)
            downto = 1;
        if (stack === undefined) {
            try {
                throw err;
            }
            catch (error) {
                stack = error.stack || '';
            }
        }
        let trace = stack.trim().split('\n');
        let where = ': ';
        let l = trace.length;
        let include_arrow = false;
        let removable = '';
        let remove = false;
        if (downto > 1)
            where = ' -> ... ' + where;
        if (l > 0) {
            removable = trace[0].replace('add@', '');
            let pos = removable.indexOf('/core/');
            if (pos < 0)
                pos = removable.indexOf('\\core\\');
            if (pos >= 0) {
                pos++;
                removable = removable.slice(0, pos);
                remove = true;
            }
        }
        for (let i = 2; i < l; i++) {
            if (trace[i].includes('/external/') || trace[i].includes('\\external\\'))
                continue;
            downto--;
            if (downto > 0)
                continue;
            if (remove)
                trace[i] = trace[i].replace(removable, '');
            if (include_arrow)
                where = trace[i] + '\n' + where;
            else
                where = trace[i] + where;
            include_arrow = true;
            if (this.short_logs) {
                upto--;
                if (!upto)
                    break;
            }
        }
        let tolog = LOG.gettime()
            + where
            + LOG.logPrefices[level]
            + printable
            + '\n\n';
        if (UTL.devCode)
            LOG.backlog.push(tolog);
        //else
        console.log(tolog);
        if (level == g.error || level == g.warning)
            if (this.interface_errors)
                INTERFACE.logError(printable);
    }
    static clear() {
        LOG.backlog = [];
    }
}
LOG.backlog = [];
LOG.interface_errors = true;
LOG.short_logs = false;
LOG.logPrefices = {
    error: "Error: ",
    UIerror: "UI Error popup: ",
    warning: "Warning: ",
    info: "",
    debug: "Debug info: ",
    trace: "(tracing) ",
    DBquery: "Querying the DB: ",
    SCA: "SCA: "
};
//TODO: make manual overrides for these from the UI
//also override them from a settings file
LOG.logEnabled = {
    error: true,
    UIerror: true,
    warning: true,
    info: true,
    debug: true,
    trace: true,
    DBquery: false,
    SCA: false
};
///=============================================
//TODO: eventually typify out all presolves and prejects
function Presolve(msg) {
    return new Promise(function (resolve, reject) { resolve(msg); });
}
function PresolveT(msg) {
    return new Promise(function (resolve, reject) { resolve(msg); });
}
function Preject(msg) {
    lo(g.error, "rejected promise: " + msg, 0, 4);
    return new Promise(function (resolve, reject) { reject(msg); });
}
function PrejectT(msg) {
    let info = '';
    try {
        info = JSON.stringify(msg);
    }
    catch (err) { }
    if (info == '')
        try {
            info = JSON.stringify(Object.keys(msg));
        }
        catch (err) { }
    lo(g.error, "rejected promise: " + msg + '; ' + info, 0, 4);
    return new Promise(function (resolve, reject) { reject(msg); });
}
//# sourceMappingURL=utils.js.map