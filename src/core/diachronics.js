"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class DIACHRONICS {
    constructor(protoDivId, derivationsDivId, protoNameDivId, derivationsNameDivId, segmentsDivId, SCtextArea) {
        this.protoSlickgrid = null;
        this.derivationsSlickgrid = null;
        this.derivedLang = null;
        this.parent = null; // needed for buggy TS parser as of 3.8.3
        this.options = {};
        this.columnFilters = {};
        this.wordTypes = null;
        this.segments = "";
        this.initComplete = false;
        this.scrollLock = -1;
        this.dataRet = true;
        this.derivedWordTypesMap = [];
        this.derivedWordTypes = [];
        this.protoWordTypes = [];
        this.defaultWordType = -1;
        this.SGcellCSSkey = 'SGcellCSSkey';
        this.SGcellCSS = {};
        this.protoDivId = protoDivId;
        this.derivationsDivId = derivationsDivId;
        this.protoNameDivId = protoNameDivId;
        this.derivationsNameDivId = derivationsNameDivId;
        this.segmentsDivId = segmentsDivId;
        this.SCtextArea = SCtextArea;
        this.SCtextAreaHeight =
            (SCtextArea.clientHeight -
                UTL.getElem(this.segmentsDivId).clientHeight -
                UTL.getElem('previewMode').clientHeight -
                80) + 'px';
        this.SCtextArea.style.height = this.SCtextAreaHeight;
    }
    async init() {
        this.derivedLang = SESSION.getactiveconlang();
        if (this.derivedLang == null)
            return lo(g.error, 'conlang is not set', { error: true, msg: 'conlang is not set' });
        if (SESSION.file == null)
            return lo(g.error, 'no file is open', { error: true, msg: 'no file is open' });
        this.wordTypes = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + this.derivedLang.dbid);
        let ddata = (await SESSION.file.db.getdata(table_e.conlang, ['*'], 'id = ' + this.derivedLang.dbid))[0];
        if (!ddata.descends_from || this.derivedLang.protolangid == null) {
            INTERFACE.displayInfo('Select a conlang that descends from another conlang');
            let hideables = document.getElementsByClassName('hideable');
            let hl = hideables.length;
            for (let i = 0; i < hl; i++)
                hideables[i].style.visibility = 'hidden';
            return { error: true, msg: 'protolang is not set' };
        }
        let hideables = document.getElementsByClassName('hideable');
        let hl = hideables.length;
        for (let i = 0; i < hl; i++)
            hideables[i].style.visibility = 'inherit';
        let el = UTL.getElem(this.derivationsNameDivId);
        el && (el.innerHTML = ddata.name);
        let pdata = (await SESSION.file.db.getdata(table_e.conlang, ['*'], 'id = ' + this.derivedLang.protolangid))[0];
        el = UTL.getElem(this.protoNameDivId);
        el && (el.innerHTML = pdata.name);
        let derivedWordTypesRaw = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + this.derivedLang.dbid);
        let protoWordTypesRaw = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + this.derivedLang.protolangid);
        for (let i in derivedWordTypesRaw) {
            this.derivedWordTypesMap[derivedWordTypesRaw[i].descends_from] = derivedWordTypesRaw[i];
            this.derivedWordTypes[derivedWordTypesRaw[i].id] = derivedWordTypesRaw[i];
        }
        this.defaultWordType = derivedWordTypesRaw[0].id;
        for (let i in protoWordTypesRaw)
            this.protoWordTypes[protoWordTypesRaw[i].id] = protoWordTypesRaw[i];
        this.protoDataview = this.createDataview();
        this.derivationsDataview = this.createDataview();
        let protoColumns = [];
        let derivedColumns = [];
        let keys = Object.keys(diachronics_sch);
        for (let i = 0; i < keys.length; i++) {
            protoColumns.push(DICTIONARY.createcolumnfromschema(keys[i], TABLE_SCHEMAS.entry.cols[keys[i]], diachronics_sch[keys[i]], {
                validator: this.validator_proto_word_type,
                formatter: this.protoWordTypeFormatter,
                callee: this,
                editor: wordTypeEditor
            }));
            derivedColumns.push(DICTIONARY.createcolumnfromschema(keys[i], TABLE_SCHEMAS.entry.cols[keys[i]], diachronics_sch[keys[i]], {
                validator: this.validator_derived_word_type,
                formatter: this.derivedWordTypeFormatter,
                callee: this,
                editor: wordTypeEditor
            }));
        }
        this.options =
            {
                editable: true,
                enableAddRow: false,
                enableCellNavigation: true,
                asyncEditorLoading: false,
                autoEdit: false,
                multiColumnSort: true,
                numberedMultiColumnSort: true,
                tristateMultiColumnSort: true,
                sortColNumberInSeparateSpan: false,
                showHeaderRow: true,
                headerRowHeight: 30,
                explicitInitialization: true
            };
        this.protoSlickgrid = await this.setupGrid(this.protoDataview, this.protoDivId, this.derivedLang.protolangid, protoColumns);
        this.derivationsSlickgrid = await this.setupGrid(this.derivationsDataview, this.derivationsDivId, this.derivedLang.dbid, derivedColumns, this.dataRet.map((x) => x.diachronic_id));
        UTL.subscribeWithProps(this.protoSlickgrid.onScroll.subscribe, this.syncScrollbars, {
            fromId: 1,
            toId: 2,
            fromSG: this.protoSlickgrid,
            toSG: this.derivationsSlickgrid,
            callee: this
        });
        UTL.subscribeWithProps(this.derivationsSlickgrid.onScroll.subscribe, this.syncScrollbars, {
            fromId: 2,
            toId: 1,
            fromSG: this.derivationsSlickgrid,
            toSG: this.protoSlickgrid,
            callee: this
        });
        this.setupDataview(this.protoDataview, this.protoSlickgrid);
        this.setupDataview(this.derivationsDataview, this.derivationsSlickgrid);
        let localfilterproto = function (item) {
            for (let id in localfilterproto.callee.columnFilters) {
                if (id !== undefined && localfilterproto.callee.columnFilters[id] !== "") {
                    let c = localfilterproto.callee.protoSlickgrid.getColumns()[localfilterproto.callee.protoSlickgrid.getColumnIndex(id)];
                    if (id == 'part_of_speech') {
                        if (localfilterproto.callee.protoWordTypes[item[c.field]].abbreviation.
                            indexOf(localfilterproto.callee.columnFilters[id]) < 0)
                            return false;
                    }
                    else {
                        if (item[c.field].indexOf(localfilterproto.callee.columnFilters[id]) < 0)
                            return false;
                    }
                }
            }
            return true;
        };
        localfilterproto.callee = this;
        this.protoDataview.beginUpdate();
        this.protoDataview.setFilter(localfilterproto);
        this.protoDataview.endUpdate();
        let localfilterderivations = function (item) {
            for (let id in localfilterderivations.callee.columnFilters) {
                if (id !== undefined && localfilterderivations.callee.columnFilters[id] !== "") {
                    let c = localfilterderivations.callee.derivationsSlickgrid.getColumns()[localfilterderivations.callee.derivationsSlickgrid.getColumnIndex(id)];
                    if (id == 'part_of_speech') {
                        if (localfilterderivations.callee.derivedWordTypes[item[c.field]].abbreviation.
                            indexOf(localfilterderivations.callee.columnFilters[id]) < 0)
                            return false;
                    }
                    else {
                        if (item[c.field].indexOf(localfilterderivations.callee.columnFilters[id]) < 0)
                            return false;
                    }
                }
            }
            return true;
        };
        localfilterderivations.callee = this;
        this.derivationsDataview.beginUpdate();
        this.derivationsDataview.setFilter(localfilterderivations);
        this.derivationsDataview.endUpdate();
        let miscdata = (await SESSION.file.db.getdata(table_e.miscdata, ['*'], 'conlang = ' + this.derivedLang.dbid).catch(LOG.caughtError))[0];
        if (miscdata)
            this.SCtextArea.value = miscdata.sound_changes || '';
        miscdata = (await SESSION.file.db.getdata(table_e.miscdata, ['*'], 'conlang = ' + this.derivedLang.protolangid).catch(LOG.caughtError))[0];
        if (miscdata) {
            this.segments = miscdata.segments || '';
            let segmentsEl = UTL.getElem(this.segmentsDivId);
            segmentsEl.value = 'Protolang segments:\n' + this.segments;
            segmentsEl.style.height = (segmentsEl.scrollHeight) + "px";
        }
        this.SCtextArea.onkeydown = this.onSCAtextAreaInput;
        this.SCtextArea.parent = this;
        this.initComplete = true;
        return UTL.res_success;
    }
    onSCAtextAreaInput() {
        if (this.SCtimerHandle !== undefined)
            clearTimeout(this.SCtimerHandle);
        let dia = this.parent;
        dia.SCtextArea.style.height = dia.SCtextAreaHeight;
        this.SCtimerHandle = setTimeout(dia.onSCAtextAreaInputTimeout, 800, dia);
    }
    async onSCAtextAreaInputTimeout(dia) {
        await SESSION.file.db.updatedata(table_e.miscdata, { 'sound_changes': dia.SCtextArea.value }, 'conlang = ' + dia.derivedLang.dbid).catch(LOG.caughtError);
        if (dia.SCtextArea.value.trim().length < 1) {
            INTERFACE.clearUImessages();
            return;
        }
        let lineMap = [];
        let SCAinput = dia.segments + '\n' + dia.SCtextArea.value;
        let SCs = PARSER.parseAllLines(SCAinput, lineMap);
        if (!SCs.success) {
            INTERFACE.displayError(SCs.error);
            return;
        }
        INTERFACE.clearUImessages();
        let cursorRow = dia.SCtextArea.value.substr(0, dia.SCtextArea.selectionStart).split("\n").length - 1;
        cursorRow += (dia.segments + '\n').split("\n").length - 1;
        let lastSCtoApply = lineMap[cursorRow];
        let lastSC = lineMap[lineMap.length - 1];
        let persistDerivationsToDB = (lastSCtoApply == lastSC);
        let highlightMode = (UTL.getElem('previewMode').checked &&
            UTL.getElem('highlightMode').checked);
        if (!UTL.getElem('previewMode').checked) {
            lastSCtoApply = lastSC;
            persistDerivationsToDB = true;
        }
        let wordsToDerive = await SESSION.file.db.getdata(table_e.entry, ['*'], 'conlang = ' + dia.derivedLang.protolangid).catch(LOG.caughtError);
        let diaIds = wordsToDerive.map(x => x.diachronic_id);
        if (diaIds.length) {
            let alreadyDerived = await SESSION.file.db.getdata(table_e.entry, ['*'], 'conlang = ' + dia.derivedLang.dbid + ' AND diachronic_id IN (' + diaIds.join(', ') + ')').catch(LOG.caughtError);
            let alreadyDerivedDiaIds = alreadyDerived.map(x => x.diachronic_id);
            let newData = wordsToDerive.filter(x => !alreadyDerivedDiaIds.includes(x.diachronic_id));
            if (newData.length) {
                newData = newData.map(x => [
                    dia.derivedLang.dbid,
                    x.root,
                    dia.derivedWordTypesMap[x.part_of_speech].id || dia.defaultWordType,
                    x.translations,
                    x.diachronic_id,
                    x.notes,
                    x.state
                ]);
                // this really needs a better way of keeping track of all the table columns
                // TODO: the DB engine should really be changed to one with more features...
                await SESSION.file.db.savemultiplerows(table_e.entry, [
                    'conlang',
                    'root',
                    'part_of_speech',
                    'translations',
                    'diachronic_id',
                    'notes',
                    'state'
                ], newData).catch(LOG.caughtError);
            }
        }
        let alreadyDerivedWordsToDerive = await SESSION.file.db.getdata(table_e.entry, ['*'], 'conlang = ' + dia.derivedLang.dbid + ' AND diachronic_id IN (' + diaIds.join(', ') + ')').catch(LOG.caughtError);
        alreadyDerivedWordsToDerive.sort((a, b) => { return a.diachronic_id - b.diachronic_id; });
        wordsToDerive.sort((a, b) => { return a.diachronic_id - b.diachronic_id; });
        let pdl = alreadyDerivedWordsToDerive.length;
        for (let i = 0; i < pdl; i++) {
            alreadyDerivedWordsToDerive[i].protoId = wordsToDerive[i].id;
            if (alreadyDerivedWordsToDerive[i].state & entry_state_e.irregular_derivation)
                alreadyDerivedWordsToDerive[i].state |= entry_state_e.not_up_to_date;
        }
        if (persistDerivationsToDB) {
            let idsToDel = alreadyDerivedWordsToDerive.map(x => x.id);
            await SESSION.file.db.deleterowsbyid(table_e.entry, idsToDel);
        }
        // preserve original order:
        alreadyDerivedWordsToDerive.sort((a, b) => { return a.protoId - b.protoId; });
        wordsToDerive.sort((a, b) => { return a.id - b.id; });
        let newData = [];
        for (let i = 0; i < pdl; i++) {
            newData[i] = [
                alreadyDerivedWordsToDerive[i].conlang,
                alreadyDerivedWordsToDerive[i].root,
                alreadyDerivedWordsToDerive[i].part_of_speech,
                alreadyDerivedWordsToDerive[i].translations,
                alreadyDerivedWordsToDerive[i].diachronic_id,
                alreadyDerivedWordsToDerive[i].notes,
                alreadyDerivedWordsToDerive[i].state
            ];
            if (alreadyDerivedWordsToDerive[i].state & entry_state_e.irregular_derivation)
                continue;
            let parserResult = PARSER.executeCommandsOnASingleWord(wordsToDerive[i].root, SCs.commands, lastSCtoApply + 1);
            newData[i][1] = parserResult.word || ' ';
            if (highlightMode)
                alreadyDerivedWordsToDerive[i].root = parserResult.outInfo.wordMatches || newData[i][1];
            else
                alreadyDerivedWordsToDerive[i].root = newData[i][1];
            DICTIONARY.setSGcellCSS(dia.SGcellCSS, i, alreadyDerivedWordsToDerive[i].state);
        }
        if (persistDerivationsToDB) {
            await SESSION.file.db.savemultiplerows(table_e.entry, [
                'conlang',
                'root',
                'part_of_speech',
                'translations',
                'diachronic_id',
                'notes',
                'state'
            ], newData).catch(LOG.caughtError);
            if (highlightMode)
                dia.refreshDerivations(alreadyDerivedWordsToDerive);
            else
                dia.refreshDerivationsFromDB(diaIds);
        }
        else
            dia.refreshDerivations(alreadyDerivedWordsToDerive);
    }
    createDataview() {
        let dataview = new Slick.Data.DataView();
        dataview.parent = this;
        dataview.setItems([]);
        return dataview;
    }
    setupDataview(dataview, sg) {
        dataview.onRowCountChanged.subscribe(function (e, args) {
            if (sg == null)
                return lo(g.error, 'dataview slickgrid null or undefined', false);
            sg.updateRowCount();
            sg.render();
        });
        dataview.onRowsChanged.subscribe(function (e, args) {
            if (sg == null)
                return lo(g.error, 'dataview slickgrid null or undefined', false);
            sg.invalidateRows(args.rows);
            sg.render();
        });
    }
    async refreshDerivationsFromDB(filter) {
        let data = await SESSION.file.db.getdata(table_e.entry, ["*"], "conlang = " + this.derivedLang.dbid + " AND diachronic_id IN (" + filter.join(', ') + ")").catch(LOG.caughtError);
        this.refreshDerivations(data);
    }
    refreshDerivations(data) {
        this.derivationsDataview.beginUpdate();
        this.derivationsDataview.setItems(data);
        this.derivationsDataview.endUpdate();
        this.derivationsSlickgrid.invalidate();
        this.derivationsSlickgrid.updateRowCount();
        this.derivationsSlickgrid.render();
        this.SGcellCSS = {};
        let datalen = data.length;
        for (let i = 0; i < datalen; i++) {
            DICTIONARY.setSGcellCSS(this.SGcellCSS, i, data[i].state);
        }
        this.derivationsSlickgrid.setCellCssStyles(this.SGcellCSSkey, this.SGcellCSS);
        this.SCtextArea.focus();
    }
    async setupGrid(dataview, divId, clgId, columns, diaIds) {
        let slickgrid = new Slick.Grid("#" + divId, dataview, columns, this.options);
        slickgrid.setSelectionModel(new Slick.CellSelectionModel());
        let idsCond = '';
        if (diaIds !== undefined)
            idsCond = ' AND diachronic_id IN (' + diaIds.join(', ') + ')';
        let data = await SESSION.file.db.getdata(table_e.entry, ["*"], "conlang = " + clgId + idsCond).catch(LOG.caughtError);
        if (diaIds === undefined)
            this.dataRet = data;
        dataview.beginUpdate();
        dataview.setItems(data);
        dataview.endUpdate();
        UTL.subscribeWithProps(slickgrid.onBeforeEditCell.subscribe, (props, e, args) => {
            props.callee.previousCellValue = args.item[args.column.id];
            props.callee.pcvRow = args.item.id;
            props.callee.pcvColumn = args.column.id;
        }, { callee: this });
        UTL.subscribeWithProps(slickgrid.onCellChange.subscribe, (props, e, args) => {
            args.item[props.callee.pcvColumn] = props.callee.previousCellValue;
            props.dataview.beginUpdate();
            props.dataview.updateItem(props.callee.pcvRow, args.item);
            props.dataview.endUpdate();
            INTERFACE.displayInfo('Cells can be only copied here. Edit them in the lexicon tab.');
            slickgrid.render();
        }, {
            callee: this,
            dataview: dataview,
            slickgrid: slickgrid
        });
        /*
        TODO: implement sorting while maintaining row-to-row correspondance
        
        UTL.subscribeWithProps(
            slickgrid.onSort.subscribe,
            (props:eni, e:any, args:any) =>
                {
                let cols = args.sortCols;
                for (let i = 0, l = cols.length; i < l; i++)
                    {
                    props.dataview.fastSort(cols[i].sortCol.field, cols[i].sortAsc);
                    }
                },
                {
                callee: this,
                dataview: dataview
                }
            );*/
        UTL.subscribeWithProps(slickgrid.onHeaderRowCellRendered.subscribe, (props, e, args) => {
            let id = UTL.randomstring(20);
            args.node.innerHTML = `<input type='text' id="${id}" placeholder=" search/filter"></input>`;
            args.node.onkeyup = () => { props.callee.updateColumnFilters(id, args.column.id); };
        }, { callee: this });
        slickgrid.init();
        if (diaIds !== undefined) {
            this.SGcellCSS = {};
            let datalen = data.length;
            for (let i = 0; i < datalen; i++) {
                DICTIONARY.setSGcellCSS(this.SGcellCSS, i, data[i].state);
            }
            slickgrid.setCellCssStyles(this.SGcellCSSkey, this.SGcellCSS);
        }
        return slickgrid;
    }
    syncScrollbars(props, e, args) {
        if (props.callee.syncFrom == props.toId)
            return;
        if (props.callee.noLagOnScroll)
            clearTimeout(props.callee.noLagOnScroll);
        props.callee.syncFrom = props.fromId;
        props.callee.noLagOnScroll = setTimeout(function (ref) {
            ref.syncFrom = null;
        }, 100, props.callee);
        let top = props.fromSG.getViewport().top;
        ;
        props.toSG.scrollRowToTop(top);
    }
    updateEntries() {
        this.protoSlickgrid.invalidate();
        this.protoSlickgrid.updateRowCount();
        this.protoSlickgrid.render();
        this.derivationsSlickgrid.invalidate();
        this.derivationsSlickgrid.updateRowCount();
        this.derivationsSlickgrid.render();
    }
    updateColumnFilters(elemId, colId) {
        let elem = UTL.getInputElem(elemId);
        if (elem == null) {
            lo(g.error, "no element with id of " + elemId + " to filter by!");
            return;
        }
        this.columnFilters[colId] = elem.value.trim();
        this.derivationsDataview.refresh();
        this.protoDataview.refresh();
    }
    validator_proto_word_type(value) {
        if (!this.protoWordTypes)
            return { valid: false, msg: "Lexicon not initialised with word types" };
        if (!this.protoWordTypes[value])
            return { valid: false, msg: "Invalid word type" };
        else
            return { valid: true, msg: null };
    }
    protoWordTypeFormatter(row, cell, value, columnDef, dataContext) {
        if (columnDef.callee.protoWordTypes)
            if (columnDef.callee.protoWordTypes[value])
                return columnDef.callee.protoWordTypes[value].abbreviation;
        return 'err' + value;
    }
    validator_derived_word_type(value) {
        if (!this.derivedWordTypes)
            return { valid: false, msg: "Lexicon not initialised with word types" };
        if (!this.derivedWordTypes[value])
            return { valid: false, msg: "Invalid word type" };
        else
            return { valid: true, msg: null };
    }
    derivedWordTypeFormatter(row, cell, value, columnDef, dataContext) {
        if (columnDef.callee.derivedWordTypes)
            if (columnDef.callee.derivedWordTypes[value])
                return columnDef.callee.derivedWordTypes[value].abbreviation;
        return 'err' + value;
    }
}
//# sourceMappingURL=diachronics.js.map