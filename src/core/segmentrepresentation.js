"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
const HEXCOUNTERINIT = 9472; // beginning of unicode block characters
// therefore the usage of those as letters is NOT supported
var match_direction_t;
(function (match_direction_t) {
    match_direction_t[match_direction_t["LTR"] = 0] = "LTR";
    match_direction_t[match_direction_t["RTL"] = 1] = "RTL";
    match_direction_t[match_direction_t["ALL"] = 2] = "ALL";
})(match_direction_t || (match_direction_t = {}));
class segmentation_t {
    constructor() {
        this.hexcounter = HEXCOUNTERINIT;
        this.segToInner = {};
        this.innerToSeg = {};
        this.groups = {};
        this.groupRatios = {};
        this.maxSegLength = 0;
    }
}
class SCAcommand_t {
    constructor(commandNumber, type) {
        this.commandNumber = -1;
        this.numberOfExecutions = 0;
        this.leadingEmptyLines = 0;
        this.lastGroupDefinitionCommandIndex = -1;
        this.line = '';
        this.allCurrentSegmentsAndGroups = new segmentation_t();
        this.commandNumber = commandNumber;
        this.command = type;
    }
}
class SCAcommandSoundChange_t extends SCAcommand_t {
    constructor(commandNumber, source, target, condition, matchDirection, matchPattern) {
        //source - a string, containing the segments to undergo the sound change
        //         may be empty (string of length 0 !), in which case there's segment generation
        //target - a string, containing the segments, which the sound change results in
        //         may be empty (string of length 0 !), in which case there's segment loss
        //         one of the source and the target has to be non-empty
        //condition - a string or an array of strings, containing the sound environment(s), under which the sound change takes place
        //            has to be non empty when the source is (otherwise the segments would be generated at every position in the word)
        //matchDirection - the direction in which the sound change takes place. Defaults to LTRmatchDirection
        //matchPattern - a string, indicating which matches should actually undergo the sound change
        //               '_' undergoes it, 'x' is ignored, e.g. with 'xx_' only every third occurance of the environment undergoes the sound change
        //group names should be preserved in the source, target and condition
        //all segments should already be in inner representation
        super(commandNumber, command_t.SOUND_CHANGE);
        this.condition = '_';
        this.matchDirection = match_direction_t.ALL;
        this.matchPattern = '_';
        this.lastGroupDefinitionCommand = null;
        this.sourceIsEmpty = false;
        this.precondition = '';
        this.postcondition = '';
        this.sourceWithGroups = '';
        this.targetWithGroups = '';
        this.sourceRE = RegExp('');
        this.preRE = RegExp('');
        this.postRE = RegExp('');
        this.pruning = 0;
        if (typeof matchPattern === 'undefined')
            matchPattern = "_";
        this.source = source;
        this.target = target;
        if (condition !== undefined)
            this.condition = condition;
        if (matchDirection !== undefined)
            this.matchDirection = matchDirection;
        this.matchPattern = matchPattern;
        this.lastGroupDefinitionCommand = null;
    }
    prepareForSoundChanges(innerRepresentation) {
        this.sourceIsEmpty = (this.source.length == 0);
        this.precondition = '';
        this.postcondition = '';
        let prePostConditionSplit = [];
        prePostConditionSplit = this.condition.split('_');
        if (prePostConditionSplit.length != 2 && this.condition != '') {
            let errorstring = "Illdefined condition: multiple or no source locations (underscore characters) in " +
                innerToHumanReadable(innerRepresentation, this.condition);
            let hmm = {
                'error': true,
                'msg': errorstring
            };
            return hmm;
        }
        else if (prePostConditionSplit.length == 2) {
            this.precondition = linguisticToRegexAlternativeNotation(innerRepresentation, prePostConditionSplit[0]);
            this.postcondition = linguisticToRegexAlternativeNotation(innerRepresentation, prePostConditionSplit[1]);
            if (!this.sourceIsEmpty) {
                if (this.precondition.length > 0)
                    this.precondition += "_";
                if (this.postcondition.length > 0)
                    this.postcondition = "_" + this.postcondition;
            }
            this.precondition = wordToInner(innerRepresentation, this.precondition).replace(/#/g, '[\\s,.]');
            this.postcondition = wordToInner(innerRepresentation, this.postcondition).replace(/#/g, '[\\s,.]');
        }
        this.sourceWithGroups = this.source;
        this.targetWithGroups = this.target;
        this.source = wordToInner(innerRepresentation, this.source);
        this.target = wordToInner(innerRepresentation, this.target);
        this.sourceRE = RegExp(this.source, 'gu');
        if (this.sourceIsEmpty) {
            // g flag is needed so that SCA doesn't stuck in a loop when generating new segments
            this.preRE = RegExp(this.precondition, 'gu');
            this.postRE = RegExp(this.postcondition, 'gu');
        }
        else {
            // g flag needs to be absent so that obscure condition matching bugs don't crop up
            this.preRE = RegExp(this.precondition, 'u');
            this.postRE = RegExp(this.postcondition, 'u');
        }
        return { error: false, msg: '' };
    }
}
class SCAcommandPattern_t extends SCAcommand_t {
    constructor(commandNumber, prefix, suffix, principalFormIndex, condition = '', secondaryPrincipalFormIndex = -1) {
        //TODO: document
        super(commandNumber, command_t.PATTERN);
        this.SCAprefix = '';
        this.SCAsuffix = '';
        this.SCAcondition = '';
        this.RE = RegExp('uninitialised');
        this.prefix = prefix;
        this.suffix = suffix;
        this.condition = UTL.removeAllSpaces(condition);
        this.principalFormIndex = principalFormIndex;
        this.secondaryPrincipalFormIndex = secondaryPrincipalFormIndex;
        this.lastGroupDefinitionCommand = null;
    }
    prepareForSoundChanges(innerRepresentation) {
        this.SCAprefix = wordToInner(innerRepresentation, this.prefix);
        this.SCAsuffix = wordToInner(innerRepresentation, this.suffix);
        if (this.condition.length > 0) {
            this.SCAcondition = linguisticToRegexAlternativeNotation(innerRepresentation, this.condition);
            this.SCAcondition = wordToInner(innerRepresentation, this.SCAcondition).replace(/#/g, '[\\s,.]');
            this.RE = RegExp(this.SCAcondition, 'u');
        }
        return false;
    }
}
class SCAcommandGroupDefinition_t extends SCAcommand_t {
    constructor(commandNumber, name, segments, segmentRatios, itIsGroupRedefinition) {
        //segments should be an non empty string array of segments (surface representation) and group names
        //segmentRatios should be a json with the former as key and generation probability as the latter
        super(commandNumber, command_t.DEFINE_GROUP);
        this.allCurrentSegmentsAndGroups = new segmentation_t();
        this.innerSegments = '';
        this.name = name;
        this.segments = segments;
        this.segmentRatios = segmentRatios;
        this.itIsGroupRedefinition = itIsGroupRedefinition;
    }
    setAllCurrentSegmentsAndGroups(allCurrentSegmentsAndGroups) {
        //more stringent checks will be in place during precompilation and execution
        //what we really need is a deep copy
        this.allCurrentSegmentsAndGroups.hexcounter = allCurrentSegmentsAndGroups.hexcounter;
        //shallow copying object properties so as to not step on each others' toes
        this.allCurrentSegmentsAndGroups.segToInner = Object.assign({}, allCurrentSegmentsAndGroups.segToInner);
        this.allCurrentSegmentsAndGroups.innerToSeg = Object.assign({}, allCurrentSegmentsAndGroups.innerToSeg);
        this.allCurrentSegmentsAndGroups.groups = Object.assign({}, allCurrentSegmentsAndGroups.groups);
        this.allCurrentSegmentsAndGroups.groupRatios = Object.assign({}, allCurrentSegmentsAndGroups.groupRatios);
        this.allCurrentSegmentsAndGroups.maxSegLength = allCurrentSegmentsAndGroups.maxSegLength;
        this.innerSegments = wordToInner(this.allCurrentSegmentsAndGroups, arjoin(this.segments, ' '));
    }
}
function innerToHumanReadable(segments, word) {
    return sca(word, segments.innerToSeg);
}
function bothRepresentations(segments, sth) {
    return innerToHumanReadable(segments, sth) + ' (' + sth + ')';
}
function sca(ww, rm) {
    //TODO: refactor this functions' name
    //replaces the strings in ww as defined by rm JSON: it defines a simple dictionary replacement
    //e.g. if ww = 'foo' and rm = {'f':'bar'}
    //the function will return 'baroo'
    if (ww === undefined)
        return '';
    if (ww === null)
        return '';
    if (rm === undefined)
        return ww;
    if (rm === null)
        return ww;
    if (Object.keys(rm).length === undefined)
        return ww;
    if (Object.keys(rm).length == 0)
        return ww;
    return ww.replace(new RegExp("(" + Object.keys(rm).map(function (i) { return i.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&"); }).join("|") + ")", "gu"), function (s) { return rm[s]; }
    //NOTE: if JS regex syntax changes (significantly), this part will have to be reworked or remade entirely
    );
}
function declareSegments(segments, segs, g, nums) {
    let groupOrSegmentRedifition = false;
    if (g != '' && segments.groups[g] !== undefined)
        groupOrSegmentRedifition = true;
    if (g != '' && nums[0].trim() != '') {
        segments.groupRatios[g] = [];
        let totalChance = 0;
        for (let i = 0; i < nums.length; i++) {
            try {
                segments.groupRatios[g][i] = totalChance + Number(nums[i]);
                totalChance += Number(nums[i]);
            }
            catch (e) {
                segments.groupRatios[g][i] = totalChance + 1;
                totalChance++;
            }
        }
        for (let i = nums.length; i < segs.length; i++) {
            segments.groupRatios[g][i] = totalChance + 1;
            totalChance++;
        }
    }
    let res = '';
    for (let i = 0; i < segs.length; i++)
        if (!res.includes(segs[i])) //allow for repeated inclusion for sake of conversion, e.g.
         
        //A {a o å u} > E {e y e i}
        //but don't redeclare segments
        {
            let hexchar = '';
            if (segments.groups[segs[i]] !== undefined && segments.groups[segs[i]] !== null)
                hexchar = segments.groups[segs[i]]; //unpack a group within a group definition
            else if (segments.segToInner[segs[i]] === undefined) {
                hexchar = String.fromCharCode(segments.hexcounter);
                segments.hexcounter++;
                segments.maxSegLength = Math.max(segments.maxSegLength, segs[i].length);
                if (segments.segToInner[segs[i]] !== undefined)
                    groupOrSegmentRedifition = true;
                segments.segToInner[segs[i]] = hexchar;
                segments.innerToSeg[hexchar] = segs[i];
            }
            else
                hexchar = segments.segToInner[segs[i]];
            res += hexchar;
        }
    if (g != '') {
        segments.groups[g] = res;
        segments.maxSegLength = Math.max(segments.maxSegLength, g.length);
    }
    //dolog('  segments: ' + JSON.stringify(segments.segToInner).replaceAll('"',''));
    //dolog('  groups: ' + JSON.stringify(segments.groups).replaceAll('"',''));
    //dolog('  group chances: ' + JSON.stringify(segments.groupRatios).replaceAll('"',''));
    return groupOrSegmentRedifition;
}
function wordToInner(segments, w) {
    w = w.trim();
    let res = '';
    let i = 0, j = 0;
    let wlength = w.length;
    let found = false;
    let sequence = '';
    for (i = 0; i < wlength;) {
        found = false;
        for (j = Math.min(segments.maxSegLength, wlength - i); j > 0; j--) //give priority to longer sequences first
         { //otherwise digraphs and combining diacritics won't ever get parsed
            sequence = w.substring(i, i + j);
            if (segments.groups[sequence] !== undefined) { //if the char combination matches a group, decompose it to its constituents:
                found = true; //e.g. 'V' would become [aouei]
                res += '[' + segments.groups[sequence] + ']';
                i += j;
                break;
            }
            else if (segments.segToInner[sequence] !== undefined) {
                found = true;
                res += segments.segToInner[sequence];
                i += j;
                break;
            }
        }
        if (!found) //only add the character if it doesn't correspond to a group or a segment
         {
            res += w[i];
            i++;
        }
    }
    return res;
}
//w should be a monolithic word and not contain any special or whitespace characters!
function wordToInnerLeaveGroups(segments, w) {
    w = w.trim();
    let res = '';
    let i = 0, j = 0;
    let wlength = w.length;
    let found = false;
    let sequence = '';
    for (i = 0; i < wlength;) {
        found = false;
        for (j = Math.min(segments.maxSegLength, wlength - i); j > 0; j--) //give priority to longer sequences first
         { //otherwise digraphs and combining diacritics won't ever get parsed
            sequence = w.substring(i, i + j);
            if (segments.segToInner[sequence] !== undefined && segments.groups[sequence] === undefined) { //if there's a conflict between a segment name and a group name, assume it's a group
                found = true;
                res += segments.segToInner[sequence];
                i += j;
                break;
            }
        }
        if (!found) //only add the character if it doesn't correspond to a group or a segment
         {
            res += w[i];
            i++;
        }
    }
    return res;
}
function linguisticToRegexAlternativeNotation(segments, notation) {
    //TODO: move to the parser?
    //transform linguistic alternative notation {a b c d} to regex alternative notation (a|b|c|d)
    //alternatives within alternatives are illegal
    let l = notation.length;
    let inAlternative = false;
    let orig = notation;
    let aBarInSpace = false;
    let firstCharReached = false;
    let notation_arr = notation.split("");
    for (let i = 0; i < l; i++) {
        switch (notation_arr[i]) {
            case '{':
                if (inAlternative) {
                    lo(g.error, "embedded alternatives({} within {}) are prohibited: " + innerToHumanReadable(segments, notation));
                    return orig;
                }
                notation_arr[i] = '(';
                inAlternative = true;
                aBarInSpace = false;
                firstCharReached = false;
                break;
            case '}':
                if (!inAlternative) {
                    lo(g.error, "embedded alternatives({} within {}) are prohibited: " + innerToHumanReadable(segments, notation));
                    return orig;
                }
                notation_arr[i] = ')';
                if (i > 0 && notation_arr[i - 1] == '|') {
                    //notation = deleteCharAt(notation, i-1);
                    notation_arr.splice(i, 1);
                    i--;
                    l--;
                }
                inAlternative = false;
                aBarInSpace = false;
                break;
            case '\t':
            case ' ':
                if (inAlternative) {
                    if (!aBarInSpace && firstCharReached) {
                        notation_arr[i] = '|';
                        aBarInSpace = true;
                    }
                    else {
                        //notation = deleteCharAt(notation, i);
                        notation_arr.splice(i, 1);
                        l--;
                        i--;
                    }
                }
                break;
            default:
                aBarInSpace = false;
                firstCharReached = true;
                break;
        }
    }
    notation = notation_arr.join("");
    //dolog("transforming alternatives: from >"+orig+"< to >"+notation+"<");
    return notation;
}
//# sourceMappingURL=segmentrepresentation.js.map