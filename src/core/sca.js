"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//SCA stands for sound change applier
class wordMetaData_t {
    // so call them only when logging is enabled from the utils
    constructor(input, SCcommand, segments, accumulateStats) {
        this.error = false;
        this.errorMsg = '';
        this.outInfo = new outInfo_t();
        this.matchStart = [];
        this.matchEnd = [];
        this.sourceMatches = [];
        this.numberOfSourceMatches = 0;
        this.prematchedWord = '';
        this.postmatchedWord = '';
        this.preconditionMatchMask = 0;
        this.postconditionMatchMask = 0;
        this.conditionalMatchStart = [];
        this.conditionalMatchEnd = [];
        this.logging = false; // some of the logging operations here are really time-consuming
        this.input = input;
        this.word = input;
        this.SCcommand = SCcommand;
        this.segments = segments;
        this.accumulateStats = accumulateStats;
        this.logging = LOG.logEnabled[g.SCA];
    }
}
;
class SCA {
    static applySCtoaWord(segments, word, SCcommand, accumulateStats) {
        //comments after SCs are allowed - but what to do about 'A > B // <-' ?
        //some solutions:
        //A > B / <-
        //A > B : <-
        //<- A > B
        //A > B <-
        let wordMetaData = new wordMetaData_t(word, SCcommand, segments, accumulateStats);
        if (wordMetaData.SCcommand.command !== command_t.SOUND_CHANGE)
            return { word: wordMetaData.word, success: true, outInfo: wordMetaData.outInfo, errorMsg: wordMetaData.errorMsg };
        SCA.matchSource(wordMetaData);
        if (wordMetaData.error)
            return { word: wordMetaData.word, success: false, outInfo: wordMetaData.outInfo, errorMsg: wordMetaData.errorMsg };
        if (wordMetaData.SCcommand.sourceIsEmpty) {
            SCA.matchConditionSegmentGeneration(wordMetaData);
            if (wordMetaData.error)
                return { word: wordMetaData.word, success: false, outInfo: wordMetaData.outInfo, errorMsg: wordMetaData.errorMsg };
            //SCAmatchSourceUnderCondition(wordMetaData);
            //if (wordMetaData.error) return {word: wordMetaData.word, success: false};
        }
        else {
            SCA.matchConditionSegmentSubstitution(wordMetaData);
            if (wordMetaData.error)
                return { word: wordMetaData.word, success: false, outInfo: wordMetaData.outInfo, errorMsg: wordMetaData.errorMsg };
        }
        SCA.substituteSourceWithTarget(segments, wordMetaData);
        if (wordMetaData.error)
            return { word: wordMetaData.word, success: false, outInfo: wordMetaData.outInfo, errorMsg: wordMetaData.errorMsg };
        return { word: wordMetaData.word, success: true, outInfo: wordMetaData.outInfo, errorMsg: wordMetaData.errorMsg };
    }
    static matchSource(wordMD) {
        //keep the inits for now
        wordMD.matchStart = [];
        wordMD.matchEnd = [];
        wordMD.numberOfSourceMatches = 0;
        wordMD.sourceMatches = [];
        let conditionMatch;
        if (wordMD.SCcommand.command !== command_t.SOUND_CHANGE) {
            wordMD.error = true;
            wordMD.errorMsg = 'SCAmatchSource() executed on a non sound change command';
            return;
        }
        let soundChangeCmd = wordMD.SCcommand;
        //NOTE: if JS regex syntax changes (significantly), this part of the code will have to be reworked or remade entirely
        if (!soundChangeCmd.sourceIsEmpty)
            while ((conditionMatch = soundChangeCmd.sourceRE.exec(wordMD.word)) !== null) {
                wordMD.matchStart[wordMD.numberOfSourceMatches] = soundChangeCmd.sourceRE.lastIndex - conditionMatch[0].length;
                wordMD.matchEnd[wordMD.numberOfSourceMatches] = soundChangeCmd.sourceRE.lastIndex - 1;
                wordMD.numberOfSourceMatches++;
            }
        else
            for (let i = 0; i < wordMD.word.length; i++) //the case with segment generation could be optimised
             {
                wordMD.matchStart[wordMD.numberOfSourceMatches] =
                    wordMD.matchEnd[wordMD.numberOfSourceMatches] = i;
                wordMD.numberOfSourceMatches++;
            }
        if (!soundChangeCmd.sourceIsEmpty) //only when there's something to match
            if (wordMD.logging || soundChangeCmd.precondition != '' || soundChangeCmd.postcondition != '') //don't bother when the SC is unconditional and there's no logging
                for (let i = 0; i < wordMD.numberOfSourceMatches; i++) {
                    let prematch = wordMD.word.substring(0, wordMD.matchStart[i]);
                    let postmatch = wordMD.word.substring(wordMD.matchEnd[i] + 1);
                    let mach = "_".repeat(wordMD.matchEnd[i] - wordMD.matchStart[i] + 1);
                    wordMD.sourceMatches[i] = prematch + mach + postmatch;
                    if (wordMD.logging)
                        lo(g.SCA, "word matches source at: " + bothRepresentations(wordMD.segments, wordMD.sourceMatches[i]));
                }
        if (wordMD.logging) {
            if (!soundChangeCmd.sourceIsEmpty) {
                let matches = wordMD.word.split("");
                let numCurrentMatches = 0;
                let matchesStartIndex = 0;
                let matchesEndIndex = 0;
                for (let i = 0; i < matches.length; i++) {
                    let magicTime = 0;
                    if (matchesStartIndex < wordMD.numberOfSourceMatches) {
                        if (wordMD.matchStart[matchesStartIndex] == i) {
                            matchesStartIndex++;
                            magicTime = 1;
                            numCurrentMatches++;
                        }
                    }
                    if (matchesEndIndex < wordMD.numberOfSourceMatches) {
                        if (wordMD.matchEnd[matchesEndIndex] == i) {
                            matchesEndIndex++;
                            if (matchesEndIndex == matchesStartIndex)
                                magicTime = 2;
                            else
                                magicTime = 3;
                            numCurrentMatches--;
                        }
                    }
                    switch (magicTime) {
                        case 0:
                            if (!numCurrentMatches)
                                break;
                            if (numCurrentMatches == 1)
                                matches[i] = '-';
                            else
                                matches[i] = '' + Math.min(numCurrentMatches, 9);
                            break;
                        case 1:
                            matches[i] = '<';
                            break;
                        case 2:
                            matches[i] = '_';
                            break;
                        case 3:
                            matches[i] = '>';
                            break;
                    }
                }
                let matchesStr = matches.join("");
                lo(g.SCA, "source is " + bothRepresentations(wordMD.segments, soundChangeCmd.source));
                lo(g.SCA, "all source matches are: " + bothRepresentations(wordMD.segments, matchesStr));
            }
            else
                lo(g.SCA, "source is empty, meaning we're generating segments...");
        }
        lo(g.SCA, "wordMD.numberOfSourceMatches = " + wordMD.numberOfSourceMatches);
        //note the positions, where the source is matched
        //output:
        //	matchStart - array with beginings of respective input matches
        //	matchEnd - array with endings of respective input matches
        //	numberOfSourceMatches - number of matches
        //example:
        //input: loremipsum
        //       0123456789
        //source: VN* > ...
        //output:
        //	numberOfSourceMatches = 4
        //	matchStart[0 to 3] = 1, 3, 5, 8
        //	matchEnd[0 to 3] = 1, 4, 5, 9
        //interpretation: (x = matched source segment, b = match begining, e = match end)
        //       loremipsum
        //       0123456789
        //       _x_xxx__xx
        //       _x_bex__be
    }
    static matchConditionSegmentGeneration(wordMD) {
        //start looking for matches for conditional sound changes
        let prematches = [];
        let postmatches = [];
        let conditionMatch = [];
        let numPreMatches = 0;
        let numPostMatches = 0;
        if (wordMD.SCcommand.command !== command_t.SOUND_CHANGE) {
            wordMD.error = true;
            wordMD.errorMsg = 'SCAmatchConditionSegmentGeneration() executed on a non sound change command';
            return;
        }
        let soundChangeCmd = wordMD.SCcommand;
        if (soundChangeCmd.precondition != '')
            while ((conditionMatch = soundChangeCmd.preRE.exec(wordMD.word)) !== null) {
                prematches[numPreMatches++] = soundChangeCmd.preRE.lastIndex;
                if (soundChangeCmd.sourceIsEmpty && prematches[numPreMatches - 1] < wordMD.word.length)
                    prematches[numPreMatches - 1] = Math.max(prematches[numPreMatches - 1] - 1, 0);
            }
        if (soundChangeCmd.postcondition != '')
            while ((conditionMatch = soundChangeCmd.postRE.exec(wordMD.word)) !== null)
                if (soundChangeCmd.postRE.lastIndex - conditionMatch[0].length - 1 >= 0)
                    postmatches[numPostMatches++] = soundChangeCmd.postRE.lastIndex - conditionMatch[0].length - 1;
        //output: boundaries of condition matches
        //	numPreMatches - number of times the precondition has been matched
        //	numPostMatches - number of times the postcondition has been matched
        //	prematches - array with indices of precondition matches, several cases of notation:
        //		case 1: no segment generation
        //			always the first segment after the precondition match, even if this segment technically doesn't exist, e.g.
        //			loremipsu with / V_ gives prematches[0-3] = 2, 4, 6, 9
        //			012345678
        //			  ^ ^ ^  ^
        //		case 2: segment generation
        //			the last segment of the precondition match, unless the precondition is the word begining or the last segment
        //			in which case the resulting index is the first or one greater than the last index of the input respectively
        //			loremipsu with / V_ gives prematches[0-3] = 1, 3, 5, 9
        //			012345678
        //			 ^ ^ ^   ^
        //	prematches - array with indices of postcondition matches - contains the last segment before each postcondition match
        //			loremipsu with / _V gives postmatches[0-3] = 0, 2, 4, 7
        //			012345678
        //			^ ^ ^  ^
        //TODO: why is it unmarked word initially?
        //what will umm / V_ give?
        //potential bug?
        //keep the inits for now
        wordMD.prematchedWord = '';
        wordMD.postmatchedWord = '';
        wordMD.preconditionMatchMask = 0;
        wordMD.postconditionMatchMask = 0;
        if (soundChangeCmd.precondition != '') {
            let wordLength = wordMD.word.length;
            let j = 0;
            let matchIndex = 0;
            for (let i = 0; i < wordLength; i++)
                if (i == prematches[j]) {
                    wordMD.prematchedWord += '!';
                    if (matchIndex < wordMD.numberOfSourceMatches)
                        while (wordMD.matchStart[matchIndex] < i) //there are still things left to match
                         {
                            if (matchIndex++ >= wordMD.numberOfSourceMatches)
                                break;
                        }
                    if (wordMD.matchStart[matchIndex] == i)
                        wordMD.preconditionMatchMask |= (1 << matchIndex);
                    j++;
                }
                else
                    wordMD.prematchedWord += wordMD.word[i];
        }
        //output:
        //	prematchedWord - indicates where the precondition has been matched, for logging purposes only
        //		e.g. loremipsu with / V_  and no segment generation gives lo!e!i!su
        //		TODO: marking after the end, too
        //	preconditionMatchMask - number, indicating in binary where input matches with precondition, e.g.
        //	loremipsum with C > ... / V_ gives:
        //	1010100100 = huge number in decimal
        if (soundChangeCmd.postcondition != '') {
            let wordLength = wordMD.word.length;
            let j = 0;
            let matchIndex = 0;
            for (let i = 0; i < wordLength; i++)
                if (i == postmatches[j]) {
                    wordMD.postmatchedWord += '!';
                    if (matchIndex < wordMD.numberOfSourceMatches)
                        while (wordMD.matchEnd[matchIndex] < i)
                            if (matchIndex++ >= wordMD.numberOfSourceMatches)
                                break;
                    if (wordMD.matchEnd[matchIndex] == i)
                        wordMD.postconditionMatchMask |= (1 << matchIndex);
                    j++;
                }
                else
                    wordMD.postmatchedWord += wordMD.word[i];
        }
        //output:
        //	postmatchedWord - indicates where the postcondition has been matched, for logging purposes only
        //		e.g. loremipsu with / _V gives   !o!e!ip!u
        //		TODO: marking before the begining, too
        //	postconditionMatchMask - number, indicating in binary where input matches with postcondition, e.g.
        //	loremipsum with C > ... / _V gives:
        //	0010101001 = huge number in decimal
        //TODO: recheck the flag logic
        //for logging purposes only:
        if (wordMD.logging) {
            let s = (prematches.length != 1) ? 's' : '';
            if (wordMD.logging)
                if (soundChangeCmd.precondition != '') {
                    lo(g.SCA, "precondition is: " + bothRepresentations(wordMD.segments, soundChangeCmd.precondition));
                    lo(g.SCA, "word matches precondition at: " + bothRepresentations(wordMD.segments, wordMD.prematchedWord) + ', a total of ' + prematches.length + ' time' + s);
                }
            s = (postmatches.length != 1) ? 's' : '';
            if (wordMD.logging)
                if (soundChangeCmd.postcondition != '') {
                    lo(g.SCA, "postcondition is: " + bothRepresentations(wordMD.segments, soundChangeCmd.postcondition));
                    lo(g.SCA, "word matches postcondition at: " + bothRepresentations(wordMD.segments, wordMD.postmatchedWord) + ', a total of ' + postmatches.length + ' time' + s);
                }
        }
        //keep the inits for now
        wordMD.conditionalMatchStart = [];
        wordMD.conditionalMatchEnd = [];
        for (let i = 0; i < wordMD.numberOfSourceMatches; i++)
            //TODO: the pre- and postconditions shouldnt be relied upon here
            //remove them from the structure
            if ((soundChangeCmd.precondition == '' || (wordMD.preconditionMatchMask & (1 << i)) != 0)
                && (soundChangeCmd.postcondition == '' || (wordMD.postconditionMatchMask & (1 << i)) != 0)) {
                wordMD.conditionalMatchStart[i] = wordMD.matchStart[i];
                wordMD.conditionalMatchEnd[i] = wordMD.matchEnd[i];
            }
        //end of looking for matches for conditional sound changes
        //output:
        //	conditionalMatchStart - array with beginings of respective input matches that also match the condition; also contains undefined if the input doesnt match
        //	conditionalMatchEnd - array with endings of respective input matches that also match the condition; also contains undefined if the input doesnt match
        //example:
        //input: loremipsum
        //       0123456789
        //       __x_x_xx__
        //source: CC* > ... / V_V
        //output:
        //	conditionalMatchStart[0 to 4] = undefined, 2, 4, 6, undefined
        //	conditionalMatchEnd[0 to 4] = undefined, 2, 4, 7, undefined
    }
    static matchConditionSegmentSubstitution(wordMD) {
        wordMD.conditionalMatchStart = [];
        wordMD.conditionalMatchEnd = [];
        if (wordMD.SCcommand.command !== command_t.SOUND_CHANGE) {
            wordMD.error = true;
            wordMD.errorMsg = 'SCAmatchConditionSegmentSubstitution() executed on a non sound change command';
            return;
        }
        let soundChangeCmd = wordMD.SCcommand;
        //for logging purposes only:
        if (wordMD.logging) {
            if (soundChangeCmd.precondition != '')
                lo(g.SCA, "precondition is: " + bothRepresentations(wordMD.segments, soundChangeCmd.precondition));
            if (soundChangeCmd.postcondition != '')
                lo(g.SCA, "postcondition is: " + bothRepresentations(wordMD.segments, soundChangeCmd.postcondition));
        }
        //don't know what's up with this, but if these two lines are removed, the condition is not matched correctly
        if (soundChangeCmd.precondition != '')
            while (soundChangeCmd.preRE.exec(undefined) !== null) { }
        if (soundChangeCmd.postcondition != '')
            while (soundChangeCmd.postRE.exec(undefined) !== null) { }
        let cm = [];
        for (let i = 0; i < wordMD.numberOfSourceMatches; i++) {
            let matchesPrecondition = ((soundChangeCmd.precondition == '') || ((cm = soundChangeCmd.preRE.exec(wordMD.sourceMatches[i])) !== null));
            let matchesPostcondition = ((soundChangeCmd.postcondition == '') || ((cm = soundChangeCmd.postRE.exec(wordMD.sourceMatches[i])) !== null));
            if (wordMD.logging)
                lo(g.SCA, wordMD.sourceMatches[i] + " matches pre-, resp. postcondition: " + matchesPrecondition + ', ' + matchesPostcondition);
            if (matchesPrecondition && matchesPostcondition) {
                wordMD.conditionalMatchStart[i] = wordMD.matchStart[i];
                wordMD.conditionalMatchEnd[i] = wordMD.matchEnd[i];
                if (wordMD.logging)
                    lo(g.SCA, "word matches condition at: " + bothRepresentations(wordMD.segments, wordMD.sourceMatches[i]));
            }
        }
    }
    static substituteSourceWithTarget(segments, wordMD) {
        if (wordMD.SCcommand.command !== command_t.SOUND_CHANGE) {
            wordMD.error = true;
            wordMD.errorMsg = 'SCA.substituteSourceWithTarget() executed on a non sound change command';
            return;
        }
        let soundChangeCmd = wordMD.SCcommand;
        let logmsg = '\' (interpreted as \'' + soundChangeCmd.source +
            ' > ' + soundChangeCmd.target +
            ' / ' + soundChangeCmd.condition +
            '\') for \'';
        let outInfoWordMatches = '';
        let endResult = '';
        let affectedSegments = '';
        let jStart = 0;
        let alternation = 0;
        let numMatches = 0;
        let indexOfLastMatch = 0;
        for (let i = 0; i < wordMD.conditionalMatchStart.length; i++)
            if (wordMD.conditionalMatchStart[i] !== null && wordMD.conditionalMatchStart[i] !== undefined)
                numMatches++;
        for (let i = 0; i < wordMD.conditionalMatchStart.length; i++) //the actual applying of the sound change happens in here
         {
            let l = wordMD.word.length;
            if (wordMD.conditionalMatchStart[i] !== null && wordMD.conditionalMatchStart[i] !== undefined)
                for (let j = jStart; j < l; j++) {
                    if (j == wordMD.conditionalMatchStart[i])
                        alternation++;
                    let evenAndOddConditions = //flag which states if there's a match that also adheres to the 'every other' condition
                     (soundChangeCmd.matchDirection == match_direction_t.ALL)
                        || (soundChangeCmd.matchDirection == match_direction_t.RTL && soundChangeCmd.pruning == -1)
                        || (soundChangeCmd.matchDirection == match_direction_t.RTL && (numMatches - alternation) % 2 == soundChangeCmd.pruning)
                        || (soundChangeCmd.matchDirection == match_direction_t.LTR && ((alternation % 2) == soundChangeCmd.pruning))
                        || numMatches == 1;
                    lo(g.SCA, '' + wordMD.SCcommand.line + ' ' + j + ': ' + evenAndOddConditions);
                    if (soundChangeCmd.sourceIsEmpty) // if we are just generating new segments
                     { // TODO: look into if the two subcases can be unified
                        outInfoWordMatches += wordMD.word[j];
                        if (j <= wordMD.conditionalMatchStart[i] || j > wordMD.conditionalMatchEnd[i] || !evenAndOddConditions) {
                            endResult += wordMD.word[j];
                            affectedSegments += wordMD.word[j];
                        }
                    }
                    if (evenAndOddConditions) {
                        if (j == wordMD.conditionalMatchStart[i]) {
                            if (wordMD.accumulateStats)
                                soundChangeCmd.numberOfExecutions++;
                            outInfoWordMatches += '<';
                            affectedSegments += '<';
                            let res = SCA.generateOutputSegments(segments, soundChangeCmd.sourceWithGroups, soundChangeCmd.targetWithGroups, wordMD.word.substring(wordMD.conditionalMatchStart[i], wordMD.conditionalMatchEnd[i] + 1), 
                            //wordMD.SCcommand.outputGroupOrder
                            '');
                            endResult += res;
                            affectedSegments += res;
                        }
                    }
                    if (!soundChangeCmd.sourceIsEmpty) // if we are replacing existing segments
                     {
                        outInfoWordMatches += wordMD.word[j];
                        if (j < wordMD.conditionalMatchStart[i] || j > wordMD.conditionalMatchEnd[i] || !evenAndOddConditions) {
                            endResult += wordMD.word[j];
                            affectedSegments += wordMD.word[j];
                        }
                    }
                    indexOfLastMatch = i;
                    if (j == wordMD.conditionalMatchEnd[i]) {
                        jStart = j + 1;
                        if (evenAndOddConditions) {
                            outInfoWordMatches += '>';
                            affectedSegments += '>';
                        }
                        break;
                    }
                }
        }
        let finalstring = wordMD.word.substring(wordMD.conditionalMatchEnd[indexOfLastMatch] + 1);
        outInfoWordMatches += finalstring;
        endResult += finalstring;
        affectedSegments += finalstring;
        logmsg += outInfoWordMatches;
        logmsg += '\' with parity ' + soundChangeCmd.pruning + '; rtl?: ' + soundChangeCmd.matchDirection + '; length ' + wordMD.word.length;
        logmsg += ' resulting in \'' + endResult + '\' from input \'' + wordMD.word + '\'';
        if (wordMD.logging) {
            lo(g.SCA, '' + sca(logmsg, segments.innerToSeg));
            lo(g.SCA, '		' + sca(wordMD.prematchedWord, segments.innerToSeg) + ' <- precondition matches');
            lo(g.SCA, '		' + sca(wordMD.postmatchedWord, segments.innerToSeg) + ' <- postcondition matches');
        }
        wordMD.outInfo =
            {
                origWord: wordMD.word,
                wordMatches: affectedSegments
            };
        wordMD.word = endResult;
    }
    static generateOutputSegments(segments, fromSegs, toSegs, matchedString, order) {
        let result = '';
        //the arguments should already be in inner representation
        let toLength = toSegs.length;
        if (toLength == 0)
            return result;
        let fromGroupsNames = [];
        let fromGroupsIndices = []; //used for group reordering
        fromGroupsNames[0] = '';
        fromGroupsIndices[0] = 0;
        let fromLength = fromSegs.length;
        let fromNoGroupsIndex = 0;
        //find if there any segments in the source
        if (fromLength > 0)
            for (let i = 0; i < fromLength; i++) {
                for (let j = Math.min(segments.maxSegLength, fromLength - i); j > 0; j--) //give priority to longer sequences first
                 {
                    let sequence = fromSegs.substring(i, i + j);
                    if (segments.groups[sequence] !== undefined) {
                        fromGroupsNames[fromGroupsNames.length] = sequence;
                        fromGroupsIndices[fromGroupsIndices.length] = fromNoGroupsIndex;
                        i += j - 1;
                        j = -10; //break from the inner for
                    }
                }
                fromNoGroupsIndex++;
            }
        let useOrder = false;
        if (order.length + 1 >= fromGroupsNames.length)
            useOrder = true;
        let inputGroupIndex = 1;
        let inputGroupIndexOrder = 0;
        for (let i = 0; i < toLength; i++) //iterate all output segments one by one
         {
            let found = false;
            let outputGroup = '';
            for (let j = Math.min(segments.maxSegLength, toLength - i); j > 0; j--) //give priority to longer sequences first
             {
                let sequence = toSegs.substring(i, i + j);
                if (segments.groups[sequence] !== undefined) {
                    found = true;
                    outputGroup = sequence;
                    i += j - 1;
                    j = -10; //break from the inner for
                }
            }
            if (fromLength == 0 && found) // generating new segments
             {
                let groupString = segments.groups[outputGroup];
                if (segments.groupRatios[outputGroup] === undefined || segments.groupRatios[outputGroup] === null) {
                    let index = UTL.randInt0toNincl(groupString.length - 1);
                    result += groupString[index];
                }
                else {
                    let final = arr(segments.groupRatios[outputGroup]).length - 1;
                    let index = UTL.randInt0toNincl(segments.groupRatios[outputGroup][final]);
                    for (let q = 0; q <= final; q++) {
                        if (index <= segments.groupRatios[outputGroup][q]) {
                            result += segments.groups[outputGroup][q];
                            q = final + 2; //break from innermost for
                        }
                    }
                }
            }
            else if (found && fromGroupsNames.length > 1) //changing segments
             {
                let inputChar = '';
                let targetIndex = 0;
                if (!useOrder) {
                    inputChar = matchedString[fromGroupsIndices[inputGroupIndex]];
                    targetIndex = (segments.groups[fromGroupsNames[inputGroupIndex]]).search(inputChar);
                }
                else {
                    if (isNaN(Number(order[inputGroupIndexOrder]))) {
                        lo(g.error, "Error: order of metathesis includes non numerical characters: " + order);
                        return result;
                    }
                    let newOrder = Number(order[inputGroupIndexOrder]);
                    inputChar = matchedString[fromGroupsIndices[newOrder]];
                    targetIndex = (segments.groups[fromGroupsNames[newOrder]]).search(inputChar);
                }
                if (targetIndex == -1) {
                    lo(g.error, "Error: Logic error in group replacement - couldn't find " + inputChar + " in " + segments.groups[fromGroupsNames[inputGroupIndex]]);
                    lo(g.error, "i.e. " + innerToHumanReadable(segments, inputChar) + " in " + innerToHumanReadable(segments, segments.groups[fromGroupsNames[inputGroupIndex]]));
                }
                else {
                    targetIndex = Math.min(targetIndex, (segments.groups[outputGroup]).length - 1);
                    result += segments.groups[outputGroup][targetIndex];
                }
                inputGroupIndex++;
                inputGroupIndexOrder++;
                inputGroupIndex = Math.min(inputGroupIndex, fromGroupsNames.length - 1);
                inputGroupIndexOrder = Math.min(inputGroupIndexOrder, order.length - 1);
            }
            if (!found) //no group found in the target - so just adding the segment
                result += toSegs[i];
        } //end of all-segments-iteration loop
        return UTL.replaceAll(result, sca('_', segments.segToInner), ' ');
        //use '_' to split a word into two
    }
}
//# sourceMappingURL=sca.js.map