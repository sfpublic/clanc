"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class DICTIONARY {
    constructor() {
        this.options = {};
        this.element = null;
        this.conlang = null;
        this.columnFilters = {};
        this.previousCellValue = null;
        this.wordTypes = [];
        this.defaultWordType = 0;
        this.SGcellCSSkey = 'SGcellCSSkey';
        this.SGcellCSS = {};
        this.DBidToSGrowMap = [];
        this.slickgrid = null;
        this.columns = null;
        this.dataview = null;
        this.initialised = false;
    }
    /// if the init fails, the caller should dereference the object
    async init(element) {
        this.conlang = SESSION.getactiveconlang();
        if (this.conlang == null)
            return lo(g.error, 'conlang is not set', { error: true, msg: 'conlang is not set' });
        if (SESSION.file == null)
            return lo(g.error, 'no file is open', { error: true, msg: 'no file is open' });
        if (element.divId == null)
            return lo(g.error, 'html element is not properly initialised', { error: true, msg: 'html element is not properly initialised' });
        let rawWTdata = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + this.conlang.dbid);
        for (let i in rawWTdata)
            this.wordTypes[rawWTdata[i].id] = rawWTdata[i];
        this.defaultWordType = rawWTdata[0].id;
        try {
            this.dataview = new Slick.Data.DataView();
            this.dataview.parent = this;
            this.dataview.setItems([]);
            this.dataview.onRowCountChanged.subscribe(function (e, args) {
                if (args.dataView.parent.slickgrid == null)
                    return lo(g.error, 'dataview slickgrid null or undefined', false);
                args.dataView.parent.slickgrid.updateRowCount();
                args.dataView.parent.slickgrid.render();
            });
            this.dataview.onRowsChanged.subscribe(function (e, args) {
                if (args.dataView.parent.slickgrid == null)
                    return lo(g.error, 'dataview slickgrid null or undefined', false);
                args.dataView.parent.slickgrid.invalidateRows(args.rows);
                args.dataView.parent.slickgrid.render();
            });
            this.columns = [];
            let keys = Object.keys(dictionary_sch);
            for (let i = 0; i < keys.length; i++) {
                this.columns.push(DICTIONARY.createcolumnfromschema(keys[i], TABLE_SCHEMAS.entry.cols[keys[i]], dictionary_sch[keys[i]], {
                    validator: this.validator_word_type,
                    formatter: this.wordTypeFormatter,
                    callee: this,
                    editor: wordTypeEditor
                }));
            }
            this.options =
                {
                    editable: true,
                    enableAddRow: true,
                    enableCellNavigation: true,
                    asyncEditorLoading: false,
                    autoEdit: false,
                    multiColumnSort: true,
                    numberedMultiColumnSort: true,
                    tristateMultiColumnSort: true,
                    sortColNumberInSeparateSpan: false,
                    showHeaderRow: true,
                    headerRowHeight: 30,
                    explicitInitialization: true
                };
            this.slickgrid = new Slick.Grid("#" + element.divId, this.dataview, this.columns, this.options);
            this.slickgrid.setSelectionModel(new Slick.CellSelectionModel());
            SESSION.file.db.getdata(table_e.entry, ["*"], "`conlang` = " + this.conlang.dbid).
                then((data) => {
                this.dataview.beginUpdate();
                this.dataview.setItems(data);
                this.dataview.endUpdate();
                //TODO: fail the initialisation if either the query or data update fails
            }).
                catch(LOG.caughtError);
            UTL.subscribeWithProps(this.slickgrid.onAddNewRow.subscribe, (props, e, args) => {
                props.callee.addOrUpdateEntry(args, true);
            }, { callee: this });
            UTL.subscribeWithProps(this.slickgrid.onCellChange.subscribe, (props, e, args) => {
                let wordIsChanged = (this.columns[args.cell].name == 'word');
                props.callee.addOrUpdateEntry(args, false, wordIsChanged);
            }, { callee: this });
            UTL.subscribeWithProps(this.slickgrid.onBeforeEditCell.subscribe, (props, e, args) => {
                props.callee.previousCellValue = (args.item && args.item["root"]) || null;
            }, { callee: this });
            UTL.subscribeWithProps(this.slickgrid.onSort.subscribe, (props, e, args) => {
                let cols = args.sortCols;
                for (let i = 0, l = cols.length; i < l; i++)
                    props.callee.dataview.fastSort(cols[i].sortCol.field, cols[i].sortAsc);
                DICTIONARY.updateWordStatesInGrid(props.callee, props.callee.dataview.getItems());
                props.callee.updateEntries();
            }, { callee: this });
            UTL.subscribeWithProps(this.slickgrid.onHeaderRowCellRendered.subscribe, (props, e, args) => {
                let id = UTL.randomstring(20);
                args.node.innerHTML = `<input type='text' id="${id}" placeholder=" search/filter"></input>`;
                args.node.onkeyup = () => { props.callee.updateColumnFilters(id, args.column.id); };
            }, { callee: this });
            UTL.subscribeWithProps(this.slickgrid.onActiveCellChanged.subscribe, (props, e, args) => {
                let selectedWord = '';
                let wordType = -1;
                if (args && props.dataView.getItem(args.row)) {
                    selectedWord = props.dataView.getItem(args.row).root;
                    wordType = props.dataView.getItem(args.row).part_of_speech;
                }
                IF_MORPHOLOGY.refreshPreviewOnly(wordType, selectedWord);
            }, {
                callee: this,
                dataView: this.dataview
            });
            this.slickgrid.init();
            let localfilter = function (item) {
                for (let id in localfilter.callee.columnFilters) {
                    if (id !== undefined && localfilter.callee.columnFilters[id] !== "") {
                        let c = localfilter.callee.slickgrid.getColumns()[localfilter.callee.slickgrid.getColumnIndex(id)];
                        if (id == 'part_of_speech') {
                            if (localfilter.callee.wordTypes[item[c.field]].abbreviation.
                                indexOf(localfilter.callee.columnFilters[id]) < 0)
                                return false;
                        }
                        else {
                            if (item[c.field].indexOf(localfilter.callee.columnFilters[id]) < 0)
                                return false;
                        }
                    }
                }
                return true;
            };
            localfilter.callee = this;
            this.dataview.beginUpdate();
            this.dataview.setFilter(localfilter);
            this.dataview.endUpdate();
            this.initialised = true;
            UTL.getElem(element.divId).onkeydown =
                UTL.assignWithProps(this.keyEventListener, { owner: this });
        }
        catch (error) {
            lo(g.error, error.toString());
            return { error: true, msg: error.toString() };
        }
        return UTL.res_success;
    }
    addOrUpdateEntry(args, addition, wordIsChanged = false) {
        let item = args.item;
        let newDBValue = false;
        if (SESSION.file == null)
            return lo(g.error, 'no file is open', { error: true, msg: 'no file is open' });
        let promise;
        for (let cell in item) {
            if (typeof cell == "string")
                cell = cell.trim();
        }
        if (addition || this.previousCellValue == null || this.previousCellValue.length < 1) {
            newDBValue = true;
            item.conlang = this.conlang.dbid;
            item.state = 0;
            item.part_of_speech = this.defaultWordType;
            item.diachronic_id = UTL.randomint(4294967296);
            if (item.root && !item.translations)
                item.translations = "";
            promise = SESSION.file.db.savedata(table_e.entry, item).
                then((res) => {
                return SESSION.file.db.getid(table_e.entry, "`conlang` = " + this.conlang.dbid +
                    " AND `root` LIKE \"" + UTL.escapeCharacters(item.root) + "\"");
            });
        }
        else {
            promise = SESSION.file.db.updatedata(table_e.entry, item, "`id` = " + args.item["id"]).
                then(() => {
                return args.item["id"];
            });
        }
        promise.then(async (res) => {
            if (!UTL.verifyID(res))
                return Preject('db query failed with an invalid id: ' + res);
            if (addition) {
                item.id = res;
                this.dataview.addItem(item);
            }
            else {
                let cell = this.slickgrid.getActiveCell();
                this.slickgrid.removeCellCssStyles("newEntry" + cell.row);
            }
            if (wordIsChanged && SESSION.getactiveconlang().protolangid) {
                let newstate = args.item["state"] | entry_state_e.irregular_derivation;
                newstate &= ~entry_state_e.not_up_to_date;
                await SESSION.file.db.updatedata(table_e.entry, {
                    state: newstate
                }, "`id` = " + args.item["id"]).catch(LOG.caughtError);
                args.item["state"] = newstate;
                this.setSGcellCSS(this.DBidToSGrowMap[args.item["id"]], newstate);
            }
            this.updateEntries();
            return UTL.res_success;
        }).
            catch((res) => {
            if (newDBValue && !item.root) {
                // expected issue - code will take care of it
                // so don't display an error message
            }
            else
                INTERFACE.displayError(res);
            if (addition) {
                item.id = UTL.generateNewId();
                this.dataview.addItem(item);
            }
            let newrow = {};
            let cell = this.slickgrid.getActiveCell();
            newrow[cell.row] = {};
            for (let index in dictionary_sch) {
                newrow[cell.row][index] = "newEntry";
            }
            this.slickgrid.setCellCssStyles("newEntry" + cell.row, newrow);
            this.updateEntries();
            this.slickgrid.getCellNode(cell.row, 1).innerHTML = "add word to save entry";
        });
    }
    updateEntries() {
        this.slickgrid.invalidate();
    }
    updateColumnFilters(elemId, colId) {
        let elem = UTL.getInputElem(elemId);
        if (elem == null) {
            lo(g.error, "no element with id of " + elemId + " to filter by!");
            return;
        }
        this.columnFilters[colId] = elem.value.trim();
        this.dataview.refresh();
    }
    static createcolumnfromschema(col, schemacol, tablecol, refopts) {
        let column = {
            name: tablecol.name,
            width: tablecol.width,
            sortable: true,
            hidden: false
        };
        if (tablecol.hidden) {
            column.width = 0;
            column.maxWidth = 0;
            column.minWidth = 0;
            column.focusable = false;
            column.resizable = false;
            column.selectable = false;
            column.headerCssClass = 'sgHidden';
            column.cssClass = 'sgHidden';
        }
        column.id = col;
        column.field = col;
        switch (schemacol.type) {
            case db_type_t.txt:
            case db_type_t.ltxt:
                if (schemacol.flags & notnull_p)
                    column.validator = this.validator_txt_notnull;
                else
                    column.validator = this.validator_txt;
                break;
            case db_type_t.ref:
                if (schemacol.flags & notnull_p)
                    column.validator = this.validator_ref_notnull;
                else
                    column.validator = this.validator_ref;
                break;
            case db_type_t.bool:
                column.validator = this.validator_bool_notnull;
                break;
        }
        column.editor = Slick.Editors.Text;
        if (schemacol.type == db_type_t.ref && schemacol.references == table_e.word_type && refopts) {
            column.validator = refopts.validator;
            column.formatter = refopts.formatter;
            column.editor = refopts.editor;
            column.callee = refopts.callee;
        }
        return column;
    }
    async resyncToDB() {
        if (SESSION.file == null)
            return Preject('no file is currently open');
        return SESSION.file.db.getdata(table_e.entry, ["*"], "`conlang` = " + this.conlang.dbid).
            then(async (data) => {
            this.wordTypes = [];
            let rawWTdata = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + this.conlang.dbid);
            for (let i in rawWTdata)
                this.wordTypes[rawWTdata[i].id] = rawWTdata[i];
            this.defaultWordType = rawWTdata[0].id;
            this.dataview.beginUpdate();
            this.dataview.setItems(data);
            this.dataview.endUpdate();
            DICTIONARY.updateWordStatesInGrid(this, data);
            //TODO: fail the resync if either the query or data update fails
            return UTL.res_success;
        });
    }
    static updateWordStatesInGrid(ref, data) {
        ref.SGcellCSS = {};
        let datalen = data.length;
        for (let i = 0; i < datalen; i++) {
            ref.DBidToSGrowMap[data[i].id] = i;
            ref.setSGcellCSS(i, data[i].state);
        }
        ref.slickgrid.setCellCssStyles(ref.SGcellCSSkey, ref.SGcellCSS);
    }
    static setSGcellCSS(SGcellCSS, row, state) {
        SGcellCSS[row] = { root: '' };
        if (state & entry_state_e.irregular_derivation)
            SGcellCSS[row].root += ' sgCellIrregularDerivation';
        if (state & entry_state_e.not_up_to_date)
            SGcellCSS[row].root += ' sgCellNotUpToDate';
    }
    setSGcellCSS(row, state) {
        DICTIONARY.setSGcellCSS(this.SGcellCSS, row, state);
    }
    keyEventListener(props, e) {
        var _a;
        if ((e.shiftKey && e.key == "Enter") || e.key == "Insert") {
            let rows = props.owner.slickgrid.getSelectedRows();
            if (rows.length > 0) {
                let row = rows[0] + 1;
                let newWord = { id: UTL.generateNewId() };
                props.owner.dataview.insertItem(row, newWord);
                props.owner.slickgrid.setActiveCell(row, 1);
                props.owner.slickgrid.editActiveCell(props.owner.slickgrid.getCellEditor());
            }
        }
        else if (e.key == 'Delete' && UTL.confirmdatadeletion()) {
            let cell = props.owner.slickgrid.getActiveCell();
            let editor = props.owner.slickgrid.getCellEditor();
            if (cell && !editor && ((_a = document.activeElement) === null || _a === void 0 ? void 0 : _a.tagName) != "INPUT") {
                let rows = props.owner.slickgrid.getSelectedRows();
                let dbids = [];
                try {
                    dbids = rows.map((x) => props.owner.dataview.getItemByIdx(x).id);
                    SESSION.file.db.deleterowsbyid(table_e.entry, dbids);
                    props.owner.resyncToDB();
                }
                catch (err) {
                    lo(g.error, err.toString());
                    INTERFACE.displayError("The selection to delete is invalid. Please reselect.");
                }
                e.preventDefault();
                e.stopPropagation();
            }
        }
    }
    static validator_txt(value) {
        if (typeof value !== "string")
            return { valid: false, msg: "This field should be string." };
        else
            return { valid: true, msg: null };
    }
    static validator_txt_notnull(value) {
        if (value == null || value == undefined || !value.length)
            return { valid: false, msg: "This is a required field." };
        else if (typeof value !== "string")
            return { valid: false, msg: "This field should be string." };
        else
            return { valid: true, msg: null };
    }
    static validator_ref(value) {
        //TODO
        return { valid: false, msg: "This validator isn't implemented yet." };
    }
    static validator_ref_notnull(value) {
        //TODO
        return { valid: false, msg: "This validator isn't implemented yet." };
    }
    static validator_bool_notnull(value) {
        if (value !== true && value !== false)
            return { valid: false, msg: "This bool field should be either true or false." };
        else
            return { valid: true, msg: null };
    }
    static validator_any(value) {
        return { valid: true, msg: null };
    }
    static validator_any_notnull(value) {
        if (value == null || value == undefined || !value.length)
            return { valid: false, msg: "This is a required field" };
        else
            return { valid: true, msg: null };
    }
    validator_word_type(value) {
        if (!this.wordTypes)
            return { valid: false, msg: "Lexicon not initialised with word types" };
        if (!this.wordTypes[value])
            return { valid: false, msg: "Invalid word type" };
        else
            return { valid: true, msg: null };
    }
    wordTypeFormatter(row, cell, value, columnDef, dataContext) {
        if (columnDef.callee.wordTypes)
            if (columnDef.callee.wordTypes[value])
                return columnDef.callee.wordTypes[value].abbreviation;
        return 'err' + value;
    }
}
class wordTypeEditor {
    constructor(args) {
        this.initialised = false;
        this.initval = 0;
        this.wordTypes = args.column.callee.wordTypes;
        this.args = args;
        this.id = UTL.randomstring(15);
        this.init();
    }
    async init() {
        let ids = [];
        let names = [];
        let wtl = this.wordTypes.length;
        for (let i = 0; i < wtl; i++) {
            if (!this.wordTypes[i])
                continue;
            ids.push(this.wordTypes[i].id);
            names.push(this.wordTypes[i].name);
        }
        let html = INTERFACE_STATICS.barebonesDropdown(names, ids, this.id);
        this.args.container.innerHTML = html;
        setTimeout(this.completeInit, 5, this);
    }
    completeInit(callee) {
        callee.dropdown = UTL.getElem(callee.id);
        callee.dropdown.value = callee.initval;
        callee.initialised = true;
        callee.focus();
    }
    destroy() {
        if (!this.initialised)
            return;
        this.dropdown.outerHTML = '';
    }
    focus() {
        if (!this.initialised)
            return;
        this.dropdown.focus();
    }
    serializeValue() {
        if (!this.initialised)
            return;
        return this.dropdown.value;
    }
    // value is whatever is returned from serializeValue()
    applyValue(item, value) {
        if (!this.initialised)
            return;
        item.part_of_speech = Number(value);
    }
    loadValue(item) {
        this.initval = item.part_of_speech;
        if (!this.initialised)
            return;
        this.dropdown.value = item.part_of_speech;
    }
    isValueChanged() {
        if (!this.initialised)
            return false;
        return (this.args.item.part_of_speech != this.dropdown.value);
    }
    validate() {
        if (!this.initialised)
            return { valid: false, msg: 'initialisation not complete' };
        if (!this.wordTypes[this.dropdown.value])
            return { valid: false, msg: "Invalid word type selected" };
        return { valid: true, msg: null };
    }
}
//# sourceMappingURL=dictionary.js.map