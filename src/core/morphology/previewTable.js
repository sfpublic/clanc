"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class dummyPreviewTable_t {
    constructor() {
        this.table = [];
    }
}
class previewTable_t {
    constructor(segments) {
        this.table = [];
        this.getLength = () => { return this.table.length; };
        this.getItem = (id) => { return this.table[id]; };
        this.notify = () => { };
        this.segments = segments;
    }
    async init(slotIds, wordToInflect) {
        let indexedData = IF_MORPHOLOGY.paradigmDataBackup;
        if (indexedData == null) {
            INTERFACE.displayError("Fatal error - indexed word type data is null");
            return this;
        }
        if (slotIds.length < 0) {
            INTERFACE.displayError("Fatal error - no slot ids provided for table construction");
            return this;
        }
        let slotData = [];
        let indices = [];
        let numCategories = 0;
        for (let s in slotIds) {
            let slot = indexedData.indexedSlots[slotIds[s]];
            let slotCategoryIds = [...slot.categoryIds];
            slotCategoryIds = slotCategoryIds.filter(x => !slot.missing_categoryIds.includes(x));
            let categoryPos = [];
            for (let i in slot.category_positions)
                categoryPos[slot.category_positions[i].id] = slot.category_positions[i].position;
            slotCategoryIds.sort((a, b) => { return categoryPos[a] - categoryPos[b]; });
            if (slotCategoryIds.length == 0) {
                // there isn't any morphology for the slot yet, disregard it
                continue;
            }
            numCategories += slotCategoryIds.length;
            for (let j in slotCategoryIds) {
                indices.push({ slot: slotIds[s], category: slotCategoryIds[j] });
            }
            let res = previewTable_t.slotToCartGrammemes(slotCategoryIds);
            let morphemeIds = await SESSION.file.db.MTMgetIdsFromIdCombos(DBMTMs['m_grammemes'], table_e.grammeme, res, 'slot = ' + slot.id);
            if (res.length != morphemeIds.length) {
                lo(g.error, 'Not all grammeme combinations have a corresponding morpheme: ' +
                    res.length + " vs. " + morphemeIds.length);
                return;
            }
            let singleSlotData = [];
            let mdl = morphemeIds.length;
            for (let i = 0; i < mdl; i++) {
                singleSlotData.push(indexedData.indexedMorphemes[morphemeIds[i]].SCs);
            }
            slotData.push(singleSlotData);
        }
        let wordforms;
        if (wordToInflect)
            // TODO: Optimise. Many sound changes will get executed multiple times.
            // It's probably some kind of exponential complexity.
            wordforms = slotData.reduce((accum, curVal) => accum.flatMap((accEl) => curVal.flatMap((cvEl) => { return accEl + '\n' + cvEl; })), [[]]);
        else
            wordforms = slotData.reduce((accum, curVal) => accum.flatMap((accEl) => curVal.flatMap((cvEl) => { return accEl + MORPHOLOGY.morphemeSCsToPreview(cvEl); })), [[]]);
        switch (numCategories) {
            case 0:
                break;
            case 1:
                previewTable_t.construct1DPreviewTable(this, wordforms, indices[0], this.segments, wordToInflect);
                break;
            case 2:
                previewTable_t.construct2DPreviewTable(this, wordforms, indices, 0, 1, this.segments, wordToInflect);
                break;
            default:
                previewTable_t.constructMultidimPreviewTable(this, wordforms, indices, this.segments, wordToInflect);
                break;
        }
    }
    static slotToCartGrammemes(categoryIds) {
        let cartProdInput = [];
        let ics = IF_MORPHOLOGY.paradigmDataBackup.indexedCategories;
        let igs = IF_MORPHOLOGY.paradigmDataBackup.indexedGrammemes;
        for (let i in categoryIds) {
            let grmIds = [...ics[categoryIds[i]].grammemeIds];
            grmIds.sort((a, b) => { return igs[a].position - igs[b].position; });
            cartProdInput.push(grmIds);
        }
        return UTL.cartProdLeft(cartProdInput);
    }
    static construct1DPreviewTable(newTable, wordforms, index, segments, wordToInflect) {
        let indexedData = IF_MORPHOLOGY.paradigmDataBackup;
        //TODO: stop using global vars and pass it around instead
        let grms = indexedData.indexedCategories[index.category].grammemeIds;
        let l = grms.length;
        grms.sort((a, b) => {
            return indexedData.indexedGrammemes[a].position -
                indexedData.indexedGrammemes[b].position;
        });
        for (let i = 0; i < l; i++) {
            if (wordToInflect)
                newTable.table.push([
                    indexedData.indexedGrammemes[grms[i]].gloss,
                    PARSER.executeCommandsOnASingleWord(wordToInflect, segments + '\nⱲ = ⱳ\n' + wordforms[i]).word
                ]);
            //TODO: figure out why the last segment group is not reversed to surface representation in this case
            //maybe it is if an affix contains it
            else
                newTable.table.push([
                    indexedData.indexedGrammemes[grms[i]].gloss,
                    wordforms[i]
                ]);
        }
    }
    static construct2DPreviewTable(newTable, wordforms, indices, offset = 0, step = 1, segments, wordToInflect) {
        let indexedData = IF_MORPHOLOGY.paradigmDataBackup;
        //TODO: stop using global vars and pass it around instead
        let indexedCategories = indexedData.indexedCategories;
        let colGrmArr;
        let rowGrmArr;
        colGrmArr = [...indexedCategories[indices[1].category].grammemeIds];
        rowGrmArr = [...indexedCategories[indices[0].category].grammemeIds];
        colGrmArr.sort((a, b) => {
            return indexedData.indexedGrammemes[a].position -
                indexedData.indexedGrammemes[b].position;
        });
        rowGrmArr.sort((a, b) => {
            return indexedData.indexedGrammemes[a].position -
                indexedData.indexedGrammemes[b].position;
        });
        let numMorphCols = colGrmArr.length;
        let row = [''];
        for (let i = 0; i < numMorphCols; i++) {
            let id = colGrmArr[i];
            row.push(indexedData.indexedGrammemes[id].gloss);
        }
        newTable.table.push(row);
        let numMorphRows = rowGrmArr.length;
        for (let i = 0; i < numMorphRows; i++) {
            let id = rowGrmArr[i];
            let row = [indexedData.indexedGrammemes[id].gloss];
            for (let j = 0; j < numMorphCols; j++) {
                let index = offset + step * (i * numMorphCols + j);
                if (wordToInflect)
                    row.push(PARSER.executeCommandsOnASingleWord(wordToInflect, segments + '\nⱲ = ⱳ\n' + wordforms[index]).word);
                //TODO: figure out why the last segment group is not reversed to surface representation in this case
                //maybe it is if an affix contains it
                else
                    row.push(wordforms[index]);
            }
            newTable.table.push(row);
        }
    }
    static constructMultidimPreviewTable(newTable, wordforms, indices, segments, wordToInflect) {
        let indexedData = IF_MORPHOLOGY.paradigmDataBackup;
        //TODO: stop using global vars and pass it around instead
        let indexedCategories = indexedData.indexedCategories;
        let offset = 0;
        let step = 1;
        let firstTwoIndices = [
            indices[0],
            indices[1]
        ];
        indices.splice(0, 2);
        let cartProdInput = [];
        for (let i in indices) {
            let cartProdRow = [];
            let grmArr;
            grmArr = [...indexedCategories[indices[i].category].grammemeIds];
            grmArr.sort((a, b) => {
                return indexedData.indexedGrammemes[a].position -
                    indexedData.indexedGrammemes[b].position;
            });
            step *= grmArr.length;
            for (let j in grmArr)
                cartProdRow.push({
                    slot: indices[i].slot,
                    grammeme: grmArr[j]
                });
            cartProdInput.push(cartProdRow);
        }
        let grmSlotCombos = UTL.cartProdLeft(cartProdInput);
        for (let i in grmSlotCombos) {
            let combo = grmSlotCombos[i];
            let title = '';
            let prevSlot = -1;
            for (let j in combo) {
                if (combo[j].slot == prevSlot)
                    title += '.';
                else if (prevSlot >= 0)
                    title += '-';
                title += indexedData.indexedGrammemes[combo[j].grammeme].gloss;
                prevSlot = combo[j].slot;
            }
            let dummyTable = new dummyPreviewTable_t();
            previewTable_t.construct2DPreviewTable(dummyTable, wordforms, firstTwoIndices, offset, step, segments, wordToInflect);
            dummyTable.table[0][0] = title;
            newTable.table = newTable.table.concat(dummyTable.table).concat([[]]);
            offset++;
        }
    }
}
//# sourceMappingURL=previewTable.js.map