"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class indexedMorphologyData_t {
    constructor() {
        this.indexedWordTypes = [];
        this.indexedTables = [];
        this.indexedSlots = [];
        this.indexedCategories = [];
        this.indexedMorphemes = [];
        this.indexedGrammemes = [];
        this.indexedSlotOverwrittenCategories = [];
    }
}
class MORPHOLOGY {
    constructor() {
        return lo(g.error, "This object is only to be used statically.", null);
    }
    static async aggregateWordTypeData(word_type_id, conlang) {
        let wtypeData = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + conlang).catch(LOG.caughtError);
        let wtypeDatalength = wtypeData.length;
        let indexedWordTypes = [];
        for (let i = 0; i < wtypeDatalength; i++) {
            indexedWordTypes[wtypeData[i].id] = new word_type_t(wtypeData[i]);
        }
        wtypeData = undefined; // don't work with it anymore, not all members are defined
        if (indexedWordTypes[word_type_id] == null)
            return null; // this should never happen, if the function is called properly
        for (let i in indexedWordTypes) {
            if (indexedWordTypes[i].inherits_from) {
                indexedWordTypes[indexedWordTypes[i].inherits_from].subtypes.push(indexedWordTypes[i]);
            }
        }
        // we always have to parse the tree from its very top,
        // in order to get correct inheritance and overwrite effects
        // so, find the top of the queried node, and start populating node data from there
        // later, just return the queried node itself
        let top = indexedWordTypes[word_type_id].inherits_from;
        let treeTop = indexedWordTypes[word_type_id];
        while (top != null) {
            let oldTop = treeTop;
            treeTop = indexedWordTypes[top];
            treeTop.subtypes = [oldTop]; // prune the subtypes we're not interested in
            top = indexedWordTypes[top].inherits_from;
        }
        let wordTypeIds = MORPHOLOGY.aggregateNodeIds([], treeTop);
        let tablesData = await SESSION.file.db.getdata(table_e.paradigm_table, ['*'], 'word_type IN (' + wordTypeIds.join(', ') + ')').catch(LOG.caughtError);
        let tablesDataLength = tablesData.length;
        let tableIds = [];
        for (let i = 0; i < tablesDataLength; i++)
            tableIds.push(tablesData[i].id);
        let indexedTables = [];
        for (let i = 0; i < tablesDataLength; i++) {
            let tableId = tablesData[i].id;
            indexedTables[tableId] = new paradigm_table_t(tablesData[i]);
            tablesData[i] = undefined; // don't work with it anymore, doesn't have all the fields
            if (indexedTables[tableId].word_type != null) // should always happen
             {
                let wtype = indexedTables[tableId].word_type;
                indexedWordTypes[wtype].paradigm_tables.push(indexedTables[tableId]);
            }
        }
        tablesData = undefined; // don't work with it anymore, doesn't have all the fields
        let slotsData = await SESSION.file.db.getdata(table_e.slot, ['*'], 'paradigm_table IN (' + tableIds.join(', ') + ')').catch(LOG.caughtError);
        let slotIds = [];
        let slotsDataLength = slotsData.length;
        let indexedSlots = [];
        for (let i = 0; i < slotsDataLength; i++) {
            let slotId = slotsData[i].id;
            slotIds.push(slotId);
            indexedSlots[slotId] = new slot_t(slotsData[i]);
            slotsData[i] = undefined; // don't work with it anymore, doesn't have all the fields
            if (indexedSlots[slotId].paradigm_table != null) // should always happen
             {
                let table = indexedSlots[slotId].paradigm_table;
                indexedTables[table].slots.push(indexedSlots[slotId]);
            }
        }
        slotsData = undefined; // don't work with it anymore, doesn't have all the fields
        let categoryData = await SESSION.file.db.getdata(table_e.category, ['*'], 'conlang = ' + conlang).catch(LOG.caughtError);
        let categoryIds = [];
        let categoryDataLength = categoryData.length;
        let indexedCategories = [];
        for (let i = 0; i < categoryDataLength; i++) {
            let categoryId = categoryData[i].id;
            categoryIds.push(categoryId);
            indexedCategories[categoryId] = new category_t(categoryData[i]);
            categoryData[i] = undefined; // don't work with it anymore, doesn't have all the fields
        }
        categoryData = undefined; // don't work with it anymore, doesn't have all the fields
        let grammemeData = await SESSION.file.db.getdata(table_e.grammeme, ['*'], 'category IN (' + categoryIds.join(', ') + ')').catch(LOG.caughtError);
        let grammemeDataLength = grammemeData.length;
        let indexedGrammemes = [];
        for (let i = 0; i < grammemeDataLength; i++) {
            let grammemeId = grammemeData[i].id;
            indexedGrammemes[grammemeId] = new grammeme_t(grammemeData[i]);
            grammemeData[i] = undefined; // don't work with it anymore, doesn't have all the fields
            indexedCategories[indexedGrammemes[grammemeId].category].grammemeIds.push(grammemeId);
        }
        grammemeData = undefined; // don't work with it anymore, doesn't have all the fields
        let morphemeData = await SESSION.file.db.getdata(table_e.morpheme, ['*'], 'slot IN (' + slotIds.join(', ') + ')').catch(LOG.caughtError);
        let morphemeDataLength = morphemeData.length;
        let indexedMorphemes = [];
        for (let i = 0; i < morphemeDataLength; i++) {
            let morphemeId = morphemeData[i].id;
            indexedMorphemes[morphemeId] = new morpheme_t(morphemeData[i]);
            morphemeData[i] = undefined; // don't work with it anymore, doesn't have all the fields
        }
        morphemeData = undefined; // don't work with it anymore, doesn't have all the fields
        let slotOWData = await SESSION.file.db.getdata(table_e.slot_overwritten_categories, ['*'], 'word_type IN (' + wordTypeIds.join(', ') + ')').catch(LOG.caughtError);
        let slotOWDataLength = slotOWData.length;
        let indexedSlotOverwrittenCategories = [];
        for (let i = 0; i < slotOWDataLength; i++) {
            let slotOWId = slotOWData[i].id;
            indexedSlotOverwrittenCategories[slotOWId] = new slot_overwritten_categories_t(slotOWData[i]);
            slotOWData[i] = undefined; // don't work with it anymore, doesn't have all the fields
        }
        slotOWData = undefined; // don't work with it anymore, doesn't have all the fields
        for (let i in indexedSlots) {
            let slotId = indexedSlots[i].id;
            let cdata = await SESSION.file.db.MTMgetrows(DBMTMs['slot_categories'], table_e.slot, slotId).catch(LOG.caughtError);
            let mcdata = await SESSION.file.db.MTMgetids(DBMTMs['slot_missing_categories'], table_e.slot, slotId).catch(LOG.caughtError);
            for (let c in cdata) {
                indexedSlots[slotId].categoryIds.push(cdata[c].category);
                indexedSlots[slotId].category_positions.push({
                    id: cdata[c].category,
                    position: cdata[c].position,
                    mtmid: cdata[c].id
                });
            }
            for (let mc in mcdata)
                indexedSlots[slotId].missing_categoryIds.push(mcdata[mc].category);
        }
        for (let i in indexedWordTypes) {
            let wtId = indexedWordTypes[i].id;
            let mtdata = await SESSION.file.db.MTMgetrows(DBMTMs['wt_missing_tables'], table_e.word_type, wtId).catch(LOG.caughtError);
            let msdata = await SESSION.file.db.MTMgetrows(DBMTMs['wt_missing_slots'], table_e.word_type, wtId).catch(LOG.caughtError);
            for (let mt in mtdata)
                indexedWordTypes[wtId].missing_tables.push(mtdata[mt].paradigm_table);
            for (let ms in msdata)
                indexedWordTypes[wtId].missing_slots.push(msdata[ms].slot);
        }
        for (let i in indexedMorphemes) {
            let morphemeId = indexedMorphemes[i].id;
            let gdata = await SESSION.file.db.MTMgetids(DBMTMs['m_grammemes'], table_e.morpheme, morphemeId).catch(LOG.caughtError);
            for (let g in gdata)
                indexedMorphemes[morphemeId].grammemeIds.push(gdata[g].grammeme);
        }
        let indexedData = {
            indexedWordTypes: indexedWordTypes,
            indexedTables: indexedTables,
            indexedSlots: indexedSlots,
            indexedCategories: indexedCategories,
            indexedMorphemes: indexedMorphemes,
            indexedGrammemes: indexedGrammemes,
            indexedSlotOverwrittenCategories: indexedSlotOverwrittenCategories
        };
        await MORPHOLOGY.populateNodeData(treeTop, null, indexedData);
        return PresolveT(indexedData);
    }
    static aggregateNodeIds(ids, node) {
        ids.push(node.id);
        let stl = node.subtypes.length;
        for (let i = 0; i < stl; i++)
            MORPHOLOGY.aggregateNodeIds(ids, node.subtypes[i]);
        return ids;
    }
    static async populateNodeData(wtypeNode, upperWTypeNode, indexedData) {
        if (upperWTypeNode != null) {
            let existingTableIds = [];
            let ptl = wtypeNode.paradigm_tables.length;
            for (let i = 0; i < ptl; i++) {
                existingTableIds.push(wtypeNode.paradigm_tables[i].id);
            }
            ptl = upperWTypeNode.paradigm_tables.length;
            for (let i = 0; i < ptl; i++) {
                if (!existingTableIds.includes(upperWTypeNode.paradigm_tables[i].id))
                    wtypeNode.paradigm_tables.push(upperWTypeNode.paradigm_tables[i]);
            }
        }
        let stl = wtypeNode.subtypes.length;
        for (let i = 0; i < stl; i++)
            await MORPHOLOGY.populateNodeData(wtypeNode.subtypes[i], wtypeNode, indexedData);
    }
    static async updateSlotMorphemes(slotId, indexedData) {
        !indexedData && (indexedData = IF_MORPHOLOGY.paradigmDataBackup);
        if (indexedData == null) {
            INTERFACE.displayError("Fatal error - indexed word type data is null");
            return;
        }
        let slot = indexedData.indexedSlots[slotId];
        // Operation 1 - delete all morphemes, which have grammemes in categories, which do not belong to the slot anymore
        // Step 1 - determine which categories are present for the slot, out of all the current conlang categories
        let categoryIds = [...slot.categoryIds];
        categoryIds = categoryIds.filter(x => !slot.missing_categoryIds.includes(x));
        // Step 2 - get all the grammemes of all the categories not present in the slot
        let allClgCategories = [];
        for (let ctg in indexedData.indexedCategories)
            allClgCategories.push(indexedData.indexedCategories[ctg].id);
        let allNotPresentCategories = allClgCategories.filter(x => !categoryIds.includes(x));
        // Step 3 - out of those grammemes, get the ones with the smallest position in their respective slot (not to be deleted)
        let keepTheseGrammemes = [];
        let deleteRowsWithTheseGrammemes = [];
        let allNotPresentCategoryGrammemes = [];
        for (let ctg in allNotPresentCategories) {
            let keepThisId = -1;
            let smallestPos = null;
            let ctgId = allNotPresentCategories[ctg];
            // TODO: Optimise. Currently standing at O(x^2) complexity
            for (let grm in indexedData.indexedGrammemes) {
                let grmId = indexedData.indexedGrammemes[grm].id;
                if (indexedData.indexedGrammemes[grmId].category != ctgId)
                    continue;
                let grmPos = indexedData.indexedGrammemes[grm].position;
                allNotPresentCategoryGrammemes.push(grmId);
                if (!smallestPos || grmPos < smallestPos) {
                    keepThisId = grmId;
                    smallestPos = grmPos;
                }
            }
            for (let grm in indexedData.indexedGrammemes) {
                let grmId = indexedData.indexedGrammemes[grm].id;
                if (indexedData.indexedGrammemes[grmId].category != ctgId)
                    continue;
                if (grmId == keepThisId)
                    continue;
                deleteRowsWithTheseGrammemes.push(grmId);
            }
            if (keepThisId < 0) {
                INTERFACE.displayError("Fatal error - no grammemes found for category " + ctgId + " during update stage 1");
                return;
            }
            keepTheseGrammemes[ctgId] = keepThisId;
        }
        // TODO: why is indexedData.indexedGrammemes[grm].id NULL sometimes?
        keepTheseGrammemes = keepTheseGrammemes.filter(x => x != null);
        // if there aren't slot categories left anymore, delete everything instead
        if (!categoryIds.length) {
            deleteRowsWithTheseGrammemes = [...deleteRowsWithTheseGrammemes, ...keepTheseGrammemes];
            keepTheseGrammemes = [];
        }
        // Step 4 - get all the morpheme ids for the slot
        let morphemeIds = [];
        for (let i in indexedData.indexedMorphemes) {
            if (indexedData.indexedMorphemes[i].slot == slotId)
                morphemeIds.push(indexedData.indexedMorphemes[i].id);
        }
        // Step 5 - delete all MtM rows for grammeme ids from Step 2
        let rows = await SESSION.file.db.MTMgetwhere(DBMTMs['m_grammemes'], 'morpheme IN (' + morphemeIds.join(', ') + ')' +
            ' AND grammeme IN (' + deleteRowsWithTheseGrammemes.join(', ') + ')').catch(LOG.caughtError);
        await SESSION.file.db.MTMdeleterows(DBMTMs['m_grammemes'], 'grammeme IN (' + allNotPresentCategoryGrammemes.join(', ') + ')' +
            'AND morpheme IN (' + morphemeIds.join(', ') + ')').catch(LOG.caughtError);
        // Step 6 - delete all the morphemes, referenced by a MtM row for grammeme ids in Step 2 but not Step 3
        let morphemesToDelete = [];
        morphemesToDelete = rows.map((x) => x.morpheme);
        if (morphemesToDelete.length > 0)
            await SESSION.file.db.deleterowsbyid(table_e.morpheme, morphemesToDelete).catch(LOG.caughtError);
        // Operation 2 - add all the missing (new) morphemes for all the grammemes in categories, which now belong to the slot
        // Step 1 - get a single random morpheme for the slot
        let morpheme;
        for (let m in indexedData.indexedMorphemes)
            if (indexedData.indexedMorphemes[m].slot == slotId) {
                morpheme = indexedData.indexedMorphemes[m];
                break;
            }
        // Step 2 - if the slot actually has morphemes...
        let alreadyPresentCategoryIds = [];
        if (morpheme !== undefined) {
            // ...go through all the grammemes and aggregate their categories
            for (let i in morpheme.grammemeIds) {
                alreadyPresentCategoryIds = [...new Set([
                        ...alreadyPresentCategoryIds,
                        indexedData.indexedGrammemes[morpheme.grammemeIds[i]].category
                    ])];
            }
        }
        // Step 3 - determine the missing categories present for the slot, if any
        let missingSlotCategories = categoryIds.filter(x => !alreadyPresentCategoryIds.includes(x));
        if (missingSlotCategories.length > 0) {
            // Step 4 - for the missing categories, determine the respective grammeme with the smallest position
            let mctgSmallestGrammeme = [];
            let mctgOtherGrammemes = [];
            let allMCTGGrammemes = [];
            for (let ctg in missingSlotCategories) {
                let smallestId = -1;
                let smallestPos = null;
                let ctgId = missingSlotCategories[ctg];
                // TODO: Optimise. Currently standing at O(2x^2) complexity
                for (let grm in indexedData.indexedGrammemes) {
                    let grmId = indexedData.indexedGrammemes[grm].id;
                    if (indexedData.indexedGrammemes[grmId].category != ctgId)
                        continue;
                    let grmPos = indexedData.indexedGrammemes[grm].position;
                    allMCTGGrammemes.push(grmId);
                    if (!smallestPos || grmPos < smallestPos) {
                        smallestId = grmId;
                        smallestPos = grmPos;
                    }
                }
                for (let grm in indexedData.indexedGrammemes) {
                    let grmId = indexedData.indexedGrammemes[grm].id;
                    if (indexedData.indexedGrammemes[grmId].category != ctgId)
                        continue;
                    if (grmId == smallestId)
                        continue;
                    mctgOtherGrammemes.push(grmId);
                }
                if (smallestId < 0) {
                    INTERFACE.displayError("Fatal error - no grammemes found for category " + ctgId + " during update stage 2");
                    return;
                }
                mctgSmallestGrammeme[ctgId] = smallestId;
            }
            // Step 5 - for the existing morphemes, add MtM rows for each of the smallest grammemes
            for (let m in indexedData.indexedMorphemes)
                if (indexedData.indexedMorphemes[m].slot == slotId) {
                    let morphemeId = indexedData.indexedMorphemes[m].id;
                    for (let grm in mctgSmallestGrammeme) {
                        await SESSION.file.db.MTMaddOne(DBMTMs['m_grammemes'], table_e.morpheme, morphemeId, mctgSmallestGrammeme[grm]).catch(LOG.caughtError);
                        indexedData.indexedMorphemes[m].grammemeIds.push(mctgSmallestGrammeme[grm]);
                    }
                }
            // Step 6 - for all the missing and existing categories, add a new morpheme for the cartesian product
            // of each of the grammemes (sans those already added)
            // New categories will probably only be added one at a time, but meh.
            if (categoryIds.length > 0) {
                let cartProdInput = [];
                for (let ctgId in categoryIds) {
                    let intermVal;
                    if (alreadyPresentCategoryIds.length > 0) {
                        //exclude combos that already exist - the smallest grammemes of new categories
                        //the line below can be optimised
                        intermVal = indexedData.indexedCategories[categoryIds[ctgId]].grammemeIds.
                            filter((x) => !mctgSmallestGrammeme.includes(x));
                    }
                    else {
                        intermVal = indexedData.indexedCategories[categoryIds[ctgId]].grammemeIds;
                    }
                    cartProdInput.push(intermVal);
                }
                let newGrammemeCombos = UTL.cartProd(cartProdInput);
                const NEW_MORPH_GIVEAWAY = '__newM__';
                let newMorphemeDBrows = newGrammemeCombos.map((x) => [slotId, NEW_MORPH_GIVEAWAY, -1]);
                await SESSION.file.db.savemultiplerows(table_e.morpheme, ['slot', 'SCs', 'overwrites'], newMorphemeDBrows).catch(LOG.caughtError);
                let newMorphemeIds = await SESSION.file.db.getids(table_e.morpheme, "SCs = '" + NEW_MORPH_GIVEAWAY + "'").catch(LOG.caughtError);
                let catlen = newGrammemeCombos[0].length; //presumably, there's at least one
                let fromIds = newMorphemeIds.map((x) => Array(catlen).fill(x)).flat();
                let toIds = newGrammemeCombos.flat();
                await SESSION.file.db.MTMaddlist(DBMTMs['m_grammemes'], table_e.morpheme, fromIds, toIds).catch(LOG.caughtError);
                await SESSION.file.db.updatedata(table_e.morpheme, { 'SCs': ' ' }, "SCs = '" + NEW_MORPH_GIVEAWAY + "'").catch(LOG.caughtError);
            }
        }
    }
    static async updateSlotCategoryWithANewGrammeme(slot, category, grammeme) {
        let data = await SESSION.file.db.MTMgetwhere(DBMTMs['slot_categories'], 'slot = ' + slot).catch(LOG.caughtError);
        if (!data.map((x) => x.category).includes(category))
            return; // something has gone wrong, this slot doesn't have this category
        const NEW_MORPH_GIVEAWAY = '__newM__';
        if (data.length == 1) // the slot has just a single category
         {
            await SESSION.file.db.savedata(table_e.morpheme, { slot: slot, SCs: NEW_MORPH_GIVEAWAY, overwrites: -1 }).catch(LOG.caughtError);
            let newMorphemeId = await SESSION.file.db.getids(table_e.morpheme, "SCs = '" + NEW_MORPH_GIVEAWAY + "'").catch(LOG.caughtError);
            let newid = newMorphemeId[0];
            await SESSION.file.db.MTMaddOne(DBMTMs['m_grammemes'], table_e.morpheme, newid, grammeme).catch(LOG.caughtError);
            await SESSION.file.db.updatedata(table_e.morpheme, { 'SCs': ' ' }, "SCs = '" + NEW_MORPH_GIVEAWAY + "'").catch(LOG.caughtError);
        }
        else // the slot has multiple categories
         {
            data = data.filter((x) => x.category != category);
            let cartProdInput = [[grammeme]];
            for (let i in data) {
                let grms = await SESSION.file.db.getdata(table_e.grammeme, ['*'], 'category = ' + data[i].category).catch(LOG.caughtError);
                grms = grms.map((x) => x.id);
                cartProdInput.push(grms);
            }
            let newGrammemeCombos = UTL.cartProd(cartProdInput);
            let newMorphemeDBrows = newGrammemeCombos.map((x) => [slot, NEW_MORPH_GIVEAWAY, -1]);
            await SESSION.file.db.savemultiplerows(table_e.morpheme, ['slot', 'SCs', 'overwrites'], newMorphemeDBrows).catch(LOG.caughtError);
            let newMorphemeIds = await SESSION.file.db.getids(table_e.morpheme, "SCs = '" + NEW_MORPH_GIVEAWAY + "'").catch(LOG.caughtError);
            let catlen = newGrammemeCombos[0].length;
            let fromIds = newMorphemeIds.map((x) => Array(catlen).fill(x)).flat();
            let toIds = newGrammemeCombos.flat();
            await SESSION.file.db.MTMaddlist(DBMTMs['m_grammemes'], table_e.morpheme, fromIds, toIds).catch(LOG.caughtError);
            await SESSION.file.db.updatedata(table_e.morpheme, { 'SCs': ' ' }, "SCs = '" + NEW_MORPH_GIVEAWAY + "'").catch(LOG.caughtError);
        }
    }
    static morphemeSCsToPreview(SCs) {
        if (!SCs.includes('\n'))
            return SCs;
        let slashPos = SCs.indexOf('-');
        if (slashPos >= 0) {
            let end = SCs.indexOf('\n', slashPos);
            let start = SCs.lastIndexOf('\n', slashPos) + 1;
            end = Math.max(end, SCs.length);
            return SCs.slice(start, end) + ' *';
        }
        else {
            let lines = SCs.split('\n');
            let ll = lines.length;
            for (let i = 0; i < ll; i++)
                if (lines[i].trim().length > 0)
                    return lines[i] + ' *';
        }
        return ' ';
    }
}
//# sourceMappingURL=morphology.js.map