"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class paradigm_table_t {
    constructor(row) {
        this.id = -1;
        this.name = '';
        this.word_type = -1;
        this.slots = [];
        this.id = row.id;
        this.name = row.name;
        this.word_type = row.word_type;
    }
    static async add() {
        if (IF_MORPHOLOGY.selectedWordType < 0) {
            INTERFACE.displayError('Please select a part of speech first.');
            return;
        }
        let NEW_TABLE_GIVEAWAY = 'NEW_TABLE_GIVEAWAY' + UTL.randomstring(5);
        await SESSION.file.db.savedata(table_e.paradigm_table, {
            "name": NEW_TABLE_GIVEAWAY,
            "word_type": IF_MORPHOLOGY.selectedWordType
        }).catch(LOG.caughtError);
        let newTableId = await SESSION.file.db.getid(table_e.paradigm_table, "name = '" + NEW_TABLE_GIVEAWAY + "'").catch(LOG.caughtError);
        if (newTableId >= 0) {
            IF_MORPHOLOGY.selectedParadigmTable = newTableId;
            IF_MORPHOLOGY.selectedSlot = -1;
            IF_MORPHOLOGY.selectedMorpheme = -1;
            await SESSION.file.db.updatedata(table_e.paradigm_table, { 'name': 'paradigm table ' + UTL.newHTMLId() }, "name = '" + NEW_TABLE_GIVEAWAY + "'").catch(LOG.caughtError);
        }
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async delete(tableId) {
        if (!UTL.confirmdatadeletion())
            return;
        if (tableId < 0) {
            INTERFACE.displayError('Please select a valid paradigm table first.');
            return;
        }
        await SESSION.file.db.deleterowsbyid(table_e.paradigm_table, [tableId]);
        if (IF_MORPHOLOGY.selectedParadigmTable == tableId) {
            IF_MORPHOLOGY.selectedParadigmTable = -1;
            IF_MORPHOLOGY.selectedSlot = -1;
            IF_MORPHOLOGY.selectedMorpheme = -1;
        }
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async uninherit(wordTypeId, tableId) {
        var _a;
        if (wordTypeId < 0) {
            INTERFACE.displayError('Please select a valid word type first.');
            return;
        }
        if (tableId < 0) {
            INTERFACE.displayError('Please select a valid paradigm table first.');
            return;
        }
        (_a = SESSION.file) === null || _a === void 0 ? void 0 : _a.db.MTMaddOne(DBMTMs['wt_missing_tables'], table_e.word_type, wordTypeId, tableId).catch(LOG.caughtError);
        if (IF_MORPHOLOGY.selectedParadigmTable == tableId) {
            IF_MORPHOLOGY.selectedParadigmTable = -1;
            IF_MORPHOLOGY.selectedSlot = -1;
            IF_MORPHOLOGY.selectedMorpheme = -1;
        }
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async reinherit(wordTypeId, tableId) {
        var _a;
        if (wordTypeId < 0) {
            INTERFACE.displayError('Please select a valid word type first.');
            return;
        }
        if (tableId < 0) {
            INTERFACE.displayError('Please select a valid paradigm table first.');
            return;
        }
        (_a = SESSION.file) === null || _a === void 0 ? void 0 : _a.db.MTMdeleterows(DBMTMs['wt_missing_tables'], 'word_type = ' + wordTypeId +
            'AND paradigm_table = ' + tableId).catch(LOG.caughtError);
        IF_MORPHOLOGY.refreshParadigmArea();
    }
}
//# sourceMappingURL=paradigmTable.js.map