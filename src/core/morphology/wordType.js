"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class word_type_t {
    constructor(row) {
        this.id = -1;
        this.conlang = -1;
        this.name = '';
        this.abbreviation = '';
        this.assignable = false;
        this.part_of_speech = false;
        this.inherent_categories = [];
        this.missing_inherent_categories = [];
        this.missing_tables = [];
        this.missing_slots = [];
        this.inherits_from = -1;
        this.subtypes = [];
        this.paradigm_tables = [];
        this.id = row.id;
        this.conlang = row.conlang;
        this.name = row.name;
        this.abbreviation = row.abbreviation;
        this.assignable = row.assignable;
        this.part_of_speech = row.part_of_speech;
        this.inherent_categories = row.inherent_categories || [];
        this.missing_inherent_categories = row.missing_inherent_categories || [];
        this.missing_tables = row.missing_tables || [];
        this.missing_slots = row.missing_slots || [];
        this.inherits_from = row.inherits_from;
    }
    static async getDerivedWordTypes(fromType, notSelf) {
        let res;
        let derivedWordTypes = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'inherits_from = ' + fromType).catch(LOG.caughtError);
        res = derivedWordTypes.map(x => x.id);
        if (!notSelf)
            res.push(fromType);
        for (let i in derivedWordTypes) {
            let subtypes = await word_type_t.getDerivedWordTypes(derivedWordTypes[i].id, true);
            res = [...res, ...subtypes];
        }
        return res;
    }
    async isDerivedFrom(from, considerSelf) {
        if (!from)
            return true;
        if (considerSelf && this.id == from)
            return true;
        if (this.inherits_from < 0)
            return false;
        let ihf = this.inherits_from;
        while (ihf >= 0) {
            if (ihf == from)
                return true;
            ihf = (await SESSION.file.db.getdata(table_e.word_type, ['*'], 'id = ' + ihf).catch(LOG.caughtError))[0].inherits_from;
        }
        return false;
    }
}
//# sourceMappingURL=wordType.js.map