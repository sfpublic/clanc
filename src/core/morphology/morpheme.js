"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class morpheme_t {
    constructor(row) {
        this.id = -1;
        this.slot = -1;
        this.SCs = "";
        this.grammemeIds = [];
        this.overwrites = -1;
        this.id = row.id;
        this.slot = row.slot;
        this.SCs = row.SCs;
        this.overwrites = row.overwrites;
    }
}
//# sourceMappingURL=morpheme.js.map