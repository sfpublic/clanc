"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class slot_t {
    constructor(row) {
        this.id = -1;
        this.paradigm_table = -1;
        this.categoryIds = [];
        this.missing_categoryIds = [];
        this.weight = -1;
        this.name = '';
        this.category_positions = [];
        this.position = -1;
        this.word_type = null;
        this.id = row.id;
        this.paradigm_table = row.paradigm_table;
        this.weight = row.weight;
        this.name = row.name;
        this.position = row.position;
        this.word_type = row.word_type;
    }
    static async add() {
        if (IF_MORPHOLOGY.selectedParadigmTable < 0) {
            INTERFACE.displayError('Please select a paradigm table first.');
            return;
        }
        let position = 0;
        let data = await SESSION.file.db.getdata(table_e.slot, ['*'], 'paradigm_table = ' + IF_MORPHOLOGY.selectedParadigmTable).catch(LOG.caughtError);
        for (let g in data)
            position = Math.max(position, data[g].position);
        position++;
        await SESSION.file.db.savedata(table_e.slot, {
            "paradigm_table": IF_MORPHOLOGY.selectedParadigmTable,
            "weight": 0,
            "name": "affix " + UTL.newHTMLId(),
            "position": position,
            "word_type": IF_MORPHOLOGY.selectedWordType
        }).catch(LOG.caughtError);
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async remove() {
        if (IF_MORPHOLOGY.selectedSlot < 0) {
            INTERFACE.displayError('Please select an affix first.');
            return;
        }
        let slot = IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[IF_MORPHOLOGY.selectedSlot];
        let position = slot.position;
        let data = await SESSION.file.db.getdata(table_e.slot, ['*'], 'position > ' + position +
            ' AND paradigm_table = ' + slot.paradigm_table);
        for (let g in data)
            await SESSION.file.db.updatedata(table_e.slot, { 'position': data[g].position - 1 }, 'id = ' + data[g].id);
        await SESSION.file.db.deleterowsbyid(table_e.slot, [IF_MORPHOLOGY.selectedSlot]);
        IF_MORPHOLOGY.selectedSlot = -1;
        IF_MORPHOLOGY.selectedMorpheme = -1;
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async moveInward(slot) {
        let slots = IF_MORPHOLOGY.paradigmDataBackup.indexedSlots;
        if (!slots[slot]) {
            INTERFACE.displayError("Select an existing slot first.");
            return;
        }
        if (slots[slot].position == 1)
            return;
        let toswitch = -1;
        for (let s in slots)
            if (slots[s].position == slots[slot].position - 1) {
                toswitch = slots[s].id;
            }
        if (toswitch < 0) {
            INTERFACE.displayError("Internal error - slot cannot be moved inward.");
            return;
        }
        await SESSION.file.db.updatedata(table_e.slot, { 'position': slots[slot].position - 1 }, 'id = ' + slots[slot].id);
        await SESSION.file.db.updatedata(table_e.slot, { 'position': slots[toswitch].position + 1 }, 'id = ' + slots[toswitch].id);
        slots[slot].position--;
        slots[toswitch].position++;
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async moveOutward(slot) {
        let slots = IF_MORPHOLOGY.paradigmDataBackup.indexedSlots;
        if (!slots[slot]) {
            INTERFACE.displayError("Select an existing slot first.");
            return;
        }
        let toswitch = -1;
        for (let s in slots)
            if (slots[s].position == slots[slot].position + 1) {
                toswitch = slots[s].id;
            }
        if (toswitch < 0) {
            return;
        }
        await SESSION.file.db.updatedata(table_e.slot, { 'position': slots[slot].position + 1 }, 'id = ' + slots[slot].id);
        await SESSION.file.db.updatedata(table_e.slot, { 'position': slots[toswitch].position - 1 }, 'id = ' + slots[toswitch].id);
        slots[slot].position++;
        slots[toswitch].position--;
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async uninherit(wordTypeId, slotId) {
        var _a;
        if (wordTypeId < 0) {
            INTERFACE.displayError('Please select a valid word type first.');
            return;
        }
        if (slotId < 0) {
            INTERFACE.displayError('Please select a valid slot first.');
            return;
        }
        (_a = SESSION.file) === null || _a === void 0 ? void 0 : _a.db.MTMaddOne(DBMTMs['wt_missing_slots'], table_e.word_type, wordTypeId, slotId).catch(LOG.caughtError);
        if (IF_MORPHOLOGY.selectedSlot == slotId) {
            IF_MORPHOLOGY.selectedSlot = -1;
            IF_MORPHOLOGY.selectedMorpheme = -1;
        }
        IF_MORPHOLOGY.refreshParadigmArea();
    }
    static async reinherit(wordTypeId, slotId) {
        var _a;
        if (wordTypeId < 0) {
            INTERFACE.displayError('Please select a valid word type first.');
            return;
        }
        if (slotId < 0) {
            INTERFACE.displayError('Please select a valid slot first.');
            return;
        }
        (_a = SESSION.file) === null || _a === void 0 ? void 0 : _a.db.MTMdeleterows(DBMTMs['wt_missing_slots'], 'word_type = ' + wordTypeId +
            'AND slot = ' + slotId).catch(LOG.caughtError);
        IF_MORPHOLOGY.refreshParadigmArea();
    }
}
class slot_overwritten_categories_t {
    constructor(row) {
        this.id = -1;
        this.slot = -1;
        this.category = -1;
        this.word_type = -1;
        this.present = false;
        this.id = row.id;
        this.slot = row.slot;
        this.category = row.category;
        this.word_type = row.word_type;
        this.present = row.present;
    }
}
class category_position_t {
    constructor() {
        this.id = -1;
        this.position = -1;
        this.mtmid = -1;
    }
}
//# sourceMappingURL=slot.js.map