"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class INTERFACE {
    constructor() {
        return lo(g.error, "This object is only to be used statically.", null);
    }
    static init() {
        this.clearUImessages();
        if (!UTL.devCode)
            UTL.getElem('devUI').outerHTML = '';
    }
    static async loadpage(html_page) {
        UTL.getElem("body").innerHTML = html_page.html;
        SESSION.sync_page_to_conlang = html_page.refresh;
        await html_page.load_page();
        SESSION.sync_page_to_conlang();
    }
    static savefile() {
        if (SESSION.file == null) {
            INTERFACE.displayError('No file to save');
            return;
        }
        SESSION.file.save().catch(LOG.caughtError);
    }
    static savefileas() {
        if (SESSION.file == null) {
            INTERFACE.displayError('No file to save');
            return;
        }
        SESSION.file.saveAs().then(() => {
            INTERFACE.refreshConlangTabs();
        }).catch(LOG.caughtError);
    }
    static loadfile() {
        if (SESSION.file && SESSION.file.unsavedChanges && !UTL.confirmunsavedchanges())
            return Presolve('save changes first');
        SESSION.createfile().then(() => {
            if (SESSION.file == null) {
                INTERFACE.displayError('Error opening file');
                return Preject('session file is null');
            }
            return SESSION.file.load(async () => {
                return SESSION.file.db.getdata(table_e.conlang, ["*"]).
                    then((clgs) => {
                    //TODO: eventually move the session-specific logic into its own session function
                    SESSION.conlangs = [];
                    SESSION.activeconlang = 0;
                    for (let clg of clgs) {
                        SESSION.conlangs.push({
                            dbid: clg.id,
                            protolangid: clg.descends_from
                        });
                    }
                    UTL.getElem('fileOps').hidden = false;
                    if (clgs.length == 0) {
                        SESSION.activeconlang = -1;
                        UTL.getElem('conlangOps').hidden = true;
                    }
                    else
                        UTL.getElem('conlangOps').hidden = false;
                    INTERFACE.loadpage(IF_CONLANGS.htmlpage);
                    INTERFACE.refreshInterface();
                    return Presolve('');
                });
            });
        })
            .catch(LOG.caughtError);
    }
    static async createfile(loadclgpage = true) {
        if (SESSION.file && SESSION.file.unsavedChanges && !UTL.confirmunsavedchanges())
            return Presolve('save changes first');
        return SESSION.createfile().then(() => {
            UTL.getElem('fileOps').hidden = false;
            UTL.getElem('conlangOps').hidden = true;
            if (loadclgpage)
                INTERFACE.loadpage(IF_CONLANGS.htmlpage);
            INTERFACE.refreshInterface();
        });
    }
    static refreshConlangTabs() {
        UTL.getElem("conlangtabs").innerHTML = '';
        let file = SESSION.file;
        if (file === null)
            return;
        // 	html +=
        // 		'<button onclick="INTERFACE.moveconlangleft();" class="smallbtn"><i class="fa fa-arrow-left" style="font-size: 7;"></i></button>'+
        // 		'<button onclick="INTERFACE.moveconlangright();" class="smallbtn"><i class="fa fa-arrow-right" style="font-size: 7;"></i></button>';
        SESSION.file.db.getdata(table_e.conlang, ['*']).then((data) => {
            let html = SESSION.file.filename + ': ';
            let clgnum = SESSION.conlangs.length;
            let selectedId = ((SESSION.getactiveconlang() && SESSION.getactiveconlang().dbid) || -1);
            for (let i = 0; i < clgnum; i++) {
                let name = "";
                let selected = "";
                for (let j = 0; j < data.length; j++)
                    if (data[j].id == SESSION.conlangs[i].dbid) {
                        name = data[j].name;
                        if (data[j].id == selectedId)
                            selected = 'selected';
                        break;
                    }
                html +=
                    `<button onclick="INTERFACE.selectAConlangButton(${i}, this);"
					class="btn conlangbtn ${selected}" id="conlang_${i}">${name}</button>`;
            }
            UTL.getElem("conlangtabs").innerHTML = html;
        }).catch(LOG.caughtError);
    }
    static selectAConlangButton(id, element) {
        let res = SESSION.selectconlangbyid(id);
        if (res.error)
            return;
        let elements = document.getElementsByClassName('conlangbtn');
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selected');
        element.classList.add('selected');
        SESSION.sync_page_to_conlang();
    }
    static refreshInterface() {
        INTERFACE.refreshConlangTabs();
        SESSION.sync_page_to_conlang();
    }
    static logError(msg) {
        UTL.getElem('infobox').innerHTML =
            '<div class="infobox errorbox">' +
                this.closeBtn +
                '<i class="fa fa-exclamation-circle fa-1x"></i>&nbsp;&nbsp;Console error: ' +
                msg +
                '</div>';
    }
    static displayError(msg) {
        UTL.getElem('infobox').innerHTML =
            '<div class="infobox errorbox">' +
                this.closeBtn +
                '<i class="fa fa-exclamation-circle fa-1x"></i>&nbsp;&nbsp;' +
                msg +
                '</div>';
        lo(g.UIerror, msg);
    }
    static displayInfo(msg) {
        UTL.getElem('infobox').innerHTML =
            '<div class="infobox">' +
                this.closeBtn +
                '<i class="fa fa-info-circle fa-1x"></i>&nbsp;&nbsp;' +
                msg +
                '</div>';
        lo(g.info, msg);
    }
    static displayHint(msg) {
        UTL.hints && (UTL.getElem('infobox').innerHTML =
            '<div class="infobox">' +
                this.closeBtn +
                '<i class="fa fa-info-circle fa-1x"></i>&nbsp;&nbsp;' +
                msg +
                '</div>');
    }
    static clearUImessages() {
        UTL.getElem('infobox').innerHTML = '';
    }
}
INTERFACE.closeBtn = '<i class="fa fa-times closeBtn" onclick="INTERFACE.clearUImessages();"></i>&nbsp;&nbsp;';
//# sourceMappingURL=interface.js.map