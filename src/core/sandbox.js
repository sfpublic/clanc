"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
function randomword(id, wordTypes) {
    let prefixes = ['stra', 'tla', 'tle', 'ke', 'irst', 'foo', 'bar', 'qwer'];
    let suffixes = ['iop', 'ikl', 'nm', 'op', 'edf', 'yh', 'ikm', 'olk'];
    let consonants = ['p', 't', 'k', 'b', 'd', 'g', 'n', 'm', 's', 'f', 'h', 'l', 'r', 'q', 'w'];
    let vowels = ['a', 'o', 'u', 'y', 'e', 'i'];
    let word_type = UTL.randomelement(wordTypes).id;
    let word = '';
    if (Math.random() < 0.5)
        word += UTL.randomelement(prefixes);
    let syls = 1 + UTL.randomint(3);
    for (let i = 0; i < syls; i++) {
        if (Math.random() < 0.9)
            word += UTL.randomelement(consonants);
        if (Math.random() < 0.1)
            word += UTL.randomelement(consonants);
        word += UTL.randomelement(vowels);
        if (Math.random() < 0.4)
            word += UTL.randomelement(consonants);
    }
    if (Math.random() < 0.5)
        word += UTL.randomelement(suffixes);
    return [
        id,
        word,
        word_type,
        UTL.randomstring(5 + UTL.randomint(10)),
        0,
        UTL.randomint(4294967296)
    ];
}
async function generateWords() {
    let id = SESSION.getactiveconlang().dbid;
    let words = [];
    let wordTypes = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + id);
    for (let i = 0; i < 10; i++)
        words.push(randomword(id, wordTypes));
    await SESSION.file.db.savemultiplerows(table_e.entry, ["conlang",
        "root",
        "part_of_speech",
        "translations",
        "state",
        "diachronic_id"], words);
    INTERFACE.refreshInterface();
}
async function foo() {
    generateWords();
}
function bar() {
    INTERFACE.displayInfo('foo');
    setTimeout(function () { INTERFACE.displayError('bar'); }, 5E3);
}
async function sbprintdb() {
    if (SESSION.file == null) {
        lo(g.error, 'session file is null');
        return;
    }
    let vals = Object.values(table_e);
    for (let t in vals)
        lo(g.debug, vals[t] + ": " + JSON.stringify(await SESSION.file.db.getdata(vals[t])));
    let vals2 = Object.values(DBMTMs);
    for (let t in vals2)
        lo(g.debug, mtmname(vals2[t]) + ": " + JSON.stringify(await SESSION.file.db.MTMgetalldata(vals2[t])));
    SESSION.sync_page_to_conlang();
}
/*
slickgrid reference:
let rows = props.owner.slickgrid.getSelectedRows();
let cell = props.owner.slickgrid.getActiveCell();
let editor = props.owner.slickgrid.getCellEditor();
*/ 
//# sourceMappingURL=sandbox.js.map