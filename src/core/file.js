"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class FILE {
    constructor() {
        /*
        * Conlang file.
        * - Contains the filename and a reference to the DB, holding data for the file.
        * - Is controlled by its table and by the interface through the session object.
        * - Is held by the session object.
        */
        this.db = new ALA_SQL_DB(this);
        this.id = '';
        this.filename = 'Untitledese.clc';
        this.unsavedChanges = false;
    }
    changesAreSaved() {
        this.unsavedChanges = false;
        window.onbeforeunload = null;
    }
    changesArePresent() {
        this.unsavedChanges = true;
        window.onbeforeunload = () => { return "Unsaved changes are present. Exit anyway?"; };
    }
    //asynchronous, returns a promise
    init() {
        return this.db.init().then((args) => {
            this.id = this.db.databasename;
            return PresolveT(UTL.res_success);
        });
    }
    //asynchronous, returns a promise
    deinit() {
        return this.db.deinit();
    }
    async createConlang(name) {
        await this.db.savedata(table_e.conlang, { "name": name }).catch(LOG.caughtError);
        let clgdbid = await this.db.getid(table_e.conlang, "name = '" + name + "'");
        await this.db.savedata(table_e.miscdata, {
            "conlang": clgdbid,
            "spokenfrom": "",
            "spokento": "",
            "place": "",
            "setting": "",
            "segments": `C = p t c k q b d g f s h v z m n r l j w
R = r l j w
N = m n
V = a o u y e i`
        }).catch(LOG.caughtError);
        await this.db.savedata(table_e.morphology, {
            "conlang": clgdbid
        });
        let morphid = await this.db.getid(table_e.morphology, "conlang = " + clgdbid);
        await this.db.savemultiplerows(table_e.word_type, ['conlang', 'assignable', 'part_of_speech', 'name', 'abbreviation'], [
            [clgdbid, true, true, 'pronoun', 'pr'],
            [clgdbid, true, true, 'noun', 'n'],
            [clgdbid, true, false, 'nouns 1', 'n I'],
            [clgdbid, true, false, 'nouns 2', 'n II'],
            [clgdbid, true, true, 'verb', 'v'],
            [clgdbid, true, false, 'verbs 1', 'v I'],
            [clgdbid, true, false, 'verbs 2', 'v II'],
            [clgdbid, true, true, 'adjective', 'adj'],
            [clgdbid, true, true, 'adverb', 'adv'],
            [clgdbid, true, true, 'ordinal numeral', 'num ord'],
            [clgdbid, true, true, 'cardinal numeral', 'num card'],
            [clgdbid, true, true, 'personal pronoun', 'ppr'],
            [clgdbid, true, true, 'nominal pronoun', 'npr'],
            [clgdbid, true, true, 'conjunction', 'conj'],
            [clgdbid, true, true, 'adposition', 'adp'],
            [clgdbid, true, true, 'interjection', 'interj']
        ]);
        await this.db.savemultiplerows(table_e.category, ['conlang', 'nullable', 'name'], [
            [clgdbid, true, 'number'],
            [clgdbid, false, 'person'],
            [clgdbid, false, 'tense']
        ]);
        let number, person, tense;
        await this.db.getdata(table_e.category, ['*'], 'conlang = ' + clgdbid).then((data) => {
            // SQL doesn't guarantee the order in which data will be received!
            for (let i = 0; i < data.length; i++)
                switch (data[i].name) {
                    case 'number':
                        number = data[i];
                        break;
                    case 'person':
                        person = data[i];
                        break;
                    case 'tense':
                        tense = data[i];
                        break;
                }
            return this.db.savemultiplerows(table_e.grammeme, ['category', 'name', 'gloss', 'position'], [
                [number.id, 'singular', 'SG', 1],
                [number.id, 'plural', 'PL', 2],
                [person.id, 'first', '1', 1],
                [person.id, 'second', '2', 2],
                [person.id, 'third', '3', 3],
                [tense.id, 'present', 'PRS', 1],
                [tense.id, 'past', 'PAST', 2],
                [tense.id, 'future', 'FUT', 3]
            ]);
        });
        await this.db.getdata(table_e.word_type, ['*'], 'conlang = ' + clgdbid).then(async (data) => {
            // SQL doesn't guarantee the order, in which data will be gotten!
            let pronoun, verb, noun;
            for (let i = 0; i < data.length; i++)
                switch (data[i].name) {
                    case 'pronoun':
                        pronoun = data[i];
                        break;
                    case 'noun':
                        noun = data[i];
                        break;
                    case 'nouns 1':
                        break;
                    case 'nouns 2':
                        break;
                    case 'verb':
                        verb = data[i];
                        break;
                    case 'verbs 1':
                        break;
                    case 'verbs 2':
                        break;
                    case 'adjective':
                        break;
                    case 'adverb':
                        break;
                    case 'ordinal numeral':
                        break;
                    case 'cardinal numeral':
                        await SESSION.file.db.MTMaddOne(DBMTMs['wt_inherent_categories'], table_e.word_type, data[i].id, number.id);
                        break;
                    case 'personal pronoun':
                        await SESSION.file.db.MTMaddOne(DBMTMs['wt_inherent_categories'], table_e.word_type, data[i].id, number.id);
                        await SESSION.file.db.MTMaddOne(DBMTMs['wt_inherent_categories'], table_e.word_type, data[i].id, person.id);
                        break;
                    case 'nominal pronoun':
                        break;
                    case 'conjunction':
                        break;
                    case 'adposition':
                        break;
                    case 'interjection':
                        break;
                }
            for (let i = 0; i < data.length; i++)
                switch (data[i].name) {
                    case 'nouns 1':
                        await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': noun.id }, 'id = ' + data[i].id);
                        break;
                    case 'nouns 2':
                        await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': noun.id }, 'id = ' + data[i].id);
                        break;
                    case 'verbs 1':
                        await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': verb.id }, 'id = ' + data[i].id);
                        break;
                    case 'verbs 2':
                        await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': verb.id }, 'id = ' + data[i].id);
                        break;
                    case 'personal pronoun':
                        await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': pronoun.id }, 'id = ' + data[i].id);
                        break;
                    case 'nominal pronoun':
                        await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': pronoun.id }, 'id = ' + data[i].id);
                        break;
                }
        });
        let clg = {
            dbid: clgdbid,
            protolangid: null
        };
        return PresolveT({ error: false, payload: clg, msg: '' });
    }
    async deriveConlang(name, from) {
        if (from < 0 || from == null)
            return PresolveT({ error: true, msg: 'No ancestral conlang specified' });
        let res = await this.db.savedata(table_e.conlang, {
            "name": name,
            "descends_from": from
        }).catch(LOG.caughtError);
        if (res !== 1)
            return PresolveT({ error: true, msg: 'Couldn\'t derive conlang (another derivation not renamed yet?)' });
        let clgdbid = await this.db.getid(table_e.conlang, "name = '" + name + "'");
        let protoMD = await this.db.getdata(table_e.miscdata, ['*'], 'conlang = ' + from);
        await this.db.savedata(table_e.miscdata, {
            "conlang": clgdbid,
            "spokenfrom": protoMD[0].spokento,
            "spokento": "",
            "place": protoMD[0].place,
            "setting": protoMD[0].setting,
            "segments": protoMD[0].segments
        }).catch(LOG.caughtError);
        let protoWT = await this.db.getdata(table_e.word_type, ['*'], 'conlang = ' + from);
        let protoWTidsArr = protoWT.map(x => x.id);
        let newWT = protoWT.map(x => [clgdbid, x.assignable, x.part_of_speech, x.name, x.abbreviation, x.id]);
        newWT.length > 0 && await this.db.savemultiplerows(table_e.word_type, ['conlang', 'assignable', 'part_of_speech', 'name', 'abbreviation', 'descends_from'], newWT);
        newWT = await this.db.getdata(table_e.word_type, ['*'], 'conlang = ' + clgdbid);
        let newWTidx = [];
        let newWTidsArr = newWT.map(x => x.id);
        for (let i in newWT)
            newWTidx[newWT[i].name] = newWT[i];
        let protoWTids = [];
        for (let i in protoWT)
            protoWTids[protoWT[i].id] = protoWT[i];
        for (let i in protoWT) {
            if (protoWT[i].inherits_from) {
                let tmp = protoWT[i].inherits_from;
                tmp = protoWTids[tmp];
                let newInh = newWTidx[tmp.name].id;
                let toUpd = newWTidx[protoWT[i].name].id;
                await SESSION.file.db.updatedata(table_e.word_type, { 'inherits_from': newInh }, 'id = ' + toUpd);
            }
        }
        //refresh the data after updating it
        newWT = await this.db.getdata(table_e.word_type, ['*'], 'conlang = ' + clgdbid);
        newWTidx = [];
        newWTidsArr = newWT.map(x => x.id);
        for (let i in newWT)
            newWTidx[newWT[i].name] = newWT[i];
        let protoCtg = await this.db.getdata(table_e.category, ['*'], 'conlang = ' + from);
        let newCtg = protoCtg.map(x => [clgdbid, x.nullable, x.name]);
        newCtg.length > 0 && await this.db.savemultiplerows(table_e.category, ['conlang', 'nullable', 'name'], newCtg);
        newCtg = await this.db.getdata(table_e.category, ['*'], 'conlang = ' + clgdbid);
        let newCtgIdx = [];
        let protoCtgIndices = [];
        for (let i in newCtg)
            newCtgIdx[newCtg[i].name] = newCtg[i];
        let protoCtgIdx = [];
        for (let i in protoCtg) {
            protoCtgIdx[protoCtg[i].id] = protoCtg[i];
            protoCtgIndices.push(protoCtg[i].id);
        }
        let protoGrm = await this.db.getdata(table_e.grammeme, ['*'], 'category IN (' + protoCtgIndices.join(', ') + ')');
        let newGrm = protoGrm.map(x => [
            newCtgIdx[protoCtgIdx[x.category].name].id,
            x.name,
            x.gloss,
            x.position
        ]);
        newGrm.length > 0 && await this.db.savemultiplerows(table_e.grammeme, ['category', 'name', 'gloss', 'position'], newGrm);
        let newGrmNames = [];
        let protoGrmIdx = [];
        if (newGrm.length) {
            newGrm = await this.db.getdata(table_e.grammeme, ['*'], 'category IN (' + (newCtg.map(x => x.id)).join(', ') + ')');
            let ngl = newGrm.length;
            for (let i = 0; i < ngl; i++) {
                newGrmNames[newGrm[i].name] = newGrm[i];
                protoGrmIdx[protoGrm[i].id] = protoGrm[i];
            }
        }
        let protoPT = await this.db.getdata(table_e.paradigm_table, ['*'], 'word_type IN (' + protoWTidsArr.join(', ') + ')');
        let newPT = protoPT.map(x => [x.name, newWTidx[protoWTids[x.word_type].name].id]);
        let protoPTidsArr = protoPT.map(x => x.id);
        newPT.length > 0 && await this.db.savemultiplerows(table_e.paradigm_table, ['name', 'word_type'], newPT);
        newPT = await this.db.getdata(table_e.paradigm_table, ['*'], 'word_type IN (' + newWTidsArr.join(', ') + ')');
        let newPTidsArr = newPT.map(x => x.id);
        let newPTidx = [];
        for (let i in newPT)
            newPTidx[newPT[i].name] = newPT[i];
        let protoPTids = [];
        for (let i in protoPT)
            protoPTids[protoPT[i].id] = protoPT[i];
        let protoSlots = await this.db.getdata(table_e.slot, ['*'], 'paradigm_table IN (' + protoPTidsArr.join(', ') + ')');
        let newSlots = protoSlots.map(x => [
            newPTidx[protoPTids[x.paradigm_table].name].id,
            x.weight,
            x.name,
            x.position,
            (x.word_type && x.word_type.length > 0 && newWTidx[protoWT[x.word_type].name].id) || null
        ]);
        newSlots.length > 0 && await this.db.savemultiplerows(table_e.slot, [
            "paradigm_table",
            'weight',
            'name',
            'position',
            'word_type'
        ], newSlots);
        newSlots = await this.db.getdata(table_e.slot, ['*'], 'paradigm_table IN (' + newPTidsArr.join(', ') + ')');
        let newSlotsidx = [];
        for (let i in newSlots)
            newSlotsidx[newSlots[i].name] = newSlots[i];
        let protoSlotsIdx = [];
        for (let i in protoSlots)
            protoSlotsIdx[protoSlots[i].id] = protoSlots[i];
        let protoSOWC = await this.db.getdata(table_e.slot_overwritten_categories, ['*'], 'category IN (' + protoCtgIndices.join(', ') + ')');
        let newSOWC = protoSOWC.map(x => [
            newSlotsidx[protoSlotsIdx[x.slot].name].id,
            newCtgIdx[protoCtgIdx[x.category].name].id,
            newWTidx[protoWT[x.word_type].name].id,
            x.present
        ]);
        newSOWC.length > 0 && await this.db.savemultiplerows(table_e.slot_overwritten_categories, [
            "slot",
            'category',
            'word_type',
            'present'
        ], newSOWC);
        let left = [];
        let right = [];
        let MTMdata = [];
        MTMdata = await this.db.MTMgetwhere(DBMTMs['wt_inherent_categories'], 'category IN (' + protoCtgIndices.join(', ') + ')');
        left = MTMdata.map(x => newWTidx[protoWTids[x.word_type].name].id);
        right = MTMdata.map(x => newCtgIdx[protoCtgIdx[x.category].name].id);
        left.length && await this.db.MTMaddlist(DBMTMs['wt_inherent_categories'], table_e.word_type, left, right);
        MTMdata = await this.db.MTMgetwhere(DBMTMs['wt_missing_inherent_categories'], 'category IN (' + protoCtgIndices.join(', ') + ')');
        left = MTMdata.map(x => newWTidx[protoWTids[x.word_type].name].id);
        right = MTMdata.map(x => newCtgIdx[protoCtgIdx[x.category].name].id);
        left.length && await this.db.MTMaddlist(DBMTMs['wt_missing_inherent_categories'], table_e.word_type, left, right);
        MTMdata = await this.db.MTMgetwhere(DBMTMs['slot_categories'], 'category IN (' + protoCtgIndices.join(', ') + ')');
        for (let i in MTMdata) //TODO: optimise
         {
            await this.db.MTMsavedata(DBMTMs['slot_categories'], {
                'slot': newSlotsidx[protoSlotsIdx[MTMdata[i].slot].name].id,
                'category': newCtgIdx[protoCtgIdx[MTMdata[i].category].name].id,
                'position': MTMdata[i].position
            });
        }
        MTMdata = await this.db.MTMgetwhere(DBMTMs['slot_missing_categories'], 'category IN (' + protoCtgIndices.join(', ') + ')');
        left = MTMdata.map(x => newSlotsidx[protoSlotsIdx[x.slot].name].id);
        right = MTMdata.map(x => newCtgIdx[protoCtgIdx[x.category].name].id);
        left.length && await this.db.MTMaddlist(DBMTMs['slot_missing_categories'], table_e.slot, left, right);
        MTMdata = await this.db.MTMgetwhere(DBMTMs['wt_missing_tables'], 'paradigm_table IN (' + protoPTidsArr.join(', ') + ')');
        left = MTMdata.map(x => newWTidx[protoWTids[x.word_type].name].id);
        right = MTMdata.map(x => newPTidx[protoPTids[x.paradigm_table].name].id);
        left.length && await this.db.MTMaddlist(DBMTMs['wt_missing_tables'], table_e.word_type, left, right);
        MTMdata = await this.db.MTMgetwhere(DBMTMs['wt_missing_slots'], 'word_type IN (' + protoWTidsArr.join(', ') + ')');
        left = MTMdata.map(x => newWTidx[protoWTids[x.word_type].name].id);
        right = MTMdata.map(x => newSlotsidx[protoSlotsIdx[x.slot].name].id);
        left.length && await this.db.MTMaddlist(DBMTMs['wt_missing_slots'], table_e.word_type, left, right);
        let oldMorphemes = await this.db.getdata(table_e.morpheme, ['*'], 'slot IN (' + (protoSlots.map(x => x.id)).join(', ') + ')');
        let newMorphemes = oldMorphemes.map(x => [
            newSlotsidx[protoSlotsIdx[x.slot].name].id,
            x.SCs,
            (protoSlotsIdx[x.overwrites] &&
                newSlotsidx[protoSlotsIdx[x.overwrites].name].id) ||
                null,
            x.id
        ]);
        if (newMorphemes.length > 0) {
            await this.db.savemultiplerows(table_e.morpheme, [
                "slot",
                'SCs',
                'overwrites',
                'descends_from'
            ], newMorphemes);
            newMorphemes = await this.db.getdata(table_e.morpheme, ['*'], 'slot IN (' + (newSlots.map(x => x.id)).join(', ') + ')');
            let morphemeMap = [];
            let ml = newMorphemes.length;
            for (let i = 0; i < ml; i++)
                morphemeMap[newMorphemes[i].descends_from] = newMorphemes[i];
            MTMdata = await this.db.MTMgetwhere(DBMTMs['m_grammemes'], 'morpheme IN (' + (oldMorphemes.map(x => x.id)).join(', ') + ')');
            left = MTMdata.map(x => morphemeMap[x.morpheme].id);
            right = MTMdata.map(x => newGrmNames[protoGrmIdx[x.grammeme].name].id);
            left.length && await this.db.MTMaddlist(DBMTMs['m_grammemes'], table_e.morpheme, left, right);
        }
        let clg = {
            dbid: clgdbid,
            protolangid: from
        };
        return PresolveT({ error: false, payload: clg, msg: '' });
    }
    async renameconlang(clg, new_name) {
        return this.db.updatedata(table_e.conlang, { "name": new_name }, "id = " + clg.dbid);
    }
    saveAs() {
        let filename = window.prompt("File name:", this.filename);
        if (filename == null)
            return Preject('no new filename selected');
        this.filename = filename;
        return this.save();
    }
    async save() {
        let indexedTables = {
            "version": 0.1
        };
        let keys = Object.keys(DBformat);
        for (let i = 0; i < keys.length; i++)
            indexedTables[keys[i]] = FILE.serialiseData(await this.db.getdata(DBformat[keys[i]].name).catch(LOG.caughtError));
        let mtmkeys = Object.keys(DBMTMs);
        for (let i = 0; i < mtmkeys.length; i++)
            indexedTables[mtmkeys[i]] = FILE.serialiseData(await this.db.MTMgetalldata(DBMTMs[mtmkeys[i]]).catch(LOG.caughtError));
        let blob = new Blob([JSON.stringify(indexedTables)], { type: "text/plain;charset=utf-8" });
        saveAs(blob, this.filename);
        this.changesAreSaved();
        return Presolve("saved");
    }
    async load(afterload) {
        let keys = Object.keys(DBformat);
        let kl = keys.length;
        for (let i = kl - 1; i >= 0; i--)
            await this.db.droptable(keys[i]).catch(LOG.caughtError);
        let mtmkeys = Object.keys(DBMTMs);
        let mtmkl = mtmkeys.length;
        for (let i = mtmkl - 1; i >= 0; i--)
            await this.db.MTMdroptable(DBMTMs[mtmkeys[i]]).catch(LOG.caughtError);
        UTL.resetUsedDBids();
        await this.init().catch(LOG.caughtError);
        return UTL.loadFile(async (file) => {
            let filejson = JSON.parse(file);
            if (filejson["version"] != 0.1)
                return Preject("Wrong file version - cannot open file");
            try {
                for (let i = 0; i < kl; i++) {
                    let table = filejson[keys[i]];
                    if (table.rows.length)
                        await this.db.savemultiplerows(keys[i], table.columns, table.rows).catch(LOG.caughtError);
                    //TODO: order of addition? check for dependencies
                }
                for (let i = 0; i < mtmkl; i++) {
                    let table = filejson[mtmkeys[i]];
                    if (table.rows.length)
                        // MTM API really should be dropped along with the DB change
                        await this.db.savemultiplerows(mtmname(DBMTMs[mtmkeys[i]]), table.columns, table.rows).catch(LOG.caughtError);
                }
            }
            catch (error) {
                return Preject(error);
            }
            return afterload();
        });
    }
    static serialiseData(dataIn) {
        let columns = [];
        let rows = [];
        if (dataIn.length > 0) {
            columns = Object.keys(dataIn[0]);
            let numcols = columns.length;
            let l = dataIn.length;
            for (let i = 0; i < l; i++) {
                let row = [];
                for (let c = 0; c < numcols; c++)
                    row.push(dataIn[i][columns[c]]);
                rows.push(row);
            }
        }
        return { columns: columns, rows: rows };
    }
}
//# sourceMappingURL=file.js.map