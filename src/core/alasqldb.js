"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class ALA_SQL_DB {
    constructor(f) {
        this.databasename = '';
        this.lastquery = '';
        this.onDeleteCascadeConstraints = {};
        this.onDeleteNullConstraints = {};
        this.file = f || null;
    }
    init() {
        let promises = [];
        let keys = Object.keys(DBformat);
        this.databasename = UTL.randomstring(15);
        let database = new alasql.Database(this.databasename);
        for (let i = 0; i < keys.length; i++)
            promises.push(this.createtable(DBformat[keys[i]]));
        return Promise.all(promises).then(() => {
            let morepromises = [];
            for (let i = 0; i < keys.length; i++)
                morepromises.push(this.addforeignkeys(DBformat[keys[i]]));
            return Promise.all(morepromises);
        }).then(() => {
            let morepromises = [];
            for (let i = 0; i < keys.length; i++)
                morepromises.push(this.addmtmtables(DBformat[keys[i]]));
            return Promise.all(morepromises);
        });
    }
    async tryalaquery(query) {
        this.lastquery = query;
        if (query.length < 500)
            lo(g.DBquery, query, 0, 1, 3);
        else
            lo(g.DBquery, query.substr(0, 500) + '...', 0, 1, 3);
        try {
            alasql.use(this.databasename);
            return alasql.promise(query);
        }
        catch (error) {
            lo(g.error, error.toString(), 0, 3);
            lo(g.error, "in query " + query, 0, 3);
            return Preject(error.toString());
            //TODO: better error printing system
            //TODO: should return error code
        }
    }
    async getid(table, where) {
        return this.getdata(table, ["id"], where).then((res) => {
            if (res !== null && res.length > 0) {
                return Presolve(res[0].id);
            }
            return PrejectT(-1);
        });
    }
    async getids(table, where) {
        return this.getdata(table, ["id"], where).then((res) => {
            if (res !== null) {
                return PresolveT(res.map((x) => x.id));
            }
        });
    }
    async getdata(table, cols, where) {
        if (typeof table !== "string")
            return lo(g.error, "table name " + table + " is not string", null);
        if (cols === undefined)
            cols = ["*"];
        let colsstr = cols.join(', ');
        /*let whereclause = "";
        if (where === undefined || where === "*") whereclause = "*";
        else if (typeof where !== "object") throwerr(2, "where clause "+where+" is not an object");
        else
            {
            }*/
        let query = "SELECT " + colsstr + " FROM " + table;
        if (where !== undefined)
            query += " WHERE " + where;
        //TODO: an interpreter, if it needs be
        return this.tryalaquery(query);
    }
    datarowtovalues(data, cols, auto_increment_id, enumerate_cols) {
        let values = "";
        let kl = data.length;
        if (kl == 0)
            return '';
        if (auto_increment_id)
            values += UTL.generateNewDBid() + ', ';
        for (let i = 0; i < kl; i++) {
            if (enumerate_cols)
                values += cols[i] + ' = ';
            if (data[i] === null || data[i] === undefined)
                values += 'NULL';
            else if (typeof data[i] === "string")
                values += '"' + UTL.escapeCharacters(data[i]) + '"';
            else if (typeof data[i] === "object")
                values += '"' + UTL.escapeCharacters(JSON.stringify(data[i])) + '"';
            else
                values += data[i];
            if (i < kl - 1)
                values += ', ';
            if (!auto_increment_id && cols[i] == 'id')
                UTL.setDBidAsUsed(data[i]);
        }
        return values;
    }
    datatovalues(data, cols, auto_increment_id, enumerate_cols) {
        return this.datarowtovalues(Object.values(data), cols, auto_increment_id, enumerate_cols);
    }
    async savemultiplerows(table, cols, data) {
        this.file && this.file.changesArePresent();
        let query = "INSERT INTO " + table + " ";
        let dl = data.length;
        let auto_increment_id = false;
        if (!cols.includes('id')) {
            cols.unshift('id');
            auto_increment_id = true;
        }
        query += "(" + cols.join(', ') + ")\nVALUES\n";
        for (let i = 0; i < dl; i++) {
            query += "   (";
            let tmp = this.datarowtovalues(data[i], cols, auto_increment_id, false);
            if (tmp == '')
                return Preject("no or invalid data in row for insert query:" + JSON.stringify(data[i], null, 2));
            query += tmp;
            query += ")";
            if (i < dl - 1)
                query += ',\n';
        }
        return this.tryalaquery(query);
    }
    async savedata(table, data) {
        return this.savemultiplerows(table, Object.keys(data), [Object.values(data)]);
    }
    async updatemultiplerows(table, cols, data) {
        this.file && this.file.changesArePresent();
        let query = "INSERT INTO " + table + " ";
        let dl = data.length;
        query += "(" + cols.join(', ') + ")\nVALUES\n";
        let odku = '';
        let cl = cols.length;
        for (let i = 0; i < cl; i++) {
            if (cols[i] == 'id')
                continue;
            odku += cols[i] + ' = VALUES(' + cols[i] + ')';
            if (i < cl - 1)
                odku += ', ';
        }
        for (let i = 0; i < dl; i++) {
            query += "   (";
            let tmp = this.datarowtovalues(data[i], cols, false, true);
            if (tmp == '')
                return Preject("no or invalid data in row for insert query:" + JSON.stringify(data[i], null, 2));
            query += tmp;
            query += ")";
            if (i < dl - 1)
                query += ',\n';
        }
        query += 'ON DUPLICATE KEY UPDATE ' + odku + ';';
        return this.tryalaquery(query);
    }
    async updatedata(table, data, where) {
        if (typeof table !== "string")
            return Preject("table name " + table + " is not string");
        if (typeof data !== "object")
            return Preject("insert query data is not object");
        this.file && this.file.changesArePresent();
        //TODO: transactions for multiple row insertion
        let query = "UPDATE " + table + " SET ";
        let values = this.datatovalues(data, Object.keys(data), false, true);
        if (values == '')
            return Preject("no data in row for insert query:" + JSON.stringify(data, null, 2));
        query += values;
        if (typeof where !== "string" || where.length < 2)
            return Preject("no where for update query:" + JSON.stringify(data, null, 2));
        query += " WHERE " + where;
        return this.tryalaquery(query);
    }
    //UPDATE table SET prop1 = value1, ... WHERE condition
    async droptable(table) {
        if (typeof table !== "string")
            Preject("table name " + table + " is not a string");
        this.file && this.file.changesArePresent();
        let query = "DROP TABLE " + table + ";";
        if (this.tryalaquery(query) === null)
            return Preject("error in drop table query: " + query);
        return Presolve("");
    }
    // deleterows(table:string, where:string): Promise<any>
    // 	{
    // 	if (typeof table !== "string") return Preject("table name "+table+" is not a string");
    // 	if (typeof where !== "string") return Preject("where clause "+where+" for delete query in table "+table+" is not a string");
    // 	if (where === "*") return Preject( "deleting everything is not supported for now, see TODO");
    // 	if (where === "") return Preject("where clause for delete query in table "+table+" is empty");
    // 	//TODO: add a separate function to delete everything
    // 	this.file && this.file.changesArePresent();
    // 	let query = "DELETE FROM "+table+" WHERE "+where;
    // 	return this.tryalaquery(query);
    // 	}
    async deleterowsbyid(table, ids) {
        if (ids.length == 0)
            return Preject("no ids provided");
        this.file && this.file.changesArePresent();
        if (this.onDeleteCascadeConstraints[table]) {
            let promises = [];
            for (let i in this.onDeleteCascadeConstraints[table]) {
                let tbl = this.onDeleteCascadeConstraints[table][i];
                if (!tbl.mtm)
                    promises.push(this.getids(tbl.table, tbl.key + " IN (" + ids.join(', ') + ")")
                        .then((res) => {
                        if (res.length > 0)
                            return this.deleterowsbyid(tbl.table, res);
                    }));
                else
                    promises.push(this.tryalaquery("DELETE FROM " + tbl.table + " WHERE " + tbl.key + " IN (" + ids.join(', ') + ")"));
            }
            return Promise.all(promises).then(() => {
                let query = "DELETE FROM " + table + " WHERE  id IN (" + ids.join(', ') + ")";
                return this.tryalaquery(query);
            });
        }
        if (this.onDeleteNullConstraints[table]) {
            let promises = [];
            for (let i in this.onDeleteNullConstraints[table]) {
                let tbl = this.onDeleteNullConstraints[table][i];
                promises.push(this.getids(tbl.table, tbl.key + " IN (" + ids.join(', ') + ")")
                    .then((res) => {
                    if (res.length > 0) {
                        let query = "UPDATE " +
                            tbl.table +
                            " SET " +
                            tbl.key +
                            " = NULL " +
                            " WHERE id IN (" +
                            ids.join(', ') +
                            ")";
                        return this.tryalaquery(query);
                    }
                }));
            }
            return Promise.all(promises).then(() => {
                let query = "DELETE FROM " + table + " WHERE  id IN (" + ids.join(', ') + ")";
                return this.tryalaquery(query);
            });
        }
        else {
            let query = "DELETE FROM " + table + " WHERE  id IN (" + ids.join(', ') + ")";
            return this.tryalaquery(query);
        }
    }
    async cleartable(table) {
        if (typeof table !== "string")
            return Preject("table name " + table + " is not a string");
        this.file && this.file.changesArePresent();
        let query = "DELETE FROM " + table;
        return this.tryalaquery(query);
    }
    async deinit() {
        let keys = Object.keys(DBformat);
        let promises = [];
        let kl = keys.length;
        for (let i = kl - 1; i >= 0; i--) {
            promises.push(this.droptable(keys[i]));
        }
        return Promise.all(promises).then(() => {
            return this.tryalaquery('DROP DATABASE ' + this.databasename);
        });
    }
    async createtable(table) {
        //TODO: error on repeated table name
        this.file && this.file.changesArePresent();
        let query = "CREATE TABLE IF NOT EXISTS `" + table.name + "` (";
        let keys = Object.keys(table.cols);
        let primarykey = "";
        let uniquekeys = [];
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let column = table.cols[key];
            let datatype = column.type;
            if (datatype == db_type_t.ref || datatype == db_type_t.MtM)
                continue;
            query += key + " ";
            if (typeof table.cols[key] !== "object")
                return Preject("table.cols[" + key + "] not int or object of this table: " + table.name);
            let dataprops = column.flags;
            switch (datatype) {
                case db_type_t.id:
                    query += "INT";
                    break;
                case db_type_t.txt:
                    query += "STRING";
                    break;
                case db_type_t.ltxt:
                    query += "STRING";
                    break;
                case db_type_t.int:
                    query += "INT";
                    break;
                case db_type_t.bool:
                    query += "INT";
                    break;
                default:
                    return Preject("undefined datatype: " + datatype);
            }
            if (dataprops & primarykey_p)
                primarykey = key;
            else if (dataprops & unique_p)
                uniquekeys.push(key);
            if (dataprops & notnull_p)
                query += " NOT NULL";
            // if (dataprops & autoincrement_p) query+=" AUTO_INCREMENT";
            if (dataprops & notnulllength_p)
                query += " check (len(" + key + ") >= 1)";
            if (dataprops & positive_p)
                query += " check (" + key + " > 0)";
            query += ",";
        }
        if (primarykey == "")
            return Preject("primarykey of table " + table.name + " is not set");
        if (table.unique_keys) {
            let uks = Object.keys(table.unique_keys);
            for (let i = 0; i < uks.length; i++)
                query += "  UNIQUE KEY `" + uks[i] + "` (`" + table.unique_keys[uks[i]].join('`, `') + "`),";
        }
        for (let uk of uniquekeys)
            query += "  UNIQUE(" + uk + "),";
        query += "  PRIMARY KEY (" + primarykey + "),";
        let commaindex = query.lastIndexOf(",");
        if (commaindex == -1)
            return Preject("no statements in the create query: " + query);
        query = UTL.removelastcomma(query);
        query += ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        return this.tryalaquery(query);
    }
    async addforeignkeys(table) {
        this.file && this.file.changesArePresent();
        let query = "";
        let keys = Object.keys(table.cols);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let column = table.cols[key];
            if (column.type != db_type_t.ref)
                continue;
            if (!column.references || column.references.length < 1) {
                return Preject("no reference specified for foreign key " + key + " of table " + table.name);
            }
            if (typeof table.cols[key] !== "object")
                return Preject("table.cols[" + key + "] not int or object of this table: " + table.name);
            query +=
                "ALTER TABLE `" +
                    table.name +
                    "` ADD COLUMN " +
                    key +
                    " INT REFERENCES " +
                    column.references +
                    "(id)";
            if (column.flags & notnull_p)
                query += " NOT NULL";
            if (column.flags & unique_p)
                query += " UNIQUE";
            query += "; ";
            //sanity checks
            if ((column.flags & ondeletecascade_p) && (column.flags & ondeletenull_p))
                return Preject(`A column cannot have both ON DELETE CASCADE and ON DELETE NULL: ${table.name}.${key}`);
            if (!(column.flags & ondeletecascade_p) && !(column.flags & ondeletenull_p))
                return Preject(`A column has to have either ON DELETE CASCADE or ON DELETE NULL: ${table.name}.${key}`);
            if (column.flags & ondeletecascade_p) {
                if (!this.onDeleteCascadeConstraints[table.name]) {
                    for (let i in this.onDeleteCascadeConstraints[table.name]) {
                        if (this.onDeleteCascadeConstraints[table.name][i].table == column.references)
                            return Preject(`Circular dependency between ${table.name} and ${column.references}`);
                    }
                }
                if (!this.onDeleteCascadeConstraints[column.references])
                    this.onDeleteCascadeConstraints[column.references] = [];
                this.onDeleteCascadeConstraints[column.references].push({
                    'table': table.name,
                    'key': key,
                    'mtm': false
                });
            }
            if (column.flags & ondeletenull_p) {
                if (!this.onDeleteNullConstraints[column.references])
                    this.onDeleteNullConstraints[column.references] = [];
                this.onDeleteNullConstraints[column.references].push({
                    'table': table.name,
                    'key': key,
                    'mtm': false
                });
            }
        }
        if (query == "")
            return Presolve("");
        return this.tryalaquery(query);
    }
    async addmtmtables(table) {
        this.file && this.file.changesArePresent();
        let query = "";
        let keys = Object.keys(table.cols);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let column = table.cols[key];
            if (column.type != db_type_t.MtM)
                continue;
            if (!column.references || column.references.length < 1) {
                return Preject("no reference specified for foreign key " + key + " of table " + table.name);
            }
            query += "\nCREATE TABLE IF NOT EXISTS `" + mtmname(table.name, column.references, key) + "` (";
            query += "id INT NOT NULL UNIQUE AUTO_INCREMENT,";
            query += table.name + " INT REFERENCES " + table.name + "(id) NOT NULL,";
            query += column.references + " INT REFERENCES " + column.references + "(id) NOT NULL,";
            if (column.flags & ordered_p)
                query += "position INT NOT NULL check (position > 0),";
            query += "  PRIMARY KEY (" + table.name + ', ' + column.references + ")";
            if (column.flags & notnull_p)
                lo(g.info, 'warning: not null is not implemented in this case for the time being');
            query += ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
            if (!this.onDeleteCascadeConstraints[column.references])
                this.onDeleteCascadeConstraints[column.references] = [];
            this.onDeleteCascadeConstraints[column.references].push({
                'table': mtmname(table.name, column.references, key),
                'key': column.references,
                'mtm': true
            });
            if (!this.onDeleteCascadeConstraints[table.name])
                this.onDeleteCascadeConstraints[table.name] = [];
            this.onDeleteCascadeConstraints[table.name].push({
                'table': mtmname(table.name, column.references, key),
                'key': table.name,
                'mtm': true
            });
        }
        if (query == "")
            return Presolve("");
        return this.tryalaquery(query);
    }
    //many to many add a one to many record
    async MTMaddOTM(table, first_col, fromid, toid) {
        return this.MTMaddlist(table, first_col, Array(toid.length).fill(fromid), toid);
    }
    //many to many add a single record
    async MTMaddOne(table, first_col, fromid, toid) {
        return this.MTMaddlist(table, first_col, [fromid], [toid]);
    }
    //many to many add records as two lists of id pairs
    async MTMaddlist(table, first_col, fromid, toid) {
        let tablename = mtmname(table);
        this.file && this.file.changesArePresent();
        let query = "INSERT INTO " + tablename + " ";
        let second_col = table.from;
        if (first_col == second_col)
            second_col = table.to;
        let dl = fromid.length;
        if (dl != toid.length)
            return Preject("lists of ids to add are of different length for " + tablename);
        query += "(" + first_col + ', ' + second_col + ")\nVALUES\n";
        for (let i = 0; i < dl; i++) {
            query += "   (" + fromid[i] + ', ' + toid[i] + ")";
            if (i < dl - 1)
                query += ',\n';
        }
        return this.tryalaquery(query);
    }
    async MTMsavedata(table, data) {
        this.file && this.file.changesArePresent();
        let tablename = mtmname(table);
        let query = "INSERT INTO " + tablename + " ";
        let keys = Object.keys(data);
        let cols = "";
        let values = this.datatovalues(data, keys, false, false);
        if (values == '')
            return Preject("no data in row for insert query:" + JSON.stringify(data, null, 2));
        cols = keys.join(', ');
        query += "(" + cols + ") VALUES (" + values + ")";
        return this.tryalaquery(query);
    }
    async MTMupdatedata(table, data, where) {
        this.file && this.file.changesArePresent();
        let tablename = mtmname(table);
        let query = "UPDATE " + tablename + " SET ";
        let values = this.datatovalues(data, Object.keys(data), false, true);
        if (values == '')
            return Preject("no data in row for insert query:" + JSON.stringify(data, null, 2));
        query += values;
        if (typeof where !== "string" || where.length < 2)
            return Preject("no where for update query:" + JSON.stringify(data, null, 2));
        query += " WHERE " + where;
        return this.tryalaquery(query);
    }
    async MTMgetids(table, col, fromid) {
        let query = '';
        if (col == table.from)
            query += "SELECT " + table.to + " FROM " + mtmname(table);
        else
            query += "SELECT " + table.from + " FROM " + mtmname(table);
        query += " WHERE " + col + ' = ' + fromid;
        return this.tryalaquery(query);
    }
    async MTMgetrows(table, col, fromid) {
        let query = "SELECT * FROM " + mtmname(table);
        query += " WHERE " + col + ' = ' + fromid;
        return this.tryalaquery(query);
    }
    async MTMgetwhere(table, where) {
        let query = "SELECT * FROM " + mtmname(table);
        query += " WHERE " + where;
        return this.tryalaquery(query);
    }
    async MTMgetalldata(table) {
        let query = "SELECT * FROM " + mtmname(table);
        return this.tryalaquery(query);
    }
    async MTMdeleterows(table, where) {
        this.file && this.file.changesArePresent();
        let query = "DELETE FROM " + mtmname(table) + " WHERE " + where;
        return this.tryalaquery(query);
    }
    async MTMdroptable(table) {
        this.file && this.file.changesArePresent();
        let query = "DROP TABLE " + mtmname(table) + ";";
        if (this.tryalaquery(query) === null)
            return Preject("error in drop table query: " + query);
        return Presolve("");
    }
    async MTMgetIdsFromIdCombos(table, srcCol, ids, where) {
        let tgtCol;
        if (srcCol == table.from)
            tgtCol = table.to;
        else
            tgtCol = table.from;
        let _mtmname = mtmname(table);
        let query = '';
        let AND = (where && 'AND ' + where) || '';
        let idslength = ids.length;
        // seems that support for nested selects in AlaSQL is either buggy or missing altogether - a pity
        for (let i = 0; i < idslength; i++) {
            let id = ids[i];
            query +=
                `SELECT id, ${tgtCol}, COUNT(${tgtCol}) AS cnt
			FROM ${_mtmname} 
			WHERE ${srcCol} IN (${id.join(', ')})
			GROUP BY ${tgtCol}
			ORDER BY cnt DESC; `;
        }
        let midres = await this.tryalaquery(query);
        query = '';
        let filtered = [];
        //TODO: how to do the stuff below in SQL?
        for (let i = 0; i < idslength; i++) {
            let fltr = [];
            let maxcnt = -1;
            for (let mr in midres[i]) {
                if (midres[i][mr].cnt >= maxcnt) {
                    maxcnt = midres[i][mr].cnt;
                    fltr.push(midres[i][mr]);
                }
                else
                    break;
            }
            filtered.push(fltr);
        }
        // AlaSQL should return just as many results as requested...
        for (let i = 0; i < idslength; i++) {
            query +=
                `
			SELECT id, slot
			FROM ${tgtCol}
			WHERE id IN (${filtered[i].map(x => x[tgtCol]).join(', ')})
			${AND}; `;
        }
        return this.tryalaquery(query).then((res) => { return res.map((x) => x.map((y) => y.id)); });
    }
}
//# sourceMappingURL=alasqldb.js.map