"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class TABS_CONTAINER {
    constructor(elemid, dataFeeder, tabSettingsFeeder, onClickCallback, asyncTabSettingsFeeder = null) {
        this.selectedTab = -1;
        this.elemid = elemid;
        this.dataFeeder = dataFeeder;
        this.tabSettingsFeeder = tabSettingsFeeder;
        this.onClickCallback = onClickCallback;
        this.asyncTabSettingsFeeder = asyncTabSettingsFeeder;
        if (!this.asyncTabSettingsFeeder && !this.tabSettingsFeeder)
            INTERFACE.displayError("Internal error: neither sync nor async tab settings feeder specified for " + elemid);
        TABS_CONTAINER.all_tab_containers[elemid] = this;
        if (this.tabSettingsFeeder)
            this.refresh();
        else
            this.asyncRefresh();
    }
    refresh() {
        this.dataFeeder().then((data) => {
            let pos = [];
            let dl = data.length;
            for (let i = 0; i < dl; i++) {
                let ts;
                ts = this.tabSettingsFeeder(data[i]);
                if (!ts)
                    continue;
                if (this.selectedTab == ts.id)
                    ts.selected = true;
                pos.push(ts);
            }
            UTL.getElem(this.elemid).innerHTML = this.buildTabMenu(pos);
        }).catch(LOG.caughtError);
    }
    async asyncRefresh() {
        return this.dataFeeder().then(async (data) => {
            let pos = [];
            let dl = data.length;
            for (let i = 0; i < dl; i++) {
                let ts;
                ts = await this.asyncTabSettingsFeeder(data[i]);
                if (!ts)
                    continue;
                if (this.selectedTab == ts.id)
                    ts.selected = true;
                pos.push(ts);
            }
            UTL.getElem(this.elemid).innerHTML = this.buildTabMenu(pos);
        });
    }
    buttonHtml(settings) {
        let selected = (settings.selected && 'selected') || '';
        let id = (settings.id && settings.idname && 'id="' + settings.idname + settings.id + '"') || '';
        return `<button onclick="TABS_CONTAINER.onClickReroute('${this.elemid}', this, ${settings.id});"
			class="btn conlangbtn ${selected} ${this.elemid}_class_sel" ${id}>${settings.name}</button>`;
    }
    addTabButton(settings) {
        UTL.getElem(this.elemid).innerHTML += this.buttonHtml(settings);
        if (settings.selected)
            setTimeout(TABS_CONTAINER.onClickReroute, 100, this.elemid, UTL.getElem(settings.idname + settings.id), settings.id);
    }
    buildTabMenu(settings) {
        let html = '';
        for (let i = 0; i < settings.length; i++)
            html += this.buttonHtml(settings[i]);
        return html;
    }
    static onClickReroute(elemid, htmlelem, dbid) {
        let tc = TABS_CONTAINER.all_tab_containers[elemid];
        if (!tc) {
            lo(g.error, 'No tabs container with an elem id of ' + elemid + ' found');
            return;
        }
        tc.onClick(htmlelem, dbid);
    }
    onClick(htmlelem, dbid) {
        this.selectedTab = dbid;
        let elements = document.getElementsByClassName(this.elemid + '_class_sel');
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selected');
        htmlelem.classList.add('selected');
        this.onClickCallback(htmlelem, dbid);
    }
}
TABS_CONTAINER.all_tab_containers = {};
//# sourceMappingURL=tabscontainer.js.map