"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class INTERFACE_STATICS {
    static buildASettingField(display, dbcol, table, elemid, dbid, func = 'INTERFACE_STATICS.setConlangProp', misc, value = '') {
        return '<span><div class="settingName">' +
            display +
            '</div><div class="settingInput"><input id="' +
            INTERFACE_STATICS.pfx +
            elemid +
            '" oninput="INTERFACE_STATICS.triggerPropSetting(this, ' +
            "'" + dbcol + "'," +
            "table_e." + table + ',' +
            (dbid || null) + ', ' +
            func +
            (misc && (', ' + JSON.stringify(misc).replace(/(\")/g, "'")) || '') +
            ');" type="text" size="18" value="' + value + '"></input><br/></div></span>';
    }
    static buildASettingCheckbox(display, dbcol, table, elemid, dbid, func = 'INTERFACE_STATICS.setConlangBoolProp', misc) {
        return '<span><div class="settingName">' +
            display +
            '</div><div class="settingInput"><input type="checkbox" id="' +
            INTERFACE_STATICS.pfx +
            elemid +
            '" oninput="INTERFACE_STATICS.triggerPropSetting(this, ' +
            "'" + dbcol + "'," +
            "table_e." + table + ',' +
            (dbid || null) + ', ' +
            func +
            (misc && (', ' + JSON.stringify(misc).replace(/(\")/g, "'")) || '') +
            ');" type="text" size="25"></input><br/></div></span>';
    }
    static buildASettingDropdown(display, elements, dbids, onselect, secondDivId = '', selectId = '', overrideZeroElemCheck = false) {
        if (elements.length != dbids.length) {
            lo(g.error, 'Elements and data for dropdown ' + display + ' are of different length.');
            return '';
        }
        if (elements.length == 0)
            return '';
        if (secondDivId != '')
            secondDivId = 'id="' + secondDivId + '"';
        if (selectId != '')
            selectId = 'id="' + selectId + '"';
        let html = '';
        html += '<span><div class="settingName">' + display;
        html += '</div><div class="settingInput" ' + secondDivId + '>';
        html += '<select ' + selectId + ' onchange="if (this.selectedIndex || ' + overrideZeroElemCheck + '){' + onselect + '}">';
        for (let i = 0; i < elements.length; i++)
            html += `<option value="${dbids[i]}">${elements[i]}</option>`;
        html += '</select>';
        html += '</div></span>';
        return html;
    }
    static barebonesDropdown(elements, dbids, selectId = '', onselect = '') {
        if (elements.length != dbids.length) {
            lo(g.error, 'Elements and data for barebones dropdown are of different length.');
            return '';
        }
        if (elements.length == 0)
            return '';
        if (selectId != '')
            selectId = 'id="' + selectId + '"';
        let html = '';
        html += '<select ' + selectId + ' onchange="if (this.selectedIndex){' + onselect + '}">';
        for (let i = 0; i < elements.length; i++)
            html += `<option value="${dbids[i]}">${elements[i]}</option>`;
        html += '</select>';
        return html;
    }
    static buildALabel(display, secondDivId = '', contents = '') {
        if (secondDivId != '')
            secondDivId = 'id="' + secondDivId + '"';
        let html = '';
        html += '<span><div class="settingName">' + display + '</div>';
        html += '<div class="settingInput" ' + secondDivId + '>';
        html += contents;
        html += '</div></span>';
        return html;
    }
    static buildASelfDestructButton(label, labelid, ondestroy) {
        return `<i id="_self_destruct_${labelid}">
				<button class="btn conlangbtn" id="${labelid}">${label}</button>
				<button class="btn conlangbtn"
					onclick="document.getElementById('_self_destruct_${labelid}').outerHTML = '';${ondestroy}"
					><i class="fa fa-times fa-sm"></i></button>
				</i>`;
    }
    static buildAButton(label, labelid = '', onclick = '', selected = false, cssclass = '') {
        if (onclick != '')
            onclick = 'onclick="' + onclick + '"';
        let sel = (selected && 'selected') || '';
        return `<button class="btn conlangbtn ${sel} ${cssclass}" id="${labelid}" ${onclick}>${label}</button>`;
    }
    static triggerPropSetting(elem, prop, table, dbid, func = INTERFACE_STATICS.setConlangProp, misc) {
        let name = elem.id || UTL.randomstring(15);
        if (SESSION.getactiveconlang() == null || SESSION.file == null)
            return;
        if (INTERFACE_STATICS.timer[name] !== undefined)
            clearTimeout(INTERFACE_STATICS.timer[name]);
        INTERFACE_STATICS.timer[name] = setTimeout(func, 500, prop, elem, table, dbid, misc);
    }
    static updateNameAndSetProp(prop, elem, table, dbid, misc = []) {
        for (let i = 0; i < misc.length; i++) {
            let el = UTL.getElem(misc[i], 2, true);
            el && (el.innerHTML = elem.value);
        }
        INTERFACE_STATICS.setConlangProp(prop, elem, table, dbid);
    }
    static updateElementsByCSSClassAndSetProp(prop, elem, table, misc, dbid) {
        let elements = document.getElementsByClassName(misc);
        for (let i = 0; i < elements.length; i++)
            elements[i].innerHTML = elem.value;
        INTERFACE_STATICS.setConlangProp(prop, elem, table, dbid);
    }
    static setConlangProp(prop, elem, table, dbid) {
        let data = {};
        data[prop] = elem.value;
        if (dbid != null)
            SESSION.file.db.updatedata(table, data, "id = " + dbid + "").catch(LOG.caughtError);
        else
            SESSION.file.db.updatedata(table, data, "conlang = " + SESSION.getactiveconlang().dbid + "").catch(LOG.caughtError);
    }
    static setConlangBoolProp(prop, elem, table, dbid) {
        let data = {};
        data[prop] = elem.checked;
        if (dbid != null)
            SESSION.file.db.updatedata(table, data, "id = " + dbid + "").catch(LOG.caughtError);
        else
            SESSION.file.db.updatedata(table, data, "conlang = " + SESSION.getactiveconlang().dbid + "").catch(LOG.caughtError);
    }
}
INTERFACE_STATICS.pfx = '_if_prop_';
INTERFACE_STATICS.timer = {};
//# sourceMappingURL=interfacestatics.js.map