"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
class IF_IMPORT {
    constructor() {
        return lo(g.error, "This object is only to be used statically.", null);
    }
    static init() {
        let columnsDiv = UTL.getElem('import-columns');
        let counter = -1;
        for (let col in dictionary_sch) {
            if (dictionary_sch[col].hidden)
                continue;
            counter += 1;
            //TODO: remove hardcoding:
            if (dictionary_sch[col].name == 'type') {
                IF_IMPORT.wtIndex = counter;
            }
            let flags = TABLE_SCHEMAS.entry.cols[col].flags;
            let disabled = false;
            let disabledstr = '';
            if (flags & (notnull_p | unique_p | primarykey_p)) {
                disabled = true;
                disabledstr = 'disabled="true"';
            }
            IF_IMPORT.columnsToImport[col] = true;
            let id = 'impchb' + col;
            columnsDiv.innerHTML +=
                `<input type="checkbox" id="${id}" name="${id}" checked="true" ${disabledstr}
			onclick="
				if (!${disabled})
				{
				IF_IMPORT.columnsToImport['${col}']=this.checked;
				IF_IMPORT.constructPreview();
				}">
			<label for="${id}"> Import ${dictionary_sch[col].name}</label>
			&nbsp;&nbsp;&nbsp;`;
        }
        let optionsDiv = UTL.getElem('import-options');
        optionsDiv.innerHTML =
            `
		<div class="clanc-row-container">
			<div class="clanc-header">
				${IF_IMPORT.constructSettingInput(export_separators.cell_separator, 'cell separator')}
				${IF_IMPORT.constructSettingInput(export_separators.row_header, 'row header')}
				${IF_IMPORT.constructSettingInput(export_separators.row_footer, 'row footer')}
			</div>
			<div class="clanc-header">
				${IF_IMPORT.constructSettingInput(export_separators.row_separator, 'row separator')}
				${IF_IMPORT.constructSettingInput(export_separators.file_header, 'file header')}
				${IF_IMPORT.constructSettingInput(export_separators.file_footer, 'file footer')}
			</div>
		</div>
		`;
        return UTL.res_success;
    }
    static refresh() {
        IF_IMPORT.constructPreview();
        return UTL.res_success;
    }
    static constructSettingInput(setting, readableName) {
        let res = `
		<div class="settingName">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${readableName}</div>
		<div class="settingInput"><textarea style="width:100%;height:100%;"
		id="import_option_${setting}"
		onInput="
			IF_IMPORT.separators['${setting}']=this.value;
			IF_IMPORT.constructPreview();"
		>${IF_IMPORT.separators[setting]}</textarea></div><br/>
		`;
        return res;
    }
    static async constructPreview() {
        this.wordTypes = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + SESSION.getactiveconlang().dbid);
        if (!this.dataToImportIsLoaded) {
            UTL.getInputElem('imported-file').value = "";
            UTL.getInputElem('import-preview').value = "";
            return;
        }
        let data = this.formatImportData(true);
        let res = '';
        if (data) {
            for (let i in data)
                data[i] = "[" + data[i].join("]    [") + "]";
            res = data.join('\n');
            let cols = '';
            for (let i in this.columnsToImport)
                if (this.columnsToImport[i])
                    cols += dictionary_sch[i].name + '     ';
            res = cols + '\n' + res;
        }
        else
            res = 'error parsing data';
        UTL.getInputElem('import-preview').value = res;
    }
    static loadFileToImport() {
        UTL.loadFile((file) => {
            IF_IMPORT.loadedDataToImport = file;
            IF_IMPORT.dataToImportIsLoaded = true;
            let lines = file.split('\n');
            if (lines.length > 20)
                lines.splice(10, lines.length - 20, '...');
            let preview = lines.join('\n');
            UTL.getInputElem('imported-file').value = preview;
            IF_IMPORT.constructPreview();
        });
    }
    static async importFromClipboard() {
        let file = UTL.getClipboard();
        IF_IMPORT.loadedDataToImport = file;
        IF_IMPORT.dataToImportIsLoaded = true;
        let lines = file.split('\n');
        if (lines.length > 20)
            lines.splice(10, lines.length - 20, '...');
        let preview = lines.join('\n');
        UTL.getInputElem('imported-file').value = preview;
        file.length && IF_IMPORT.constructPreview();
    }
    static importData() {
        if (!this.dataToImportIsLoaded) {
            UTL.getInputElem('imported-file').value = "";
            UTL.getInputElem('import-preview').value = "";
            INTERFACE.displayInfo("Load file to import data from.");
            return;
        }
        let data = this.formatImportData(false);
        if (data) {
            let clgid = SESSION.getactiveconlang().dbid;
            for (let i in data) {
                data[i].splice(0, 0, UTL.randomint(4294967296));
                data[i].splice(0, 0, 0);
                data[i].splice(0, 0, clgid);
            }
            let cols = ["conlang", "state", "diachronic_id"];
            for (let i in this.columnsToImport)
                if (this.columnsToImport[i])
                    cols.push(i);
            let dataLength = data.length;
            let success = true;
            for (let i = 0; i < dataLength; i++)
                data[i][IF_IMPORT.wtIndex + 3] = IF_IMPORT.parseWordType(data[i][IF_IMPORT.wtIndex + 3], success);
            success && SESSION.file.db.savemultiplerows(table_e.entry, cols, data).then((res) => {
                IF_IMPORT.loadedDataToImport = "";
                IF_IMPORT.dataToImportIsLoaded = false;
                UTL.getInputElem('imported-file').value = "";
                UTL.getInputElem('import-preview').value = "";
                INTERFACE.displayInfo("Data imported successfully.");
            }).catch(LOG.caughtError);
        }
        else
            INTERFACE.displayError("Error parsing data.");
    }
    static parseWordType(wtName, success) {
        if (!success)
            return -1;
        let wtdl = this.wordTypes.length;
        for (let i = 0; i < wtdl; i++)
            if (this.wordTypes[i].name == wtName || this.wordTypes[i].abbreviation == wtName)
                return this.wordTypes[i].id;
        INTERFACE.displayError("Cannot parse this word type: " + wtName + " - it's not defined in the Conlangs menu,");
        success = false;
        return -1;
    }
    static formatImportData(preview) {
        let previewLimitOnEitherSide = 8;
        let previewCutoffPoint = previewLimitOnEitherSide * 2 + 1;
        let importData = this.loadedDataToImport;
        //todo: find whatever is the shmancy way to iterate over an enum indexed array in TS
        this.separators[export_separators.file_header] =
            this.separators[export_separators.file_header].replace('\\t', '\t');
        this.separators[export_separators.file_footer] =
            this.separators[export_separators.file_footer].replace('\\t', '\t');
        this.separators[export_separators.row_separator] =
            this.separators[export_separators.row_separator].replace('\\t', '\t');
        this.separators[export_separators.row_header] =
            this.separators[export_separators.row_header].replace('\\t', '\t');
        this.separators[export_separators.row_footer] =
            this.separators[export_separators.row_footer].replace('\\t', '\t');
        this.separators[export_separators.cell_separator] =
            this.separators[export_separators.cell_separator].replace('\\t', '\t');
        importData = importData.substring(this.separators[export_separators.file_header].length);
        importData = importData.substring(0, importData.length - this.separators[export_separators.file_footer].length);
        let importRows = importData.split(this.separators[export_separators.row_separator]);
        if (importRows.length == 0) {
            INTERFACE.displayError("No rows to import");
            return null;
        }
        let numcolstoimport = 0;
        for (let i in this.columnsToImport)
            if (this.columnsToImport[i])
                numcolstoimport++;
        let finaldata = [];
        let datalength = importRows.length;
        for (let i = 0; i < datalength; i++) {
            if (preview && datalength > previewCutoffPoint && i == previewLimitOnEitherSide) {
                i = datalength - previewLimitOnEitherSide;
                finaldata.push(['...']);
            }
            let row = importRows[i];
            let origrow = row;
            row = row.substring(this.separators[export_separators.row_header].length);
            row = row.substring(0, row.length - this.separators[export_separators.row_footer].length);
            if (row.length < 2)
                continue; // ignore empty lines
            let cells = row.split(this.separators[export_separators.cell_separator]);
            if (cells.length != numcolstoimport) {
                INTERFACE.displayError("Wrong number of cells to import in this row: " + origrow);
                return null;
            }
            let finalcells = [];
            finalcells = cells;
            finaldata.push(finalcells);
        }
        return finaldata;
    }
}
IF_IMPORT.loadedDataToImport = "";
IF_IMPORT.columnsToImport = {};
IF_IMPORT.dataToImportIsLoaded = false;
IF_IMPORT.separators = {
    file_header: '',
    file_footer: '',
    row_header: '',
    row_footer: '',
    row_separator: '\n',
    cell_separator: ', '
};
IF_IMPORT.wordTypes = [];
IF_IMPORT.wtIndex = -1;
IF_IMPORT.htmlpage = {
    html: `<div id="import" class="clanc-container">
			<div id='import-header' class='clanc-header'>
				<button onclick="IF_IMPORT.loadFileToImport();" class="btn">
					&nbsp;&nbsp;<i class="fa fa-folder-open"></i>
					&nbsp;&nbsp;Load file to import&nbsp;&nbsp;</button>
				<button onclick="IF_IMPORT.importFromClipboard();" class="btn">
					&nbsp;&nbsp;<i class="fas fa-paste"></i>
					&nbsp;&nbsp;Import from clipboard&nbsp;&nbsp;</button><br/>
				<div id='import-options'></div><br/>
				<div id='import-columns'></div><br/>
			</div>
			<div id='import-body' class='clanc-body'>
				<div class="clanc-row-container">
					<div class="clanc-body">
						<textarea id="imported-file" disabled="true"
							placeholder="(import file preview)"
							style="width:100%;height:100%;resize: none;color:#555555;"
							></textarea>
					</div>
					<div class="clanc-body">
						<textarea id="import-preview" disabled="true"
							placeholder="(import preview after file to import is loaded)"
							style="width:100%;height:100%;resize: none;color:#555555;"
							></textarea>
					</div>
				</div>
			</div>
			<div id='import-footer' class='clanc-footer'>
				<br/>
				<button onclick="IF_IMPORT.importData();" class="btn">
					&nbsp;&nbsp;<i class="fa fa-file-import"></i>&nbsp;&nbsp;Import data&nbsp;&nbsp;</button>
				<br/>
			</div>
			</div>`,
    refresh: IF_IMPORT.refresh,
    load_page: IF_IMPORT.init
};
//# sourceMappingURL=import.html.js.map