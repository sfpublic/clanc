"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
var export_separators;
(function (export_separators) {
    export_separators["file_header"] = "file_header";
    export_separators["file_footer"] = "file_footer";
    export_separators["row_header"] = "row_header";
    export_separators["row_footer"] = "row_footer";
    export_separators["row_separator"] = "row_separator";
    export_separators["cell_separator"] = "cell_separator";
})(export_separators || (export_separators = {}));
;
class IF_EXPORT {
    constructor() {
        return lo(g.error, "This object is only to be used statically.", null);
    }
    static async init() {
        let columnsDiv = UTL.getElem('export-columns');
        for (let col in dictionary_sch) {
            if (dictionary_sch[col].hidden)
                continue;
            IF_EXPORT.columnsToExport[col] = true;
            let id = 'expchb' + col;
            columnsDiv.innerHTML +=
                `<input type="checkbox" id="${id}" name="${id}" checked="true"
			onclick="
				IF_EXPORT.columnsToExport['${col}']=this.checked;
				IF_EXPORT.constructPreview();">
			<label for="${id}"> Export ${dictionary_sch[col].name}</label>
			&nbsp;&nbsp;&nbsp;`;
        }
        let optionsDiv = UTL.getElem('export-options');
        optionsDiv.innerHTML =
            `
		<div class="clanc-row-container">
			<div class="clanc-header">
				${IF_EXPORT.constructSettingInput(export_separators.cell_separator, 'cell separator')}
				${IF_EXPORT.constructSettingInput(export_separators.row_header, 'row header')}
				${IF_EXPORT.constructSettingInput(export_separators.row_footer, 'row footer')}
			</div>
			<div class="clanc-header">
				${IF_EXPORT.constructSettingInput(export_separators.row_separator, 'row separator')}
				${IF_EXPORT.constructSettingInput(export_separators.file_header, 'file header')}
				${IF_EXPORT.constructSettingInput(export_separators.file_footer, 'file footer')}
			</div>
		</div>
		`;
        let rawWTdata = await SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + SESSION.getactiveconlang().dbid);
        IF_EXPORT.wordTypes = [];
        for (let i in rawWTdata)
            IF_EXPORT.wordTypes[rawWTdata[i].id] = rawWTdata[i];
        return UTL.res_success;
    }
    static refresh() {
        SESSION.file.db.getdata(table_e.entry, ["*"], "`conlang` = " + SESSION.getactiveconlang().dbid).
            then((data) => {
            IF_EXPORT.dataToExport = data;
            IF_EXPORT.constructPreview();
        }).catch(LOG.caughtError);
        return UTL.res_success;
    }
    static constructSettingInput(setting, readableName) {
        let res = `
		<div class="settingName">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${readableName}</div>
		<div class="settingInput"><textarea style="width:100%;height:100%;"
		id="export_option_${setting}"
		onInput="
			IF_EXPORT.separators['${setting}']=this.value;
			IF_EXPORT.constructPreview();"
		>${IF_EXPORT.separators[setting]}</textarea></div><br/>
		`;
        return res;
    }
    static constructPreview() {
        UTL.getInputElem('export-preview').value = this.formatExportData(true);
    }
    static exportData() {
        let dataToExport = this.formatExportData(false);
        let blob = new Blob([dataToExport], { type: "text/plain;charset=utf-8" });
        saveAs(blob, 'exported vocab');
    }
    static formatExportData(preview) {
        let res = '';
        res += this.separators[export_separators.file_header];
        let previewLimitOnEitherSide = 8;
        let previewCutoffPoint = previewLimitOnEitherSide * 2 + 1;
        let columns = Object.keys(this.columnsToExport);
        let exportedColumns = [];
        for (let i in columns) {
            if (this.columnsToExport[columns[i]])
                exportedColumns.push(columns[i]);
        }
        let colnum = exportedColumns.length;
        let datalength = this.dataToExport.length;
        for (let i = 0; i < datalength; i++) {
            if (preview && datalength > previewCutoffPoint && i == previewLimitOnEitherSide) {
                i = datalength - previewLimitOnEitherSide;
                res += '...' + this.separators[export_separators.row_separator];
            }
            res += this.separators[export_separators.row_header];
            let runningcolnum = 0;
            for (let j in exportedColumns) {
                if (this.dataToExport[i][exportedColumns[j]]) {
                    if (exportedColumns[j] == 'part_of_speech')
                        res += IF_EXPORT.wordTypes[this.dataToExport[i][exportedColumns[j]]].abbreviation;
                    else
                        res += this.dataToExport[i][exportedColumns[j]];
                }
                else
                    res += '';
                runningcolnum++;
                if (runningcolnum < colnum)
                    res += this.separators[export_separators.cell_separator];
            }
            res += this.separators[export_separators.row_footer];
            if (i < datalength - 1)
                res += this.separators[export_separators.row_separator];
        }
        res += this.separators[export_separators.file_footer];
        return res;
    }
}
IF_EXPORT.dataToExport = [];
IF_EXPORT.columnsToExport = {};
IF_EXPORT.separators = {
    file_header: '',
    file_footer: '',
    row_header: '',
    row_footer: '',
    row_separator: '\n',
    cell_separator: ', '
};
IF_EXPORT.wordTypes = [];
IF_EXPORT.htmlpage = {
    html: `<div id="export" class="clanc-container">
			<div id='export-header' class='clanc-header'>
				<div id='export-options'>some options</div><br/>
				<div id='export-columns'></div><br/>
			</div>
			<div id='export-body' class='clanc-body'>
				<textarea id="export-preview" disabled="true" placeholder="export preview"
					style="width:100%;height:100%;resize: none;color:#555555;"></textarea>
			</div>
			<div id='export-footer' class='clanc-footer'>
				<br/>
				<button onclick="IF_EXPORT.exportData();" class="btn">
					&nbsp;&nbsp;<i class="fa fa-file-export"></i>&nbsp;&nbsp;Export&nbsp;&nbsp;</button>
				<br/>
			</div>
			</div>`,
    refresh: IF_EXPORT.refresh,
    load_page: IF_EXPORT.init
};
//# sourceMappingURL=export.html.js.map