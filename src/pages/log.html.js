"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
let htmlpage_log = {
    html: '<textarea id="id-log" disabled="true" rows="20" cols="200" ' +
        'style="font-size: 25px; max-width:100%;"></textarea>',
    refresh: () => {
        let elem = UTL.getInputElem('id-log');
        elem && (elem.value = LOG.backlog.join('\n'));
        elem.scrollTop = elem.scrollHeight;
        return UTL.res_success;
    },
    load_page: () => { return UTL.res_success; }
};
//# sourceMappingURL=log.html.js.map