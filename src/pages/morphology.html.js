"use strict";
/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
const PRTB_CLASS = 'paradigmRowTableButton';
const PRTB_BACKGROUND = PRTB_CLASS + '_bg';
const PRB_CLASS = 'paradigmRowButton';
const PRB_BACKGROUND = PRB_CLASS + '_bg';
const AFFIX_CLASS = 'slotButton';
const LEXICON_DIV_ID = 'slickGridWordArea';
class IF_MORPHOLOGY {
    constructor() {
        return lo(g.error, "This object is only to be used statically.", null);
    }
    static init() {
        IF_MORPHOLOGY.posContainer = IF_MORPHOLOGY.createPOStabsContainer();
        IF_MORPHOLOGY.inflectionContainer = IF_MORPHOLOGY.createInflectionTabsContainer();
        IF_MORPHOLOGY.dataView = new Slick.Data.DataView();
        IF_MORPHOLOGY.dataView.setItems([]);
        let columns = [];
        let keys = Object.keys(morphology_sch);
        for (let i = 0; i < keys.length; i++)
            columns.push(DICTIONARY.createcolumnfromschema(keys[i], TABLE_SCHEMAS.entry.cols[keys[i]], morphology_sch[keys[i]], {
                validator: () => { return { valid: true, msg: null }; },
                formatter: (row, cell, value, columnDef, dataContext) => {
                    if (IF_MORPHOLOGY.paradigmDataBackup)
                        if (IF_MORPHOLOGY.paradigmDataBackup.indexedWordTypes[value])
                            return IF_MORPHOLOGY.paradigmDataBackup.indexedWordTypes[value].abbreviation;
                    return 'err' + value;
                },
                editor: Slick.Editors.Text,
                callee: null
            }));
        let options = {
            editable: false,
            enableAddRow: false,
            enableCellNavigation: true,
            asyncEditorLoading: false,
            autoEdit: false,
            multiColumnSort: true,
            numberedMultiColumnSort: true,
            tristateMultiColumnSort: true,
            sortColNumberInSeparateSpan: false,
            showHeaderRow: true,
            headerRowHeight: 30,
            explicitInitialization: true
        };
        IF_MORPHOLOGY.slickGrid = new Slick.Grid("#" + LEXICON_DIV_ID, IF_MORPHOLOGY.dataView, columns, options);
        IF_MORPHOLOGY.slickGrid.setSelectionModel(new Slick.CellSelectionModel());
        UTL.subscribeWithProps(IF_MORPHOLOGY.slickGrid.onSort.subscribe, (props, e, args) => {
            let cols = args.sortCols;
            for (let i = 0, l = cols.length; i < l; i++)
                IF_MORPHOLOGY.dataView.fastSort(cols[i].sortCol.field, cols[i].sortAsc);
            IF_MORPHOLOGY.updateEntries();
        }, { callee: this });
        UTL.subscribeWithProps(IF_MORPHOLOGY.slickGrid.onHeaderRowCellRendered.subscribe, (props, e, args) => {
            let id = UTL.randomstring(20);
            args.node.innerHTML = `<input type='text' id="${id}" placeholder=" search/filter"></input>`;
            args.node.onkeyup = () => { IF_MORPHOLOGY.updateColumnFilters(id, args.column.id); };
        }, { callee: this });
        UTL.subscribeWithProps(IF_MORPHOLOGY.slickGrid.onActiveCellChanged.subscribe, (props, e, args) => {
            IF_MORPHOLOGY.selectedWord = '';
            if (args && props.dataView.getItem(args.row)) {
                IF_MORPHOLOGY.selectedWord = props.dataView.getItem(args.row).root;
                IF_MORPHOLOGY.selectedWordType = props.dataView.getItem(args.row).part_of_speech;
                IF_MORPHOLOGY.refreshInflectionPreview(IF_MORPHOLOGY.paradigmDataBackup);
            }
        }, {
            callee: this,
            dataView: IF_MORPHOLOGY.dataView
        });
        IF_MORPHOLOGY.slickGrid.init();
        return UTL.res_success;
    }
    static refresh() {
        IF_MORPHOLOGY.wordTypeShownOnPage = -1;
        IF_MORPHOLOGY.selectedWordType = -1;
        IF_MORPHOLOGY.selectedParadigmTable = -1;
        IF_MORPHOLOGY.selectedSlot = -1;
        IF_MORPHOLOGY.selectedMorpheme = -1;
        IF_MORPHOLOGY.refreshPOSmenu();
        IF_MORPHOLOGY.selectedWord = '';
        return UTL.res_success;
    }
    static async refreshGridRows() {
        if (IF_MORPHOLOGY.selectedWordType < 0) {
            IF_MORPHOLOGY.dataView.beginUpdate();
            IF_MORPHOLOGY.dataView.setItems([]);
            IF_MORPHOLOGY.dataView.endUpdate();
            IF_MORPHOLOGY.slickGrid.invalidate();
            IF_MORPHOLOGY.slickGrid.updateRowCount();
            IF_MORPHOLOGY.slickGrid.render();
            return;
        }
        let derivedWordTypes = await word_type_t.getDerivedWordTypes(IF_MORPHOLOGY.selectedWordType);
        let data = await SESSION.file.db.getdata(table_e.entry, ["*"], "conlang = " + SESSION.getactiveconlang().dbid +
            ' AND part_of_speech IN (' + derivedWordTypes.join(', ') + ')').catch(LOG.caughtError);
        IF_MORPHOLOGY.dataView.beginUpdate();
        IF_MORPHOLOGY.dataView.setItems(data);
        IF_MORPHOLOGY.dataView.endUpdate();
        IF_MORPHOLOGY.slickGrid.invalidate();
        IF_MORPHOLOGY.slickGrid.updateRowCount();
        IF_MORPHOLOGY.slickGrid.render();
    }
    static updateEntries() {
        IF_MORPHOLOGY.slickGrid.invalidate();
        IF_MORPHOLOGY.slickGrid.updateRowCount();
        IF_MORPHOLOGY.slickGrid.render();
    }
    static updateColumnFilters(elemId, colId) {
        let elem = UTL.getInputElem(elemId);
        if (elem == null) {
            lo(g.error, "no element with id of " + elemId + " to filter by!");
            return;
        }
        this.columnFilters[colId] = elem.value.trim();
        IF_MORPHOLOGY.dataView.refresh();
    }
    static refreshPOSmenu() {
        let POSmenu = UTL.getElem('POSmenu');
        if (!POSmenu) {
            INTERFACE.displayError("Internal error: part of speech menu not found");
            return UTL.res_error;
        }
        IF_MORPHOLOGY.posContainer.refresh();
        IF_MORPHOLOGY.refreshInflectionMenu();
    }
    static refreshInflectionMenu() {
        let interfaceMenu = UTL.getElem('interfaceMenu');
        if (!interfaceMenu) {
            INTERFACE.displayError("Internal error: interface menu not found");
            return UTL.res_error;
        }
        IF_MORPHOLOGY.inflectionContainer.asyncRefresh().then(() => {
            IF_MORPHOLOGY.selectedWordType = IF_MORPHOLOGY.inflectionContainer.selectedTab;
            if (IF_MORPHOLOGY.selectedWordType < 0)
                IF_MORPHOLOGY.selectedWordType = IF_MORPHOLOGY.posContainer.selectedTab;
            IF_MORPHOLOGY.wordTypeShownOnPage = IF_MORPHOLOGY.selectedWordType;
            IF_MORPHOLOGY.refreshGridRows();
            IF_MORPHOLOGY.refreshParadigmArea();
        });
    }
    static refreshParadigmArea() {
        let paradigmArea = UTL.getElem('paradigmArea');
        if (!paradigmArea) {
            INTERFACE.displayError("Internal error: paradigm area not found");
            return UTL.res_error;
        }
        MORPHOLOGY.aggregateWordTypeData(IF_MORPHOLOGY.wordTypeShownOnPage, SESSION.getactiveconlang().dbid)
            .then(async (data) => {
            if (!data) {
                IF_MORPHOLOGY.clearMainPanel();
                IF_MORPHOLOGY.clearSideMenu();
                return;
            }
            IF_MORPHOLOGY.paradigmDataBackup = data;
            IF_MORPHOLOGY.numTables = 0;
            IF_MORPHOLOGY.numWordTypes = 0;
            let allTables = [];
            IF_MORPHOLOGY.countWordTypes(data.indexedWordTypes[IF_MORPHOLOGY.wordTypeShownOnPage], allTables);
            IF_MORPHOLOGY.numTables = allTables.length;
            await IF_MORPHOLOGY.refreshInflectionPreview(data);
            IF_MORPHOLOGY.setupGridCallbacks();
            paradigmArea.innerHTML = await IF_MORPHOLOGY.createParadigmRow(data.indexedWordTypes[IF_MORPHOLOGY.wordTypeShownOnPage]);
            IF_MORPHOLOGY.executeGridCallbacks();
            IF_MORPHOLOGY.setupGridCallbacks();
            await IF_MORPHOLOGY.refreshSideMenu(data);
            IF_MORPHOLOGY.executeGridCallbacks();
        });
    }
    static refreshPreviewOnly(wordType, word, previewElem = 'inflectionPreview') {
        if (word == '') {
            IF_MORPHOLOGY.clearInflectionPreview();
            return;
        }
        IF_MORPHOLOGY.selectedWordType =
            IF_MORPHOLOGY.wordTypeShownOnPage = wordType;
        IF_MORPHOLOGY.selectedWord = word;
        MORPHOLOGY.aggregateWordTypeData(IF_MORPHOLOGY.wordTypeShownOnPage, SESSION.getactiveconlang().dbid)
            .then(async (data) => {
            if (!data) {
                IF_MORPHOLOGY.clearInflectionPreview();
                return;
            }
            IF_MORPHOLOGY.paradigmDataBackup = data;
            IF_MORPHOLOGY.numTables = 0;
            IF_MORPHOLOGY.numWordTypes = 0;
            let allTables = [];
            IF_MORPHOLOGY.countWordTypes(data.indexedWordTypes[IF_MORPHOLOGY.wordTypeShownOnPage], allTables);
            IF_MORPHOLOGY.numTables = allTables.length;
            await IF_MORPHOLOGY.refreshInflectionPreview(data, previewElem, false);
        });
    }
    static countWordTypes(wtNode, allTables) {
        let numrows = wtNode.subtypes.length;
        let numTables = wtNode.paradigm_tables.length;
        IF_MORPHOLOGY.numWordTypes++;
        for (let i = 0; i < numTables; i++)
            if (!allTables.includes(wtNode.paradigm_tables[i].id))
                allTables.push(wtNode.paradigm_tables[i].id);
        for (let i = 0; i < numrows; i++)
            IF_MORPHOLOGY.countWordTypes(wtNode.subtypes[i], allTables);
    }
    static setupGridCallbacks() {
        IF_MORPHOLOGY.callbacks = [];
    }
    static executeGridCallbacks() {
        let cbl = IF_MORPHOLOGY.callbacks.length;
        for (let i = 0; i < cbl; i++)
            IF_MORPHOLOGY.callbacks[i]();
    }
    static refreshSideMenuWithBackup() {
        // refreshSideMenu() will have to be called from generated code
        // so we're doing this little cheat
        // as object data cannot easily be passed by generated code
        // NB: JSON.stringify and JSON.parse can be faulty for complex object data
        IF_MORPHOLOGY.refreshSideMenu(IF_MORPHOLOGY.paradigmDataBackup);
    }
    static async refreshSideMenu(paradigmData) {
        await IF_MORPHOLOGY.refreshPOScontrols(paradigmData);
        IF_MORPHOLOGY.refreshWordArea();
    }
    static async clearSideMenu() {
        IF_MORPHOLOGY.clearPOScontrols();
        IF_MORPHOLOGY.clearWordArea();
    }
    static refreshWordArea() {
        let wordArea = UTL.getElem('wordArea');
        if (!wordArea) {
            INTERFACE.displayError("Internal error: word area not found");
            return UTL.res_error;
        }
        wordArea.style.display = 'visible !important';
        wordArea.style.width = '100%';
    }
    static clearWordArea() {
        let wordArea = UTL.getElem('wordArea');
        if (!wordArea) {
            INTERFACE.displayError("Internal error: word area not found");
            return UTL.res_error;
        }
        wordArea.style.display = 'none !important';
        wordArea.style.width = '0%';
    }
    static clearMainPanel() {
        IF_MORPHOLOGY.clearInterfaceMenu();
        IF_MORPHOLOGY.clearInflectionPreview();
        IF_MORPHOLOGY.clearParadigmArea();
    }
    static clearPOSmenu() {
        let POSmenu = UTL.getElem('POSmenu');
        if (!POSmenu) {
            INTERFACE.displayError("Internal error: POSmenu not found");
            return UTL.res_error;
        }
        POSmenu.innerHTML = '';
    }
    static clearInterfaceMenu() {
        let interfaceMenu = UTL.getElem('interfaceMenu');
        if (!interfaceMenu) {
            INTERFACE.displayError("Internal error: interfaceMenu not found");
            return UTL.res_error;
        }
        interfaceMenu.innerHTML = '';
    }
    static clearInflectionPreview() {
        let inflectionPreview = UTL.getElem('inflectionPreview');
        if (!inflectionPreview) {
            INTERFACE.displayError("Internal error: inflection preview not found");
            return UTL.res_error;
        }
        inflectionPreview.innerHTML = '';
    }
    static clearParadigmArea() {
        let paradigmArea = UTL.getElem('paradigmArea');
        if (!paradigmArea) {
            INTERFACE.displayError("Internal error: paradigmArea not found");
            return UTL.res_error;
        }
        paradigmArea.innerHTML = '';
    }
    static async refreshInflectionPreview(data, previewElem = 'inflectionPreview', previewLabel = true) {
        let inflectionPreview = UTL.getElem(previewElem);
        if (!inflectionPreview) {
            INTERFACE.displayError("Internal error: inflection preview not found");
            return UTL.res_error;
        }
        IF_MORPHOLOGY.setupGridCallbacks();
        inflectionPreview.innerHTML = await IF_MORPHOLOGY.createInflectionPreviewRow(data.indexedWordTypes[IF_MORPHOLOGY.selectedWordType], previewLabel);
        IF_MORPHOLOGY.executeGridCallbacks();
    }
    static async clearPOScontrols() {
        let POScontrols = UTL.getElem('POScontrols');
        if (!POScontrols) {
            INTERFACE.displayError("Internal error: part of speech control menu not found");
            return UTL.res_error;
        }
        POScontrols.innerHTML = '';
    }
    static async refreshPOScontrols(paradigmData) {
        let POScontrols = UTL.getElem('POScontrols');
        if (!POScontrols) {
            INTERFACE.displayError("Internal error: part of speech control menu not found");
            return UTL.res_error;
        }
        let html = '';
        html += '<center><b style="font-size:30px">' + paradigmData.indexedWordTypes[IF_MORPHOLOGY.selectedWordType].name + '</b></center>';
        html += '<br/>';
        html += INTERFACE_STATICS.buildAButton('<i class="fa fa-plus fa-sm"></i> Add table', '', 'paradigm_table_t.add();');
        if (IF_MORPHOLOGY.selectedParadigmTable >= 0) {
            html += INTERFACE_STATICS.buildASettingField('Paradigm table name:', 'name', table_e.paradigm_table, 'table_name_input_field', IF_MORPHOLOGY.selectedParadigmTable, 'IF_MORPHOLOGY.updateParadigmTableName', PRTB_CLASS + IF_MORPHOLOGY.selectedParadigmTable, paradigmData.indexedTables[IF_MORPHOLOGY.selectedParadigmTable].name);
            html += '<br/>';
            html += INTERFACE_STATICS.buildAButton('<i class="fa fa-plus fa-sm"></i> Add an affix slot', '', 'slot_t.add();');
        }
        ;
        if (IF_MORPHOLOGY.selectedSlot >= 0) {
            html += INTERFACE_STATICS.buildAButton('<i class="fa fa-times fa-sm"></i> Remove selected affix slot', '', 'slot_t.remove();');
            html += '<br/>';
            html += INTERFACE_STATICS.buildASettingField('Affix name:', 'name', table_e.slot, 'affix_name_input_field', IF_MORPHOLOGY.selectedSlot, 'IF_MORPHOLOGY.updateSlotName', [AFFIX_CLASS + IF_MORPHOLOGY.selectedSlot], paradigmData.indexedSlots[IF_MORPHOLOGY.selectedSlot].name);
            html += '<br/>';
            html += INTERFACE_STATICS.buildAButton('<i class="fa fa-caret-right fa-sm"></i>  ' +
                '<i class="fa fa-caret-left fa-sm"></i> Move affix slot inward', '', 'slot_t.moveInward(' + IF_MORPHOLOGY.selectedSlot + ');');
            html += INTERFACE_STATICS.buildAButton('<i class="fa fa-caret-left fa-sm"></i>  ' +
                '<i class="fa fa-caret-right fa-sm"></i> Move affix slot outward', '', 'slot_t.moveOutward(' + IF_MORPHOLOGY.selectedSlot + ');');
            html += '<br/>';
            html += await IF_MORPHOLOGY.addSlotCategoryMenu(paradigmData, IF_MORPHOLOGY.selectedSlot);
        }
        POScontrols.innerHTML = html;
    }
    static async addSlotCategoryMenu(paradigmData, slotId) {
        let html = '';
        let categoryNames = [];
        let categoryIds = [];
        let newpos = 0;
        for (let c in paradigmData.indexedCategories) {
            categoryNames.push(paradigmData.indexedCategories[c].name);
            categoryIds.push(paradigmData.indexedCategories[c].id);
        }
        let posdata = await SESSION.file.db.MTMgetrows(DBMTMs['slot_categories'], table_e.slot, slotId).catch(LOG.caughtError);
        for (let g in posdata)
            newpos = Math.max(newpos, posdata[g].position);
        newpos++;
        categoryNames.splice(0, 0, 'add...');
        categoryIds.splice(0, 0, -1);
        html += INTERFACE_STATICS.buildASettingDropdown('Categories: ', categoryNames, categoryIds, 'SESSION.file.db.MTMsavedata(' +
            JSON.stringify(DBMTMs['slot_categories']).replace(/(\")/g, "'") + ', ' +
            '{' +
            "'" + table_e.slot + '\': ' + slotId + ', ' +
            "'" + table_e.category + '\': this.value, ' +
            'position: ' + newpos +
            '})' +
            '.catch(()=>{INTERFACE.displayInfo(\'This category is already added.\');});' +
            'IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[' + slotId + '].categoryIds.push(Number(this.value));' +
            'MORPHOLOGY.updateSlotMorphemes(' + slotId + ').then(()=>{' +
            'IF_MORPHOLOGY.refreshParadigmArea();' +
            'IF_MORPHOLOGY.categoryContainer.refresh();' +
            '});', 'slot_slot_categories');
        html += '<div id="orderedCategories"></div>';
        IF_MORPHOLOGY.callbacks.push(() => {
            IF_MORPHOLOGY.categoryContainer = IF_MORPHOLOGY.createCategoryOrderedListContainer(paradigmData, slotId);
        });
        categoryNames = [];
        categoryIds = [];
        for (let c in paradigmData.indexedCategories) {
            categoryNames.push(paradigmData.indexedCategories[c].name);
            categoryIds.push(paradigmData.indexedCategories[c].id);
        }
        categoryNames.splice(0, 0, 'add...');
        categoryIds.splice(0, 0, -1);
        html += IF_CONLANGS.addCategoryDropdown('Missing categories: ', categoryNames, categoryIds, 'slot_missing_categories', //has to be an index of DBMTMs
        table_e.slot, slotId, 'slot_', 'slot_missing_categories', 'IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[' + slotId + '].missing_categoryIds.push(Number(this.value));' +
            'MORPHOLOGY.updateSlotMorphemes(' + slotId + ').then(()=>{' +
            'IF_MORPHOLOGY.refreshParadigmArea();});');
        let data = paradigmData.indexedSlots[slotId].missing_categoryIds;
        for (let i = 0; i < data.length; i++) {
            html += '</br>' + INTERFACE_STATICS.buildASelfDestructButton(paradigmData.indexedCategories[data[i]].name, 'slot_category_sd_button_' + data[i], 'SESSION.file.db.MTMdeleterows(' +
                JSON.stringify(DBMTMs['slot_missing_categories']).replace(/(\")/g, "'") + ', ' +
                '\'slot = ' + slotId +
                ' AND category = ' + data[i] + '\').then(()=>{' +
                'IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[' + slotId + '].missing_categoryIds = ' +
                'IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[' + slotId + '].missing_categoryIds.filter' +
                '(x=>x!=' + data[i] + ');' +
                'MORPHOLOGY.updateSlotMorphemes(' + slotId + ').then(()=>{' +
                'IF_MORPHOLOGY.refreshParadigmArea();});});');
        }
        return html;
    }
    static createCategoryOrderedListContainer(paradigmData, slotId) {
        let dataFeeder = () => {
            let output = [];
            for (let i in paradigmData.indexedSlots[slotId].category_positions) {
                let cp = {};
                cp.s = paradigmData.indexedSlots[slotId].category_positions[i];
                if (paradigmData.indexedSlots[slotId].missing_categoryIds.includes(cp.s.id))
                    continue;
                cp.data = paradigmData.indexedCategories[cp.s.id];
                output.push(cp);
            }
            output.sort((a, b) => { return a.s.position - b.s.position; });
            return PresolveT(output);
        };
        let orderedListSettingsFeeder = (item) => {
            let ts = {
                name: item.data.name,
                id: item.s.mtmid,
                idname: 'slotcatordlist_',
                position: item.s.position
            };
            return ts;
        };
        let onClickCallback = (htmlelem, dbid) => { };
        let onReorderCallback = (htmlelem, moveUpEl, moveDownEl) => {
            SESSION.file.db.MTMgetwhere(DBMTMs['slot_categories'], 'id = ' + moveUpEl).then(async (data) => {
                await SESSION.file.db.MTMupdatedata(DBMTMs['slot_categories'], { 'position': data[0].position - 1 }, 'id = ' + data[0].id);
                for (let i in IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[data[0].slot].category_positions) {
                    let cp = IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[data[0].slot].category_positions[i];
                    if (cp.id == data[0].category) {
                        cp.position--;
                        break;
                    }
                }
                return SESSION.file.db.MTMgetwhere(DBMTMs['slot_categories'], 'id = ' + moveDownEl);
            }).then(async (data) => {
                await SESSION.file.db.MTMupdatedata(DBMTMs['slot_categories'], { 'position': data[0].position + 1 }, 'id = ' + data[0].id);
                for (let i in IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[data[0].slot].category_positions) {
                    let cp = IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[data[0].slot].category_positions[i];
                    if (cp.id == data[0].category) {
                        cp.position++;
                        break;
                    }
                }
                IF_MORPHOLOGY.refreshParadigmArea();
            }).catch(LOG.caughtError);
        };
        return new ORDERED_LIST_CONTAINER('orderedCategories', dataFeeder, orderedListSettingsFeeder, onClickCallback, onReorderCallback, IF_MORPHOLOGY.onCategoryDestroyCallback);
    }
    static async onCategoryDestroyCallback(htmlelem, elem, slotId, refreshUI = true) {
        if (!slotId)
            slotId = IF_MORPHOLOGY.selectedSlot;
        let posdata = await SESSION.file.db.MTMgetwhere(DBMTMs['slot_categories'], 'id = ' + elem).catch(LOG.caughtError);
        if (posdata) {
            IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[slotId].categoryIds =
                IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[slotId].categoryIds.filter(x => x != posdata[0].category);
            let pos = posdata[0].position;
            let orderdata = await SESSION.file.db.MTMgetwhere(DBMTMs['slot_categories'], 'position > ' + pos +
                ' AND slot = ' + posdata[0].slot +
                ' AND category = ' + posdata[0].category).catch(LOG.caughtError);
            if (orderdata) {
                for (let g in orderdata)
                    await SESSION.file.db.MTMupdatedata(DBMTMs['slot_categories'], { 'position': orderdata[g].position - 1 }, 'id = ' + orderdata[g].id);
            }
            await SESSION.file.db.MTMdeleterows(DBMTMs['slot_categories'], 'slot = ' + posdata[0].slot +
                'AND category = ' + posdata[0].category).catch(LOG.caughtError);
            await MORPHOLOGY.updateSlotMorphemes(slotId);
            refreshUI && IF_MORPHOLOGY.refreshParadigmArea();
        }
    }
    ;
    static updateParadigmTableName(prop, elem, table, dbid, misc) {
        //INTERFACE_STATICS.buildASettingField needs a rewrite, really
        if (!dbid)
            dbid = null;
        INTERFACE_STATICS.updateElementsByCSSClassAndSetProp(prop, elem, table, misc, dbid);
        IF_MORPHOLOGY.paradigmDataBackup.indexedTables[IF_MORPHOLOGY.selectedParadigmTable].name = elem.value;
        IF_MORPHOLOGY.refreshPOScontrols(IF_MORPHOLOGY.paradigmDataBackup);
    }
    static updateSlotName(prop, elem, table, dbid, misc = []) {
        INTERFACE_STATICS.updateNameAndSetProp(prop, elem, table, dbid, misc);
        IF_MORPHOLOGY.paradigmDataBackup.indexedSlots[IF_MORPHOLOGY.selectedSlot].name = elem.value;
        IF_MORPHOLOGY.refreshPOScontrols(IF_MORPHOLOGY.paradigmDataBackup);
    }
    static refreshWordTypeAndTablesSelectionOnCallback(wtId, tableId) {
        var _a, _b;
        let PRB_button_id = PRB_CLASS + wtId;
        let PRB_background_id = PRB_BACKGROUND + wtId;
        let PRTB_button_id = PRTB_CLASS + tableId;
        let PRTB_background_id = PRTB_BACKGROUND + tableId;
        IF_MORPHOLOGY.selectedWordType = wtId;
        IF_MORPHOLOGY.selectedParadigmTable = tableId;
        IF_MORPHOLOGY.selectedSlot = -1;
        IF_MORPHOLOGY.selectedMorpheme = -1;
        IF_MORPHOLOGY.refreshGridRows();
        let elements = document.getElementsByClassName(PRB_CLASS);
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selected');
        (_a = UTL.getElem(PRB_button_id)) === null || _a === void 0 ? void 0 : _a.classList.add('selected');
        elements = document.getElementsByClassName(PRB_BACKGROUND);
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selectedHighlight');
        if (IF_MORPHOLOGY.numWordTypes > 1)
            (_b = UTL.getElem(PRB_background_id)) === null || _b === void 0 ? void 0 : _b.classList.add('selectedHighlight');
        elements = document.getElementsByClassName(PRTB_CLASS);
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selected');
        elements = document.getElementsByClassName(AFFIX_CLASS);
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selected');
        if (tableId >= 0) {
            elements = document.getElementsByClassName(PRTB_button_id);
            for (let i = 0; i < elements.length; i++)
                elements[i].classList.add('selected');
        }
        elements = document.getElementsByClassName(PRTB_BACKGROUND);
        for (let i = 0; i < elements.length; i++)
            elements[i].classList.remove('selectedHighlight');
        if (tableId >= 0 && IF_MORPHOLOGY.numTables > 1) {
            elements = document.getElementsByClassName(PRTB_background_id);
            for (let i = 0; i < elements.length; i++)
                elements[i].classList.add('selectedHighlight');
        }
    }
    static async createParadigmRow(wtNode, uppernode = null) {
        let res = '';
        let selected = (IF_MORPHOLOGY.selectedWordType == wtNode.id);
        let PRB_button_id = PRB_CLASS + wtNode.id;
        let PRB_background_id = PRB_BACKGROUND + wtNode.id;
        let klass = 'autofit-interface-frame fadeInAndOut ' + PRB_BACKGROUND;
        let name = wtNode.name;
        if (uppernode != null)
            name += ' (' + uppernode.name + ')';
        if (selected && IF_MORPHOLOGY.numWordTypes > 1)
            klass += ' selectedHighlight';
        res += `<div class="${klass}" id="${PRB_background_id}">
		        <div class="clanc-row-container">
		            <div class="clanc-header settingInput">`;
        let fnc = `
				IF_MORPHOLOGY.refreshWordTypeAndTablesSelectionOnCallback(${wtNode.id}, -1);
				IF_MORPHOLOGY.refreshInflectionPreview(IF_MORPHOLOGY.paradigmDataBackup);
				IF_MORPHOLOGY.refreshSideMenuWithBackup();`;
        res += INTERFACE_STATICS.buildAButton(name, PRB_button_id, fnc, selected, PRB_CLASS);
        res += `</div>
			        <div class="clanc-body">
			        <center>
			        <div class="clanc-row-container" style="height: 200px;">`;
        let numTables = wtNode.paradigm_tables.length;
        for (let i = 0; i < numTables; i++)
            res += await IF_MORPHOLOGY.createParadigmTable(wtNode.paradigm_tables[i], wtNode.id, !wtNode.missing_tables.includes(wtNode.paradigm_tables[i].id));
        res += `</div>
			        </center>
			        </div>
		        </div>
		        </div>`;
        let additionalRows = '';
        let numrows = wtNode.subtypes.length;
        for (let i = 0; i < numrows; i++)
            additionalRows += await IF_MORPHOLOGY.createParadigmRow(wtNode.subtypes[i], wtNode);
        return res + additionalRows;
    }
    static async createParadigmTable(tableNode, wtId, inherited) {
        let res = '';
        let selected = (IF_MORPHOLOGY.selectedParadigmTable == tableNode.id);
        let name = tableNode.name;
        let PRTB_button_id = PRTB_CLASS + tableNode.id;
        let PRTB_background_id = PRTB_BACKGROUND + tableNode.id;
        let klass = PRTB_BACKGROUND + ' fadeInAndOut';
        if (selected && IF_MORPHOLOGY.numTables > 1)
            klass += ' selectedHighlight';
        res += `<div class="clanc-body morphology-paradigm-border ${klass} ${PRTB_background_id}">
		        <div class="clanc-container">
		        <div class="clanc-header">`;
        let fnc = `
			IF_MORPHOLOGY.refreshWordTypeAndTablesSelectionOnCallback(${wtId}, ${tableNode.id});
			IF_MORPHOLOGY.refreshInflectionPreview(IF_MORPHOLOGY.paradigmDataBackup);
			IF_MORPHOLOGY.refreshSideMenuWithBackup();`;
        res += INTERFACE_STATICS.buildAButton(name, PRTB_button_id, fnc, selected, PRTB_CLASS + ' ' + PRTB_button_id);
        if (!inherited) {
            res += `<button
				onclick="paradigm_table_t.reinherit(${wtId}, ${tableNode.id});"
				class="btn conlangbtn"> <i class="fa fa-plus fa-sm"></i></button>`;
        }
        else {
            if (wtId == tableNode.word_type) // only really delete it if this is the WT containing it
                res += `<button
					onclick="paradigm_table_t.delete(${tableNode.id});"
					class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i></button>`;
            else //otherwise mark it as unused
                res += `<button
					onclick="paradigm_table_t.uninherit(${wtId}, ${tableNode.id});"
					class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i></button>`;
            res += '</div>';
            let currentRowWordType = IF_MORPHOLOGY.paradigmDataBackup.indexedWordTypes[wtId];
            let slotsToUse = [];
            res += '<div class="clanc-row-container">';
            let tnsl = tableNode.slots.length;
            for (let i = 0; i < tnsl; i++) {
                if (await IF_MORPHOLOGY.paradigmDataBackup.indexedWordTypes[wtId].isDerivedFrom(tableNode.slots[i].word_type, true))
                    slotsToUse.push(tableNode.slots[i]);
            }
            slotsToUse.sort((a, b) => { return a.position - b.position; });
            let numslots = 0;
            let lastSlot = 0;
            for (let i in slotsToUse) {
                if (!currentRowWordType.missing_slots.includes(slotsToUse[i].id)) {
                    numslots++;
                    lastSlot = slotsToUse[i].id;
                }
            }
            let firstSlot = true;
            for (let i in slotsToUse) {
                let SGid = 'slot_' + UTL.randomstring(15);
                res += '<div class="clanc-body">';
                let affix_button_id = AFFIX_CLASS + slotsToUse[i].id;
                let fnc = `IF_MORPHOLOGY.refreshWordTypeAndTablesSelectionOnCallback(${wtId}, ${tableNode.id});
				           IF_MORPHOLOGY.selectedSlot = ${slotsToUse[i].id};
				           IF_MORPHOLOGY.selectedMorpheme = -1;
				           IF_MORPHOLOGY.refreshParadigmArea();`;
                let selected = (IF_MORPHOLOGY.selectedSlot == slotsToUse[i].id);
                res += INTERFACE_STATICS.buildAButton(slotsToUse[i].name, affix_button_id, fnc, selected, AFFIX_CLASS);
                if (currentRowWordType.missing_slots.includes(slotsToUse[i].id)) {
                    res += `<button
						onclick="slot_t.reinherit(${wtId}, ${slotsToUse[i].id});"
						class="btn conlangbtn"> <i class="fa fa-plus fa-sm"></i></button>`;
                    res += '<br/><a style="opacity:0.4;">(skipped)</a></div>';
                    continue; // slot is not inherited, skip it
                }
                if (wtId != tableNode.word_type) {
                    // add the possibility to uninherit it only if it's inherited in the first place
                    res += `<button
						onclick="slot_t.uninherit(${wtId}, ${slotsToUse[i].id});"
						class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i></button>`;
                }
                if (numslots > 1) {
                    if (firstSlot) {
                        res += '<br/>(applied first)';
                        firstSlot = false;
                    }
                    if (lastSlot == slotsToUse[i].id)
                        res += '<br/>(applied last)';
                }
                res += `<div id="${SGid}" style="height: 120px;"></div>
				        </div>`;
                let data = new slotTable_t(slotsToUse[i].id);
                if (!data.table.length)
                    continue;
                if (!data.table[0].length)
                    continue;
                let datalength = data.table[0].length;
                let cols = [];
                for (let i = 0; i < datalength; i++)
                    cols[i] = IF_MORPHOLOGY.slotColOptions('' + i);
                IF_MORPHOLOGY.callbacks.push(() => {
                    let grid = slotsToUse[i].slickgrid;
                    grid = new Slick.Grid("#" + SGid, data, cols, IF_MORPHOLOGY.slotGridOptions);
                    grid.setSelectionModel(new Slick.CellSelectionModel());
                    UTL.subscribeWithProps(grid.onBeforeEditCell.subscribe, (props, e, args) => {
                        let cell = props.data.table[args.row][args.cell];
                        if (!cell)
                            return;
                        if (UTL.morphologyEditorHint) {
                            UTL.morphologyEditorHint = false;
                            INTERFACE.displayHint("Ctrl + Enter to save, Esc to cancel");
                        }
                        if (cell.id >= 0) {
                            args.item[args.cell] = cell.value;
                        }
                    }, { data: data });
                    UTL.subscribeWithProps(grid.onCellChange.subscribe, (props, e, args) => {
                        let cell = props.data.table[args.row][args.cell];
                        if (!cell)
                            return;
                        if (cell.id >= 0) {
                            cell.value = args.item[args.cell];
                            cell.preview = MORPHOLOGY.morphemeSCsToPreview(cell.value);
                            SESSION.file.db.updatedata(table_e.morpheme, { 'SCs': args.item[args.cell] }, "id = " + cell.id);
                            IF_MORPHOLOGY.paradigmDataBackup.indexedMorphemes[cell.id].SCs = args.item[args.cell];
                            IF_MORPHOLOGY.refreshInflectionPreview(IF_MORPHOLOGY.paradigmDataBackup);
                        }
                    }, { data: data });
                    UTL.subscribeWithProps(grid.onActiveCellChanged.subscribe, (props, e, args) => {
                        if (args == null) {
                            IF_MORPHOLOGY.selectedMorpheme = -1;
                        }
                        else {
                            let cell = props.data.table[args.row][args.cell];
                            if (!cell)
                                return;
                            if (cell.id >= 0) {
                                IF_MORPHOLOGY.selectedMorpheme = cell.id;
                            }
                        }
                    }, { data: data });
                    UTL.subscribeWithProps(grid.onActiveCellPositionChanged.subscribe, (props, e, args) => {
                        //always show the SlickGrid LongTextEditor
                        let editor = props.grid.getCellEditor();
                        editor && (editor.hide = undefined);
                    }, { grid: grid });
                });
                //todo: expand and collapse the tables
            }
        }
        res += '</div></div></div>';
        return res;
    }
    static async createInflectionPreviewRow(wtNode, previewLabel = true) {
        let res = '';
        res += '<div class="autofit-interface-frame">';
        if (previewLabel) {
            res += `<div class="clanc-row-container">
			        <div class="clanc-header settingInput">
			        ${INTERFACE_STATICS.buildAButton(wtNode.name + ' preview')}
			        </div>
			        <div class="clanc-body">`;
        }
        if (previewLabel)
            res += '<center><div class="clanc-row-container">';
        else
            res += '<center><div class="clanc-container">';
        let numTables = wtNode.paradigm_tables.length;
        for (let i = 0; i < numTables; i++)
            if (!wtNode.missing_tables.includes(wtNode.paradigm_tables[i].id))
                res += await IF_MORPHOLOGY.createInflectionPreviewTable(wtNode.paradigm_tables[i], wtNode.id);
        res += '</div></center>';
        if (previewLabel) {
            res += '</div></div>';
        }
        res += '</div>';
        return res;
    }
    static async createInflectionPreviewTable(tableNode, wtId) {
        let res = '';
        let slotsToUse = [];
        let currentRowWordType = IF_MORPHOLOGY.paradigmDataBackup.indexedWordTypes[wtId];
        for (let i in tableNode.slots)
            if (!currentRowWordType.missing_slots.includes(tableNode.slots[i].id) &&
                (await IF_MORPHOLOGY.paradigmDataBackup.indexedWordTypes[wtId].isDerivedFrom(tableNode.slots[i].word_type, true)))
                slotsToUse.push(tableNode.slots[i]);
        slotsToUse.sort((a, b) => { return a.position - b.position; });
        let slotIds = slotsToUse.map(x => x.id);
        let segments = "";
        let miscdata = (await SESSION.file.db.getdata(table_e.miscdata, ['*'], 'conlang = ' + SESSION.getactiveconlang().dbid).catch(LOG.caughtError))[0];
        if (miscdata)
            segments = miscdata.segments || '';
        let data = new previewTable_t(segments);
        await data.init(slotIds, IF_MORPHOLOGY.selectedWord);
        let SGid = 'slot_preview_' + UTL.randomstring(10);
        let cols = [];
        if (!data.table.length)
            return '';
        if (!data.table[0].length)
            return '';
        let datalength = data.table[0].length;
        res += '<div class="clanc-body morphology-paradigm-border">';
        res += '<div class="clanc-container">';
        res += '<div class="clanc-header">';
        res += INTERFACE_STATICS.buildALabel(tableNode.name);
        res += '</div>';
        res += '<div class="clanc-row-container">';
        res += '<div class="clanc-body" style="height: 200px;">';
        res += '<div id="' + SGid + '" style="height: 200px;">';
        res += '</div>';
        res += '</div>';
        for (let i = 0; i < datalength; i++)
            cols[i] = IF_MORPHOLOGY.previewColOptions('' + i);
        IF_MORPHOLOGY.callbacks.push(() => {
            let grid = tableNode.slickgrid;
            grid = new Slick.Grid("#" + SGid, data, cols, IF_MORPHOLOGY.previewGridOptions);
            grid.setSelectionModel(new Slick.CellSelectionModel());
        });
        res += '</div>';
        res += '</div>';
        res += '</div>';
        return res;
    }
    static validator_txt_notnull(value) {
        if (value == null || value == undefined || !value.length)
            return { valid: false, msg: "This is a required field." };
        else if (typeof value !== "string")
            return { valid: false, msg: "This field should be string." };
        else
            return { valid: true, msg: null };
    }
    static slotColOptions(name) {
        return {
            name: name,
            sortable: false,
            hidden: false,
            id: name,
            field: name,
            validator: IF_MORPHOLOGY.validator_txt_notnull,
            editor: Slick.Editors.LongText,
            headerCssClass: 'sgNoColumnHeader'
        };
    }
    static previewColOptions(name) {
        return {
            name: name,
            sortable: false,
            hidden: false,
            id: name,
            field: name,
            headerCssClass: 'sgNoColumnHeader'
        };
    }
    static createPOStabsContainer() {
        let dataFeeder = () => {
            let clg = SESSION.getactiveconlang();
            if (clg.dbid < 0)
                return PresolveT([]);
            return SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + clg.dbid);
        };
        let tabSettingsFeeder = (item) => {
            if (!item.part_of_speech)
                return null;
            if (item.inherits_from != null)
                return null;
            let ts = {
                name: item.name,
                id: item.id,
                idname: 'postab_'
            };
            return ts;
        };
        let onClickCallback = () => {
            IF_MORPHOLOGY.inflectionContainer.selectedTab = -1;
            IF_MORPHOLOGY.selectedParadigmTable = -1;
            IF_MORPHOLOGY.selectedSlot = -1;
            IF_MORPHOLOGY.selectedMorpheme = -1;
            IF_MORPHOLOGY.refreshPOSmenu();
        };
        return new TABS_CONTAINER('POSmenu', dataFeeder, tabSettingsFeeder, onClickCallback);
    }
    static createInflectionTabsContainer() {
        let dataFeeder = () => {
            let clg = SESSION.getactiveconlang();
            if (clg.dbid < 0)
                return PresolveT([]);
            return SESSION.file.db.getdata(table_e.word_type, ['*'], 'conlang = ' + clg.dbid);
        };
        let tabSettingsFeeder = (async (item) => {
            if (IF_MORPHOLOGY.posContainer.selectedTab < 0)
                return null;
            if (item.id == IF_MORPHOLOGY.posContainer.selectedTab)
                return null;
            let res = await IF_CONLANGS.aggregateParadigmData(item.id, SESSION.getactiveconlang().dbid);
            if (!res.PoSlist.includes(IF_MORPHOLOGY.posContainer.selectedTab))
                return null;
            let ts = {
                name: item.name,
                id: item.id,
                idname: 'infltab_'
            };
            return ts;
        });
        let onClickCallback = () => {
            IF_MORPHOLOGY.selectedParadigmTable = -1;
            IF_MORPHOLOGY.selectedSlot = -1;
            IF_MORPHOLOGY.selectedMorpheme = -1;
            IF_MORPHOLOGY.refreshInflectionMenu();
        };
        return new TABS_CONTAINER('interfaceMenu', dataFeeder, null, onClickCallback, tabSettingsFeeder);
    }
}
IF_MORPHOLOGY.htmlpage = {
    html: `
			<div class="clanc-row-container" style="max-height: 100%;">

				<div class="clanc-body">
					<div class="clanc-container">
						<div class="clanc-header" id="POSmenu">
						</div>
						<div class="clanc-header" id="interfaceMenu">
						</div>
						<div class="clanc-header" id="inflectionPreview">
						</div>
						<div class="clanc-body" id="paradigmArea" style="overflow: auto;">
						</div>
					</div>
				</div>
				
					
				<div class="clanc-footer" id="sideMenu" style="min-width:300px">
					<div class="clanc-container">
						<div class="clanc-header" id="POScontrols">
						</div>
						<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;
						<div class="clanc-body" id="wordArea" style="width:100%;height:100%;">
							<div id="${LEXICON_DIV_ID}" class="clanc-body" style="width:100%;height:100%;">
							</div>
						</div>
					</div>
				</div>
			</div>
			`,
    refresh: IF_MORPHOLOGY.refresh,
    load_page: IF_MORPHOLOGY.init
};
IF_MORPHOLOGY.wordTypeShownOnPage = -1;
IF_MORPHOLOGY.selectedWordType = -1;
IF_MORPHOLOGY.selectedParadigmTable = -1;
IF_MORPHOLOGY.selectedSlot = -1;
IF_MORPHOLOGY.selectedMorpheme = -1;
IF_MORPHOLOGY.numTables = 0;
IF_MORPHOLOGY.numWordTypes = 0;
IF_MORPHOLOGY.slotGridOptions = {
    editable: true,
    autoEdit: false,
    defaultColumnWidth: 30,
    enableColumnReorder: false,
    forceFitColumns: true,
    topPanelHeight: 0
};
IF_MORPHOLOGY.previewGridOptions = {
    editable: false,
    defaultColumnWidth: 30,
    enableColumnReorder: false,
    forceFitColumns: true,
    topPanelHeight: 0
};
IF_MORPHOLOGY.callbacks = [];
IF_MORPHOLOGY.selectedWord = "";
IF_MORPHOLOGY.columnFilters = {};
//# sourceMappingURL=morphology.html.js.map