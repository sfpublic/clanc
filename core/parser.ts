/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

enum command_t
	{
	DEFINE_GROUP            ,
	SOUND_CHANGE            ,
	SYLLABIFICATION         ,
	SYLLABLE_LABELING       ,
	FEET_LABELING           ,
	PATTERN                 ,
	ILLEGAL                 ,
	};

const CSVseparator = ', ';

class outInfo_t
	{
	origWord:string = '';
	wordMatches:string = '';
	}

class executeCommandResult_t 
	{
	word: parserWord_t;
	startCommand: number;
	jumpToStart = false;
	skipNext = false;
	success = true;
	repeatCounter = 0;
	principalFormIndex = -1;
	outInfo: null | outInfo_t = null;

	constructor(word: parserWord_t, startCommand:number)
		{
		this.word = word;
		this.startCommand = startCommand;
		}
	}

class commandArray_t
	{
	success = true;
	error = "no error";
	commands:SCAcommand_t[] = [];
	}

class parserWord_t
	{
	isSecondaryPrincipalForm:boolean;
	originalString:string;
	pos:string = '';
	otherInfo:string='';
	principalForms:parserWord_t[] = [];
	surfaceRepresentation:string;
	innerRepresentation:string;
	isCaps:boolean = false;
	isCapitalised:boolean = false;
	parent:parserWord_t|null = null;

	//NOTE: any code outside this class should only ever fill in the first argument of the constructor
	constructor(word:string, isPrincipalForm?:boolean, parent:parserWord_t|null=null)
		{
		if (isPrincipalForm !== true)
			{
			this.isSecondaryPrincipalForm = false;
			this.originalString = word;
			let input = word.trim().split(CSVseparator);
			word = input[0].trim();
			this.pos = '';
			if (input[1] != undefined) this.pos = input[1].trim();
			this.otherInfo = '';
			this.principalForms = [];//TODO in a unified way maybe?
			if (input[2] != undefined && input[2].trim().length > 0)//TODO: generalise the magic number of pwfs
				this.principalForms[0] = new parserWord_t(input[2], false, this);
			if (input[3] != undefined && input[3].trim().length > 0)
				this.principalForms[1] = new parserWord_t(input[3], false, this);
			if (input[4] != undefined) this.otherInfo = input[2].trim();

			this.surfaceRepresentation = word;
			this.innerRepresentation = ' '+word.toLowerCase()+' ';
			//spaces are added so that '#' can match for word boundaries - # is converted to [,. ] later
			this.isCaps = false; //if the entire word is in capital letters
			this.isCapitalised = false; //if only the first letter is capital
			if (word.length > 1 && word.toUpperCase() == word)
				this.isCaps = true;
			else if (word.length > 0 && word[0].toUpperCase() == word[0] && UTL.itIsALetter(word[0]))
				this.isCapitalised = true;
			}
		else
			{
			this.isSecondaryPrincipalForm = true;
			word = word.trim();
			this.originalString = word;
			this.surfaceRepresentation = word;
			this.innerRepresentation = ' '+word.toLowerCase()+' ';
			this.parent = parent;
			}
		}
	
	getPrincipalForm(index:number)
		{
		if (this.isSecondaryPrincipalForm !== false)
			{
			lo(g.error,"word is a subdefinition: "+this.surfaceRepresentation);
			return undefined;
			}
		if (index == 0) return this;
		return this.principalForms[index-1];
		}
	
	getResultingString():string
		{
		if (!this.isSecondaryPrincipalForm) return this.surfaceRepresentation;
		let result:string = this.surfaceRepresentation;
		result += CSVseparator;
		//TODO: generalise the magic number of pwfs
		if (this.principalForms[0] !== undefined) result += this.principalForms[0].surfaceRepresentation;
		result += CSVseparator;
		if (this.principalForms[1] !== undefined) result += this.principalForms[1].surfaceRepresentation;
		result += CSVseparator;
		result += this.pos;

		return result;
		}
	
	duplicate()
		{
		return new parserWord_t(this.originalString);
		}
	
	updateInnerRepresentation(innerRepresentation:segmentation_t)
		{
		this.innerRepresentation = ' '+wordToInner(innerRepresentation, this.innerRepresentation)+' ';
		if (!this.isSecondaryPrincipalForm)
			this.principalForms.forEach(element => element.updateInnerRepresentation(innerRepresentation));
		}
	
	resetInnerRepresentation(oldInnerRepresentation:segmentation_t, newInnerRepresentation:segmentation_t)
		{
		let temp = innerToHumanReadable(oldInnerRepresentation, this.innerRepresentation);
		this.innerRepresentation = ' '+wordToInner(newInnerRepresentation, temp)+' ';
		if (!this.isSecondaryPrincipalForm)
			this.principalForms.forEach(element => element.resetInnerRepresentation(oldInnerRepresentation, newInnerRepresentation));
		}
	
	setSurfaceRepresentationFromInnerRepresentation(segments:segmentation_t)
		{
		this.surfaceRepresentation = innerToHumanReadable(segments, this.innerRepresentation).trim();

		if (this.getIsCaps())
			this.surfaceRepresentation = this.surfaceRepresentation.toUpperCase();
		else if (this.getIsCapitalised())
			this.surfaceRepresentation = this.surfaceRepresentation[0].toUpperCase() + this.surfaceRepresentation.substring(1);
		
		if (!this.isSecondaryPrincipalForm)
			this.principalForms.forEach(element => element.setSurfaceRepresentationFromInnerRepresentation(segments));
		}
		
	resetInnerRepresentationToSurfaceRepresentation()
		{
		this.innerRepresentation = ' '+this.surfaceRepresentation+' ';
		if (!this.isSecondaryPrincipalForm)
			this.principalForms.forEach(element => element.resetInnerRepresentationToSurfaceRepresentation());
		}
	
	innerAndSurfaceRepresentation()
		{
		return this.surfaceRepresentation + ' ('+this.innerRepresentation+')';
		}

	getIsCaps():boolean
		{
		if (this.isSecondaryPrincipalForm) return this.parent!.getIsCaps();
		else return this.isCaps;
		}

	getIsCapitalised():boolean
		{
		if (this.isSecondaryPrincipalForm) return this.parent!.getIsCapitalised();
		else return this.isCapitalised;
		}
	}

class PARSER
	{
	private static accumulateSegmentFrequencies(segmentFrequencies:any, word:parserWord_t)
		{
		for (let i = 1; i < word.innerRepresentation.length - 1; i++)
			{
			if (word.innerRepresentation[i] == ' ') continue;
			if (segmentFrequencies[word.innerRepresentation[i]] === undefined)
				{
				segmentFrequencies[word.innerRepresentation[i]] = 1;
				segmentFrequencies.dict[segmentFrequencies.dict.length] = word.innerRepresentation[i];
				}
			else segmentFrequencies[word.innerRepresentation[i]]++;
			segmentFrequencies.totalCount++;
			}
		}

	private static reportSegmentFrequencies(segmentFrequencies:any, segments:segmentation_t)
		{
		let l = segmentFrequencies.dict.length;
		for (let i = 0; i < l; i++)
			{
			let count = segmentFrequencies[segmentFrequencies.dict[i]];
			let perc = 100 * count / segmentFrequencies.totalCount;
			let perc2 = Math.round(perc*100)/100;
			let seg = innerToHumanReadable(segments, segmentFrequencies.dict[i]);
			lo(g.SCA,"'"+seg+"' occurs "+count+" times ("+perc2+"% of all segments)");
			}
		}

	static executeCommands(workedOnWords:string[], soundChanges:string):string
		{
		let tempWords = [];
		for(let i = 0; i < Math.min(10, workedOnWords.length); i++)
			tempWords[i] = workedOnWords[i];
		if (workedOnWords.length > 10)
			tempWords[10] = "...";
		lo(g.SCA, 'Starting execution of the following command: ' + JSON.stringify(tempWords));

		let commandArray = PARSER.parseAllLines(soundChanges);
		if (!commandArray.success)
			{
			INTERFACE.displayError(commandArray.error);
			return '';
			}
		let commands = commandArray.commands;
		
		let commandsLength = commands.length;
		let workedOnWordsLength = workedOnWords.length;

		if (!workedOnWordsLength)
			{
			INTERFACE.displayError('no words given as input');
			return '';
			}
		
		let segmentFrequencies = {totalCount:0, dict:[]};
		let segments:segmentation_t;
		
		for(let i = 0; i < commandsLength; i++)
			commands[i].numberOfExecutions = 0;

		for(let i = 0; i < workedOnWordsLength; i++)
			{
			let word = new parserWord_t(workedOnWords[i]);
			let startCommand = 0;
			let commandResult = new executeCommandResult_t(word, startCommand);

			for (let j = startCommand; j < commandsLength; j++)
				{
				commandResult = PARSER.executeACommand(commandResult, commands, j, true);
				if (commandResult.success === false) break;
				}

			let lastGroupDefinitionIndex = commands[commandsLength - 1].lastGroupDefinitionCommandIndex;
			if (lastGroupDefinitionIndex == -1)
				segments = new segmentation_t();
			else
				segments = commands[lastGroupDefinitionIndex].allCurrentSegmentsAndGroups;

			commandResult.word.setSurfaceRepresentationFromInnerRepresentation(segments);
			workedOnWords[i] = commandResult.word.getResultingString();//commandResult.word.surfaceRepresentation;
			PARSER.accumulateSegmentFrequencies(segmentFrequencies, word);
			//if (word.pos!==undefined) workedOnWords[i]+=CSVseparator+word.pos;
			}
		PARSER.reportSegmentFrequencies(segmentFrequencies, segments!);
		return workedOnWords.join('\n');
		}

	static executeCommandsOnASingleWord(word:parserWord_t|string, commands:SCAcommand_t[]|string, deriveWordsUpToCommand?:number)
		{
		let parsedWord: parserWord_t;
		if (typeof word === 'string')
			parsedWord = new parserWord_t(word);
		else
			parsedWord = word;

		let parsedCommands:SCAcommand_t[];
		if (typeof commands === 'string')
			{
			let commandArray = PARSER.parseAllLines(commands);
			if (!commandArray.success)
				{
				INTERFACE.displayError(commandArray.error);
				return {word:parsedWord.getResultingString(), comment:'no sound changes provided'};
				}
			parsedCommands = commandArray.commands;
			}
		else
			parsedCommands = <SCAcommand_t[]>commands;

		let commandsLength = parsedCommands.length;
		if (!commandsLength)
			return {word:parsedWord.getResultingString(), comment:'no sound changes provided'};

		let offset = 1;
		let startCommand = 0;
		let commandResult = new executeCommandResult_t(parsedWord, startCommand);
		let result = '';
		let commandNumOffset = 0;

		if (!deriveWordsUpToCommand)
			deriveWordsUpToCommand = commandsLength;
		else
			deriveWordsUpToCommand = Math.min(deriveWordsUpToCommand, commandsLength);

		for (let j = startCommand; j < deriveWordsUpToCommand; j++)
			{
			commandResult.outInfo = null;
			commandResult = PARSER.executeACommand(commandResult, parsedCommands, j, false);
			
			offset -= parsedCommands[j].leadingEmptyLines + 1;
			
			if (offset > 0) commandNumOffset++;
			
			if (offset <= 0)
			if (commandResult.outInfo !== undefined && commandResult.outInfo !== null)
				{
				for(let lel = 0; lel < parsedCommands[j].leadingEmptyLines;lel++) result += '\n';
				result += (j-commandNumOffset)+'. ';
				switch(parsedCommands[j].command)
					{
					case command_t.SOUND_CHANGE:
						if (commandResult.success)
							{
							result += '('+parsedCommands[j].numberOfExecutions+') ';
							let lastGroupDefinitionIndex = parsedCommands[j].lastGroupDefinitionCommandIndex;

							let segments: segmentation_t;
				
							if (lastGroupDefinitionIndex == -1)
								segments = new segmentation_t();
							else
								segments = parsedCommands[lastGroupDefinitionIndex].allCurrentSegmentsAndGroups;
							
							result += innerToHumanReadable(segments, commandResult.outInfo.origWord) + ' > ' +
										innerToHumanReadable(segments, commandResult.word.innerRepresentation) + ' / ' +
										innerToHumanReadable(segments, commandResult.outInfo.wordMatches)+'';
										//TODO: handle capital letters here
							}
						else
							result += "Syntax error\n"; // TODO: what kind of syntax error?
						break;
					case command_t.DEFINE_GROUP:
						
						if (commandResult.success)
							{
							let DG_command:SCAcommandGroupDefinition_t = <SCAcommandGroupDefinition_t>parsedCommands[j];
							result += DG_command.name + ' = {' + sarr(DG_command.segments).join(' ') + '}';
							//TODO: also display probabilities, if any
							}
						else
							result += "Syntax error: most likely more than one = or /\n";
						break;
					default:
						break;
					}
				}
			
			if (commandResult.success === false) break;
			if (offset <= 0) if (j < commandsLength - 1) result += '\n';
			}

		if (commandResult.principalFormIndex > -1)
			parsedWord.innerRepresentation = parsedWord.getPrincipalForm(commandResult.principalFormIndex)!.innerRepresentation;
			//TODO: what to do if it is undefined?

		let lastGroupDefinitionIndex = -1;
		let segments:segmentation_t;
		if (commandsLength > 0)
			lastGroupDefinitionIndex = parsedCommands[commandsLength - 1].lastGroupDefinitionCommandIndex;
		if (lastGroupDefinitionIndex == -1)
			segments = new segmentation_t();
		else
			segments = parsedCommands[lastGroupDefinitionIndex].allCurrentSegmentsAndGroups;

		parsedWord.setSurfaceRepresentationFromInnerRepresentation(segments);
		parsedWord.resetInnerRepresentationToSurfaceRepresentation();//TODO: reset this once phonology is unified
		//maybe the phonology should be kept as a seperate object than the sound changes

		if (commandResult.outInfo)
			commandResult.outInfo.wordMatches = innerToHumanReadable(segments, commandResult.outInfo.wordMatches);

		return {word:parsedWord.getResultingString(), comment:result, outInfo: commandResult.outInfo};
		}

	private static executeACommand(
				commandResult:executeCommandResult_t,
				commands:SCAcommand_t[],
				commandNumber:number,
				accumulateStats:boolean)
		{
		lo(g.SCA,'\n\nstarting command '+commandNumber+':\n' + commands[commandNumber].line);

		commandResult.outInfo = {origWord:'',wordMatches:''};
		let lastGroupDefinitionIndex = -1; //declaring some stuff here for syntax purposes
		let segments:segmentation_t;
		let indexFrom = undefined;
		let indexTo = undefined;
		let principalFormIndex = undefined;
		switch(commands[commandNumber].command)
			{
			case command_t.SOUND_CHANGE:
				lastGroupDefinitionIndex = commands[commandNumber].lastGroupDefinitionCommandIndex;
				
				if (lastGroupDefinitionIndex == -1)
					segments = new segmentation_t();
				else
					segments = commands[lastGroupDefinitionIndex].allCurrentSegmentsAndGroups;
				
				let old = commandResult.word.innerAndSurfaceRepresentation();
				
				if (commandResult.principalFormIndex == -1) //TODO: maybe unify this logic from the various cases
					{
					indexFrom = 0;
					indexTo = commandResult.word.principalForms.length + 1;
					}
				else
					{
					indexFrom = commandResult.principalFormIndex;
					indexTo = commandResult.principalFormIndex+1;
					}

				for (principalFormIndex = indexFrom; principalFormIndex < indexTo; principalFormIndex++)
					{
					let principalForm = commandResult.word.getPrincipalForm(principalFormIndex);
					//TODO: what to do if it is undefined?
					let result = SCA.applySCtoaWord(segments, principalForm!.innerRepresentation, commands[commandNumber], accumulateStats);
					principalForm!.innerRepresentation = result.word;
					commandResult.outInfo = result.outInfo;
				
					principalForm!.setSurfaceRepresentationFromInnerRepresentation(segments);
					let neu = principalForm!.innerAndSurfaceRepresentation();
					lo(g.SCA,old+' -> '+neu);
				
					if (!result.success)
						{
						commandResult.success = false;
						INTERFACE.displayError("sound change application wasn't successful: "+result.errorMsg);
						return commandResult;
						}
					}
				break;
			case command_t.PATTERN:
				let pattern_command:SCAcommandPattern_t = <SCAcommandPattern_t>commands[commandNumber];
				
				commandResult.principalFormIndex = pattern_command.principalFormIndex;
				
				if (commandResult.principalFormIndex > -1) //TODO: generalise this logic for multiple pwfs
					{
					if (commandResult.word.getPrincipalForm(commandResult.principalFormIndex) === undefined)
						commandResult.principalFormIndex = pattern_command.secondaryPrincipalFormIndex;
					}
				
				if (commandResult.principalFormIndex > -1) //TODO: generalise this logic for multiple pwfs
					{
					if (commandResult.word.getPrincipalForm(commandResult.principalFormIndex) === undefined)
						commandResult.principalFormIndex = 0; // every word is guaranteed to have at least one root, even if it's ∅...
					}
				
				lastGroupDefinitionIndex = commands[commandNumber].lastGroupDefinitionCommandIndex;
				
				if (lastGroupDefinitionIndex == -1)
					segments = new segmentation_t();
				else
					segments = commands[lastGroupDefinitionIndex].allCurrentSegmentsAndGroups;
				
				let old2 = commandResult.word.innerAndSurfaceRepresentation();
				
				if (commandResult.principalFormIndex == -1) //TODO: maybe unify this logic from the various cases
					{
					indexFrom = 0;
					indexTo = commandResult.word.principalForms.length + 1;
					}
				else
					{
					indexFrom = commandResult.principalFormIndex;
					indexTo = commandResult.principalFormIndex+1;
					}

				for (principalFormIndex = indexFrom; principalFormIndex < indexTo; principalFormIndex++)
					{
					let principalForm = commandResult.word.getPrincipalForm(principalFormIndex);
					
					let ruleApplies = true;
					if (pattern_command.condition.length > 0)
						{
						let cm:any = '';
						//the RE should always be defined (and a RE!) if we got all the way till here
						//don't know what's up with this, but if this line is removed, the condition is not matched correctly
						while (pattern_command.RE.exec(<string><unknown>undefined) !== null){}
						ruleApplies = ((cm=pattern_command.RE.exec(principalForm!.innerRepresentation)) !== null);
						//TODO: test the case with conditions
						}
					if (ruleApplies)
						{
						principalForm!.innerRepresentation =
							' '+pattern_command.SCAprefix+
							principalForm!.innerRepresentation.trim()+
							pattern_command.SCAsuffix+' ';

						principalForm!.setSurfaceRepresentationFromInnerRepresentation(segments);
						let neu = principalForm!.innerAndSurfaceRepresentation();
						lo(g.SCA,old2+' -> '+neu);
						}
					}
				break;
			case command_t.DEFINE_GROUP:
				lastGroupDefinitionIndex = commands[commandNumber].lastGroupDefinitionCommandIndex;
				let group_definition:SCAcommandGroupDefinition_t = <SCAcommandGroupDefinition_t>commands[commandNumber];
				
				if (lastGroupDefinitionIndex == -1)
					segments = new segmentation_t();
				else
					segments = commands[lastGroupDefinitionIndex].allCurrentSegmentsAndGroups;
				
				if (commandResult.principalFormIndex == -1) //TODO: maybe unify this logic from the various cases
					{
					indexFrom = 0;
					indexTo = commandResult.word.principalForms.length + 1;
					}
				else
					{
					indexFrom = commandResult.principalFormIndex;
					indexTo = commandResult.principalFormIndex+1;
					}

				for (principalFormIndex = indexFrom; principalFormIndex < indexTo; principalFormIndex++)
					{
					let principalForm = commandResult.word.getPrincipalForm(principalFormIndex);
					let old2 = principalForm!.innerAndSurfaceRepresentation();
				
					if (group_definition.itIsGroupRedefinition)
						principalForm!.updateInnerRepresentation(group_definition.allCurrentSegmentsAndGroups);
					else
						principalForm!.resetInnerRepresentation(segments, group_definition.allCurrentSegmentsAndGroups);
				
					principalForm!.setSurfaceRepresentationFromInnerRepresentation(group_definition.allCurrentSegmentsAndGroups);
					let neu2 = principalForm!.innerAndSurfaceRepresentation();
					lo(g.SCA,group_definition.name+' = '+group_definition.innerSegments);
					lo(g.SCA,old2+' -> '+neu2);
					}
				
				break;
			default:
				INTERFACE.displayError("Illegal command - command number "+(commandNumber+1)+ // TODO: keep the original line in the object
				"; command type "+commands[commandNumber].command);
				commandResult.success = false;
				break;
			}
		return commandResult;
		}

	private static arbitrateCommand(command:string):command_t
		{
		if (command.includes('=')) return command_t.DEFINE_GROUP;
		if (command.includes('>')) return command_t.SOUND_CHANGE;
		if (command.includes('-')) return command_t.PATTERN;
		if (command.includes('(') && command.includes(')') && command.indexOf('(') < command.indexOf(')')) return command_t.PATTERN;
		return command_t.ILLEGAL;
		}

	static parseAllLines(lineStr:string, lineMap?:number[]):commandArray_t
		{
		//TODO: also execute static analysis when sound changes are changed, with a timeout of about 200
		let commands:commandArray_t = new commandArray_t();
		
		if (lineStr === undefined)
			{
			commands.success = false;
			commands.error = "Warning: no commands due to empty (undefined) argument";
			return commands;
			}
		
		let lines = lineStr.split('\n');
		let linesNum = lines.length;
		let innerRepresentation = new segmentation_t();
		let currentCommand = 0;
		let lastGroupDefinitionCommandIndex = -1;
		let leadingEmptyLines = 0;
		
		for (let currentLine = 0; currentLine < linesNum; currentLine++)
			{
			if (lineMap)
				lineMap[currentLine] = currentCommand - 1;
			//cleanup and ignore comments
			let origLine = lines[currentLine];
			let line = origLine.trim();
			if (!line.length) {leadingEmptyLines++;continue;}
			if (line[0] == '/' && line[1] == '/') {leadingEmptyLines++;continue;}
			line = line.split('//')[0].trim();
			if (!line.length) {leadingEmptyLines++;continue;}
			
			let commandType = PARSER.arbitrateCommand(line);
			
			switch (commandType)
				{
				case command_t.SOUND_CHANGE:

					let expression = line.split('>'); // split A > B / C
					//assign source = A; target = B; condition = C
					let source = expression[0].trim();
					if (expression[1] === undefined) 
						expression[1] ='';
					let targetStr = expression[1].trim();
					let target = targetStr.split('/');
					let condition = '';
		
					//assign direction and pruning (i.e. rtl, ltr, even, odd)
					if (target[1] !== undefined) 
						condition+=target[1];
					let matchDirectionStr = '';
					if (target[2] !== undefined) 
						matchDirectionStr = target[2].trim();
						
					//dolog("matchDirection & pruning are "+matchDirection);

					matchDirectionStr = UTL.noSpaces(matchDirectionStr);
					//dolog("matchDirection & pruning are "+matchDirection);
					let pruningStr = UTL.noSpaces(matchDirectionStr.substring(3));
					//dolog("pruning is "+pruning);
					matchDirectionStr = matchDirectionStr.substring(0, 3);
					let matchDirection: match_direction_t = match_direction_t.ALL;
					if (matchDirectionStr.toLowerCase() == 'rtl')
						matchDirection = match_direction_t.RTL;
					else if (matchDirectionStr.toLowerCase() == 'ltr')
						matchDirection = match_direction_t.LTR;
					let pruning:number;
					if (pruningStr == 'odd')
						pruning = 0; 
					else if (pruningStr == 'even')
						pruning = 1;
					else
						pruning = -1;
					//TODO: rework direction and pruning in the new way
					
					//dolog("pruning is "+pruning);
					//dolog("matchDirection is "+matchDirection);
		
					targetStr = target[0].trim();
					if (source == '' && targetStr == '') 
						{
						commands.success = false;
						commands.error = "Syntax error(no source or target defined) at line "+(currentLine+1)+": '"+origLine+"'";
						return commands;
						}
					//any sound change should have at least generation or source

					/*target = target.split('#');
					outputGroupOrder = '';
					if (target[1] !== undefined)
						outputGroupOrder = noSpaces(target[1]);
					target = target[0];*/
					//parse rearrangement of input groups in the output, e.g. for metathesis:
					//TN > NT #21 / _#
					//TODO: rework rearrangement in the new way:
					//T^1N^2 > N^2T^1 / _#
					//∅ > T^1 / N^1_s
					//alternative notation?:
					//T1N2 > N2T1 / _#
					//∅ > T1 / N1_s
		
					if (targetStr.trim() == '∅') targetStr = ''; //alternative notation for segment loss
					if (targetStr.trim() == '0') targetStr = ''; //alternative notation for segment loss
					if (source.trim() == '∅') source = ''; //alternative notation for segment generation
					if (source.trim() == '0') source = ''; //alternative notation for segment generation
		
					condition = wordToInner(innerRepresentation, condition.trim());
					let sourceWithGroups = wordToInnerLeaveGroups(innerRepresentation, source);
					let targetWithGroups = wordToInnerLeaveGroups(innerRepresentation, targetStr);
		
					source = wordToInner(innerRepresentation, source);//groups are also decomposed to their constituents, e.g. Vn becomes [aouei]n
					//target = wordToInner(innerRepresentation, target);
					if (source == '' && condition == '')
						condition = '_#'; //ensure we aren't adding the new stuff in between every segment

					
					let cmd_sc = new SCAcommandSoundChange_t(
									currentCommand,
									sourceWithGroups,
									targetWithGroups,
									condition,
									matchDirection);
					cmd_sc.lastGroupDefinitionCommandIndex = lastGroupDefinitionCommandIndex;
					cmd_sc.pruning = pruning;
					cmd_sc.line = line;
					//dolog("pruning is "+cmd_sc.object.pruning);
					//dolog("matchDirection is "+cmd_sc.object.matchDirection);
					let res = cmd_sc.prepareForSoundChanges(innerRepresentation);
					
					if (res.error)
						{
						commands.success = false;
						commands.error = "Syntax error("+res.msg+") at line "+(currentLine+1)+": '"+origLine+"'";
						return commands;
						}
						
					//dolog("command "+line+" parsed successfully");
					
					cmd_sc.leadingEmptyLines = leadingEmptyLines;
					
					commands.commands[currentCommand] = cmd_sc;
					currentCommand++;
					leadingEmptyLines = 0;
					break;
				case command_t.DEFINE_GROUP:

					let args = line.split('=');
			
					//TODO: allow for optional curly brackets - redundant TODO?
			
					if (args.length != 2)
						{
						commands.success = false;
						commands.error = "Syntax error(one and only one '=' is required) at line "+(currentLine+1)+": '"+origLine+"'";
						return commands;
						}
		
					let groupName = UTL.noSpaces(args[0]);
			
					if (groupName.length == 0)
						{
						commands.success = false;
						commands.error = "Syntax error(group name cannot be left out) at line "+(currentLine+1)+": '"+origLine+"'";
						return commands;
						}
					let argStr = args[1];//noSpaces(args[1]);
		
					let ratios = '';
					if (argStr.includes('/'))
						{
						args = argStr.split('/');
				
						if (args.length != 2)
							{
							commands.success = false;
							commands.error = "Syntax error(no more than one '/' should be present) at line "+(currentLine+1)+": '"+origLine+"'";
							return commands;
							}
				
						ratios = UTL.noSpaces(args[1]);
						argStr = UTL.noSpaces(args[0]);
						}
					else argStr = UTL.noSpaces(argStr);
		
					let segs = argStr;
					let segsArr = segs.split(' ');
					let ratiosArr = ratios.split(' ');
					
					let groupOrSegmentRedifition = declareSegments(innerRepresentation,segsArr,groupName,ratiosArr);
					
					let l = segs.length;
					let rjson:any = {};
					for(let i = 0; i < l; i++) rjson[segs[i]] = ratios[i] || 1;
					
					let cmd_dg = new SCAcommandGroupDefinition_t(
							currentCommand,
							groupName,
							segsArr,
							rjson,
							groupOrSegmentRedifition);
					cmd_dg.setAllCurrentSegmentsAndGroups(innerRepresentation);
					cmd_dg.lastGroupDefinitionCommandIndex = lastGroupDefinitionCommandIndex;
					cmd_dg.leadingEmptyLines = leadingEmptyLines;
					cmd_dg.line = line;
					commands.commands[currentCommand] = cmd_dg;
					lastGroupDefinitionCommandIndex = currentCommand;
					
					//dolog("command "+line+" parsed successfully");

					currentCommand++;
					leadingEmptyLines = 0;
					break;
				case command_t.PATTERN:
					//four cases after splitting for -:
					//prefix () suffix
					//prefix ()
					//() suffix
					//()
					let args2 = [];
					let condition2 = '';
					
					if (line.includes('/'))
						{
						let lineArr = line.split('/');
						condition2 = UTL.noSpaces(lineArr[1]);
						line = lineArr[0];
						}

					if (!line.includes('-')) args2[0] = line;
					else args2 = line.split('-');
					if (args2.length > 3)
						{
						commands.success = false;
						commands.error = "Syntax error(at most two '-' are required) at line "+(currentLine+1)+": '"+origLine+"'";
						return commands;
						}
					let prefix = '';
					let suffix = '';
					let principalFormIndex = -1;
					let secondaryPrincipalFormIndex = -1;
					
					let PFIstring = '';
					
					switch (args2.length)
						{
						case 1:
							PFIstring = args2[0];
							break;
						case 2:
							if (args2[1].includes('(')) PFIstring = args2[1];
							else suffix = UTL.noSpaces(args2[1]);
							if (args2[0].includes('(')) PFIstring = args2[0];
							else prefix = UTL.noSpaces(args2[0]);
							break;
						case 3:
							if (args2[1].includes('(')) PFIstring = args2[1];
							prefix = UTL.noSpaces(args2[0]);
							suffix = UTL.noSpaces(args2[2]);
							break;
						}
					
					if (PFIstring !== '')
						{
						//up to 10 principal forms for now
						//is there any natlang with this many anyway?
						PFIstring = UTL.removeAllSpaces(PFIstring);
						if (PFIstring.includes(';'))
							{
							let PFIstringArr = PFIstring.split(';');
							let n2 = Number(PFIstringArr[1][0]);
							if (isNaN(n2))
								{
								commands.success = false;
								commands.error = "Syntax error(syntax for principal form indicies is e.g. (1;2) ) at line "+
											(currentLine+1)+": '"+origLine+"'";
								return commands;
								}
							secondaryPrincipalFormIndex = n2;
							PFIstring = PFIstringArr[0];
							}
						let num = Number(PFIstring[1]);
						if (isNaN(num))
							{
							commands.success = false;
							commands.error = "Syntax error(the number of the principal form has to be between the brackets) at line "+
										(currentLine+1)+": '"+origLine+"'";
							return commands;
							}
						principalFormIndex = num;
						lo(g.SCA,'set to '+principalFormIndex);
						}

					
					let cmd_ptn = new SCAcommandPattern_t(currentCommand, prefix, suffix, principalFormIndex, condition2, secondaryPrincipalFormIndex);
					//(prefix, suffix, principalFormIndex, condition)
					cmd_ptn.lastGroupDefinitionCommandIndex = lastGroupDefinitionCommandIndex;
					cmd_ptn.line = line;
					let res2 = cmd_ptn.prepareForSoundChanges(innerRepresentation);
					
					if (res2)
						{
						commands.success = false;
						commands.error = "Syntax error at line "+(currentLine+1)+": '"+origLine+"'";
						return commands;
						}
					
					cmd_ptn.leadingEmptyLines = leadingEmptyLines;
					
					commands.commands[currentCommand] = cmd_ptn;
					currentCommand++;
					leadingEmptyLines = 0;
					
					break;
				case command_t.ILLEGAL:
				default:
					commands.success = false;
					commands.error = "Syntax error at line "+(currentLine+1)+": '"+origLine+"'";
					return commands;
				}
			if (lineMap)
				lineMap[currentLine] = currentCommand - 1;
			}
		
		return commands;
		}
	}