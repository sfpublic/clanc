/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class SESSION
	{
	static sync_page_to_conlang: ()=>res_t|Promise<res_t>;
	static conlangs: conlang_handle_t[] = [];
	static tempinc: number = 0;
	static activeconlang:number = -1;
	static loadedfilename:string='';
	static file:FILE|null = null;
	
	/*
	* Session object - represents the currently open instance of the program.
	* - A singleton, quasi-static object
	* - Contains the global settings.
	* - Contains all conlangs.
	* - It's a static class only.
	*/
	constructor()
		{
		return lo(g.error, "This object is only to be used statically.", null);
		}
	
	static init():res_t
		{
		SESSION.sync_page_to_conlang = ()=>{return UTL.res_success;};
		INTERFACE.init();
		return UTL.res_success;
		}

	//asynchronous, returns a promise
	static async createfile()
		{
		//TODO: handle any currently opened file with UI confirmation prompt

		SESSION.closecurrentfile();
		let file = new FILE();
		let initres = await file.init().catch(LOG.caughtError);
		if (initres.error !== false)
			return PrejectT<null>(null);
		SESSION.file = file;
		SESSION.activeconlang = -1;
		SESSION.conlangs = [];
		return PresolveT<FILE>(SESSION.file);
		}
	
	static openfile()
		{
		//TODO: handle any currently opened file
		}

	static closecurrentfile()
		{
		if (SESSION.file !== null)
			{
			//TODO: warning on file close
			//TODO: more stringent checks
			SESSION.file.deinit().catch(LOG.caughtError);
			//TODO: error on deinit means a table couldn't be dropped - what should be done about this?
			SESSION.file = null;
			}
		}
	
	static async deriveConlang():Promise<any>
		{
		if (SESSION.activeconlang < 0) return Preject("error deriving conlang: no selected conlang");

		let protoname = (await SESSION.file!.db.getdata(
			table_e.conlang,
			['name'],
			'id = '+SESSION.getactiveconlang()!.dbid))[0].name;

		let name = "Post-"+protoname;
		let res = await SESSION.file!.deriveConlang(name, SESSION.getactiveconlang()!.dbid).catch(LOG.caughtError);
		let clg:conlang_handle_t = res.payload;

		if (res.error)
			return Preject("error creating conlang - db transaction failed");
		if (!('dbid' in clg && typeof clg.dbid === 'number'))
			return Preject("error creating conlang - db transaction failed");
		if (clg.dbid < 0)
			return Preject("error creating conlang - db transaction returned invalid id");

		SESSION.conlangs.push(clg);
		SESSION.selectconlang(clg);

		return PresolveT<conlang_handle_t>(clg);
		}
	
	static async createConlang():Promise<any>
		{
		//TODO: first check if there's an opened file

		let name = "Conlang "+SESSION.tempinc;
		SESSION.tempinc++;
		if (SESSION.file === null) return Preject("error creating conlang: no session file");
		let res = await SESSION.file.createConlang(name).catch(LOG.caughtError);
		let clg:conlang_handle_t = res.payload;

		if (!('dbid' in clg && typeof clg.dbid === 'number'))
			return Preject("error creating conlang - db transaction failed");
		if (clg.dbid < 0)
			return Preject("error creating conlang - db transaction returned invalid id");

		SESSION.conlangs.push(clg);
		if (SESSION.activeconlang < 0)
			{
			SESSION.activeconlang = 0;
			}
		return PresolveT<conlang_handle_t>(clg);
		}
	
	//asynchronous, returns a promise
	static deleteconlang(clg:conlang_handle_t|null)
		{
		if (clg == null) return Preject("conlang handle of conlang to delete is null");
		if (!Number.isInteger(clg.dbid)) return Preject("conlang index not an integer: "+clg.dbid);
		if (SESSION.file == null) return Preject("session file is null");
		if (!('dbid' in clg && typeof clg.dbid === 'number'))
			return Preject("error creating conlang - db transaction failed");
		if (clg.dbid < 0)
			return Preject("error creating conlang - db transaction returned invalid id");
		//TODO: conlang validity checker utility function?
		let clgidx = SESSION.getconlangindex(clg);
		if (clgidx < 0 || clgidx > SESSION.conlangs.length - 1) return Preject("conlang index out of bounds: "+clgidx);
		
		//let todelete = SESSION.conlangs[clgidx];
		
		//return todelete.deinit().then(()=>
		//TODO: replace or remove
			//{
			try
				{
				let dbid:number = SESSION.conlangs[clgidx].dbid;

				return SESSION.file.db.deleterowsbyid(table_e.conlang, [dbid]).then(()=>
					{
					SESSION.conlangs.splice(clgidx, 1);
					if (SESSION.getactiveconlang() == null)
						{
						if (SESSION.conlangs.length == 0)
							SESSION.activeconlang = -1;
						else
							SESSION.activeconlang = 0;
						}
					});
				}
			catch (error)
				{
				return Preject(error);
				}
			//});
		}

	static getactiveconlang(): conlang_handle_t | null
		{
		if (SESSION.activeconlang < 0) return null;
		return SESSION.conlangs[SESSION.activeconlang];
		}
	
	static getactiveconlangindex(): number
		{
		return SESSION.activeconlang;
		}

	static getconlangindex(clg:conlang_handle_t):number
		{
		//TODO: does any external object need to know the id?
		//maybe remove this function and make the array private
		//same for getactiveconlangindex
		return SESSION.conlangs.indexOf(clg);
		}
		
	static selectconlang(clg:conlang_handle_t): res_t
		{
		let id = SESSION.getconlangindex(clg);
		if (!UTL.verifyID(id, SESSION.conlangs.length)) return UTL.res_error;
		SESSION.activeconlang = id;
		return UTL.res_success;
		}
		
	static selectconlangbyid(id:number): res_t
		{
		if (!UTL.verifyID(id, SESSION.conlangs.length)) return UTL.res_error;
		SESSION.activeconlang = id;
		return UTL.res_success;
		}
	}