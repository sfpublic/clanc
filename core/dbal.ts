/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//The DBAL (database abstraction layer) serves a the following purposes:
//	- provides an easy way to change the DB in the future

type DBCelldata = string|number|object|null|boolean;
type DBRowdata = DBCelldata[];
type DBdata = {[index:string]:DBCelldata};

interface DBAL
	{
	databasename: string;
	file: FILE | null;
	lastquery: string;

	init(): Promise<any>;
	//Asynchronous. Initialises a new, empty DB with conforming structure
	//returns a promise

	getdata(table:table_e, cols?:string[], where?:string): Promise<any>;
	//Synchronous. Fetches few values from a table
	//	table - the table to fetch from
	//	where - json for the where clause; leave out or "*" for everything - use with care!
	//	cols - which cols to fetch; leave out for everything
	//	TODO: fetching with pagination
	//	TODO: maybe shift from SQL to jsons eventually
	//returns promise for array of jsons, or NULL on failure
	
	savedata(table:table_e, data:DBdata): Promise<any>;
	//Saves data to a table
	//	data - an array, with each element being a row, containing a JSON of its cells
	
	updatedata(table:table_e, data:DBdata, where:string): Promise<any>;
	//Saves data to a table
	//	data - an array, with each element being a row, containing a JSON of its cells
	//	where - json for the where clause; leave out for everything new entry
	
	// deleterows(table:string, where:string): Promise<boolean>;
	//Deletes rows
	//	where - json for the where clause
	
	deleterowsbyid(table:table_e, ids:number[]): Promise<boolean>;
	//Deletes rows
	//	where - json for the where clause

	savemultiplerows(table:table_e, cols:string[], data:DBRowdata[]): Promise<any>;
	//Saves multiple rows of data.

	updatemultiplerows(table:table_e, cols:string[], data:DBRowdata[]): Promise<any>;
	//Saves multiple rows of data.

	deinit(): Promise<any>;
	//Asynchronous closes the currently open DB, if any, and releases any other held resources, if necessary
	//returns a promise
	
	cleartable(table:table_e): Promise<any>;
	//Asynchronous. Clears all the data from a table, but keeps the table.
	//returns a promise
	
	droptable(table:table_e): Promise<any>;
	//Asynchronous. Drops the table
	//returns a promise
	
	getid(table:table_e, where:string): Promise<number>;
	//Returns the ID of the first row, which is applicable to the where clause
	//returns a single id, or -1 on failure
	
	getids(table:table_e, where:string): Promise<number[]>;
	//Returns the ID of all rows, which are applicable to the where clause
	//returns a list of ids, or [-1] on failure. It is possible that the list is empty (no such rows exist)

	//many to many add a single record
	MTMaddOne(table:mtmt_t, first_col:table_e, fromid:number, toid:number): Promise<any>;

	MTMaddOTM(table:mtmt_t, first_col:table_e, fromid:number, toid:number[]): Promise<any>;
	//many to many add a one to many record

	MTMaddlist(table:mtmt_t, first_col:table_e, fromid:number[], toid:number[]): Promise<any>;
	//many to many add records as two lists of id pairs

	MTMsavedata(table:mtmt_t, data:DBdata): Promise<any>;

	MTMupdatedata(table:mtmt_t, data:DBdata, where:string): Promise<any>;

	MTMgetids(table:mtmt_t, col:table_e, fromid:number): Promise<number[]>;

	MTMgetrows(table:mtmt_t, col:table_e, fromid:number): Promise<any[]>;

	MTMgetwhere(table:mtmt_t, where:string): Promise<any[]>;

	MTMgetalldata(table:mtmt_t): Promise<any[]>;

	MTMdeleterows(table:mtmt_t, where:string): Promise<number[]>;

	MTMdroptable(table:mtmt_t): Promise<any>;

	MTMgetIdsFromIdCombos(table:mtmt_t, srcCol:table_e, ids:number[][], where?:string): Promise<number[]>;
	}











