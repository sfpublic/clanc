/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class slotTableCell_t
	{
	onEditCallback:  ()=>any = ()=>{};
	preview:         string  = '';
	value:           string  = '';
	id:              number  = -1;
	position:        number  = -1;
	}

class dummySlotTable_t
	{
	table:           slotTableCell_t[][] = [];
	}

class slotTable_t
	{
	table:           slotTableCell_t[][] = [];
	getLength:       ()=>number = ()=>{return this.table.length;};
	getItem:         (id:number)=>any = (id:number)=>{return this.table[id].map((y:any)=>y.preview);};
	notify:          ()=>any = ()=>{};

	constructor(slotId:number)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;

		if (indexedData == null)
			{
			INTERFACE.displayError("Fatal error - indexed word type data is null");
			return this;
			}

		let slot = indexedData.indexedSlots[slotId];

		// Step 1 - determine which categories are present for the slot, out of all the current conlang categories
		let categoryIds = [...slot.categoryIds];
		categoryIds = categoryIds.filter(x => !slot.missing_categoryIds.includes(x));

		let categoryPos:number[] = [];
		for (let i in slot.category_positions)
			categoryPos[slot.category_positions[i].id] = slot.category_positions[i].position;

		categoryIds.sort((a:number, b:number)=>{return categoryPos[a] - categoryPos[b]});

		switch (categoryIds.length)
			{
			case 0:
				break;
			case 1:
				slotTable_t.construct1DSlotTable(this, slot, categoryIds[0]);
				break;
			case 2:
				slotTable_t.construct2DSlotTable(this, slot, categoryIds, IF_MORPHOLOGY.paradigmDataBackup.indexedMorphemes);
				break;
			default:
				slotTable_t.constructMultidimSlotTable(this, slot, categoryIds);
				break;
			}
		}

	private static construct1DSlotTable(
			newTable:slotTable_t,
			slot:slot_t,
			categoryId:number
			)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;

		for (let grm in indexedData.indexedCategories[categoryId].grammemeIds)
			{
			let grmId = indexedData.indexedCategories[categoryId].grammemeIds[grm];
			let grammeme = indexedData.indexedGrammemes[grmId];
			let header:slotTableCell_t = new slotTableCell_t();
			let val:slotTableCell_t = new slotTableCell_t;

			header.preview = grammeme.gloss;
			
			// TODO: optimise with DB calls using table joins
			for (let m in indexedData.indexedMorphemes)
				{
				let im = indexedData.indexedMorphemes[m];
				if (im.slot == slot.id && im.grammemeIds.includes(grmId))
					{
					val.value = im.SCs;
					val.preview = MORPHOLOGY.morphemeSCsToPreview(val.value);
					val.id = im.id;
					val.position = grammeme.position;
					break;
					}
				}
			
			newTable.table.push([header, val]);
			}

		newTable.table.sort((a:slotTableCell_t[], b:slotTableCell_t[])=>{return a[1].position - b[1].position});
		}

	private static construct2DSlotTable(
			newTable:slotTable_t | dummySlotTable_t,
			slot:slot_t,
			categoryIds:number[],
			indexedMorphemes:morpheme_t[]
			)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;

		let colGrmArr:number[];
		let rowGrmArr:number[];

		colGrmArr = [...indexedData.indexedCategories[categoryIds[1]].grammemeIds];
		rowGrmArr = [...indexedData.indexedCategories[categoryIds[0]].grammemeIds];

		colGrmArr.sort((a:number, b:number) =>
			{
			return indexedData.indexedGrammemes[a].position - 
			       indexedData.indexedGrammemes[b].position;
			});

		rowGrmArr.sort((a:number, b:number) =>
			{
			return indexedData.indexedGrammemes[a].position - 
			       indexedData.indexedGrammemes[b].position;
			});

		let headerRow:slotTableCell_t[] = [];
		headerRow.push(new slotTableCell_t);
		for (let grm in colGrmArr)
			{
			let grmId = colGrmArr[grm];
			let grammeme = indexedData.indexedGrammemes[grmId];
			let header:slotTableCell_t = new slotTableCell_t();

			header.preview = grammeme.gloss;
			headerRow.push(header);
			}
		newTable.table.push(headerRow);

		for (let grm in rowGrmArr)
			{
			let grmId = rowGrmArr[grm];
			let rowGrammeme = indexedData.indexedGrammemes[grmId];
			let header:slotTableCell_t = new slotTableCell_t();

			let row:slotTableCell_t[] = [];
			header.preview = rowGrammeme.gloss;
			row.push(header);
			
			for (let grm in colGrmArr)
				{
				let grmId = colGrmArr[grm];
				let cell:slotTableCell_t = new slotTableCell_t();
			
				// TODO: optimise with DB calls using table joins
				for (let m in indexedMorphemes)
					{
					let im = indexedMorphemes[m];
					if (im.slot == slot.id && im.grammemeIds.includes(grmId) && im.grammemeIds.includes(rowGrammeme.id))
						{
						cell.value = im.SCs;
						cell.preview = MORPHOLOGY.morphemeSCsToPreview(cell.value);
						cell.id = im.id;
						break;
						}
					}

				row.push(cell);
				}

			newTable.table.push(row);
			}
		}

	private static constructMultidimSlotTable(
			newTable:slotTable_t,
			slot:slot_t,
			categoryIds:number[]
			)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;

		let firstTwoCategoryIds:number[] = 
			[
			categoryIds[0],
			categoryIds[1]
			];

		categoryIds.splice(0, 2);

		let cartProdInput:number[][] = [];

		for (let i in categoryIds)
			{
			let grmIds = [...indexedData.indexedCategories[categoryIds[i]].grammemeIds];
			grmIds.sort((a:number, b:number) =>
				{
				return indexedData.indexedGrammemes[a].position - 
				       indexedData.indexedGrammemes[b].position;
				});
			cartProdInput.push(grmIds);
			}

		let cartProd = UTL.cartProd(cartProdInput);
		let ims = indexedData.indexedMorphemes;

		for (let i in cartProd)
			{
			let subtable = cartProd[i];
			let subIndexedMorphemes:morpheme_t[] = [];
			// TODO: optimise with DB calls using table joins
			for (let m in ims)
				{
				let im = ims[m];
				if (im.slot != slot.id) continue;
				let present = true;
				for (let g in subtable)
					{
					present = present && im.grammemeIds.includes(subtable[g]);
					if (!present) break;
					}
				if (!present) continue;
				subIndexedMorphemes.push(im);
				}

			let dummyTable = new dummySlotTable_t;
			slotTable_t.construct2DSlotTable(
			            dummyTable,
						slot,
						firstTwoCategoryIds,
						subIndexedMorphemes);
			let temp = dummyTable.table;

			let gloss = '';
			for (let q in subtable)
				{
				let st = subtable[q];
				gloss += indexedData.indexedGrammemes[st].gloss+'.';
				}
			temp[0][0].preview = gloss.substr(0, gloss.length - 1);

			newTable.table = newTable.table.concat(temp).concat([[]]);
			}
		}
	}