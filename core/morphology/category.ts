/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class category_t
	{
	id:               number = -1;
	conlang:          number = -1;
	nullable:         boolean = false;
	name:             string = '';
	grammemeIds:      number[] = [];

	constructor(row:any)
		{
		this.id = row.id;
		this.conlang = row.conlang;
		this.nullable = row.nullable;
		this.name = row.name;
		}
	}