/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

interface previewTableIndex_t
	{
	category:number;
	slot:number;
	}

interface slotGram_t
	{
	grammeme:number;
	slot:number;
	}

class dummyPreviewTable_t
	{
	table:           string[][] = [];
	}

class previewTable_t
	{
	table:           string[][] = [];
	getLength:       ()=>number = ()=>{return this.table.length;};
	getItem:         (id:number)=>any = (id:number)=>{return this.table[id];};
	notify:          ()=>any = ()=>{};
	segments:        string;

	constructor(segments:string)
		{
		this.segments = segments;
		}

	async init(slotIds:number[], wordToInflect?:string)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;

		if (indexedData == null)
			{
			INTERFACE.displayError("Fatal error - indexed word type data is null");
			return this;
			}

		if (slotIds.length < 0)
			{
			INTERFACE.displayError("Fatal error - no slot ids provided for table construction");
			return this;
			}

		let slotData:string[][] = [];
		let indices:previewTableIndex_t[] = [];
		let numCategories = 0;
		
		for (let s in slotIds)
			{
			let slot = indexedData.indexedSlots[slotIds[s]];
			let slotCategoryIds = [...slot.categoryIds];
			slotCategoryIds = slotCategoryIds.filter(x => !slot.missing_categoryIds.includes(x));

			let categoryPos:number[] = [];
			for (let i in slot.category_positions)
				categoryPos[slot.category_positions[i].id] = slot.category_positions[i].position;

			slotCategoryIds.sort((a:number, b:number)=>{return categoryPos[a] - categoryPos[b]});

			if (slotCategoryIds.length == 0)
				{
				// there isn't any morphology for the slot yet, disregard it
				continue;
				}
			numCategories += slotCategoryIds.length;
			for (let j in slotCategoryIds)
				{
				indices.push({slot:slotIds[s], category:slotCategoryIds[j]});
				}
			let res:number[][] = previewTable_t.slotToCartGrammemes(slotCategoryIds);
			let morphemeIds:number[] = await SESSION.file!.db.MTMgetIdsFromIdCombos
							(DBMTMs['m_grammemes'],
							table_e.grammeme,
							res,
							'slot = '+slot.id);

			if (res.length != morphemeIds.length)
				{
				lo(g.error, 'Not all grammeme combinations have a corresponding morpheme: ' +
					res.length +" vs. "+ morphemeIds.length);
				return ;
				}

			let singleSlotData:string[] = [];
			let mdl = morphemeIds.length;
			for (let i = 0; i < mdl; i++)
				{
				singleSlotData.push(indexedData.indexedMorphemes[morphemeIds[i]].SCs);
				}
			slotData.push(singleSlotData);
			}

		let wordforms:string[]
		
		if (wordToInflect)
			// TODO: Optimise. Many sound changes will get executed multiple times.
			// It's probably some kind of exponential complexity.
			wordforms = slotData.reduce(
				(accum:any, curVal:any) =>
					accum.flatMap( (accEl:any) => curVal.flatMap(  (cvEl:any) => 
							{return accEl+'\n'+cvEl;} ))
							,[[]]);
		else
			wordforms = slotData.reduce(
				(accum:any, curVal:any) =>
					accum.flatMap( (accEl:any) => curVal.flatMap(  (cvEl:any) => 
							{return accEl+MORPHOLOGY.morphemeSCsToPreview(cvEl);} ))
							,[[]]);

		switch (numCategories)
			{
			case 0:
				break;
			case 1:
				previewTable_t.construct1DPreviewTable(this, wordforms, indices[0], this.segments, wordToInflect);
				break;
			case 2:
				previewTable_t.construct2DPreviewTable(this, wordforms, indices, 0, 1, this.segments, wordToInflect);
				break;
			default:
				previewTable_t.constructMultidimPreviewTable(this, wordforms, indices, this.segments, wordToInflect);
				break;
			}
		}

	private static slotToCartGrammemes(categoryIds:number[]):number[][]
		{
		let cartProdInput:number[][] = [];
		let ics = IF_MORPHOLOGY.paradigmDataBackup.indexedCategories;
		let igs = IF_MORPHOLOGY.paradigmDataBackup.indexedGrammemes;

		for (let i in categoryIds)
			{
			let grmIds = [...ics[categoryIds[i]].grammemeIds];
			grmIds.sort((a:number, b:number) => {return igs[a].position - igs[b].position;});
			cartProdInput.push(grmIds);
			}

		return UTL.cartProdLeft(cartProdInput);
		}

	private static construct1DPreviewTable(
			newTable: previewTable_t,
			wordforms:string[],
			index:previewTableIndex_t,
			segments:string,
			wordToInflect?:string)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;
		//TODO: stop using global vars and pass it around instead
		let grms = indexedData.indexedCategories[index.category].grammemeIds;
		let l = grms.length;
		grms.sort((a:number, b:number) =>
			{
			return indexedData.indexedGrammemes[a].position - 
				   indexedData.indexedGrammemes[b].position;
			});

		for (let i = 0; i < l; i++)
			{
			if (wordToInflect)
				newTable.table.push(
					[
					indexedData.indexedGrammemes[grms[i]].gloss,
					PARSER.executeCommandsOnASingleWord(
						wordToInflect,
						segments + '\nⱲ = ⱳ\n' + wordforms[i]).word
					]);
					//TODO: figure out why the last segment group is not reversed to surface representation in this case
					//maybe it is if an affix contains it
			else
				newTable.table.push(
					[
					indexedData.indexedGrammemes[grms[i]].gloss,
					wordforms[i]
					]);
			}
		}

	private static construct2DPreviewTable(
			newTable: previewTable_t | dummyPreviewTable_t,
			wordforms:string[],
			indices:previewTableIndex_t[],
			offset:number = 0,
			step:number = 1,
			segments:string,
			wordToInflect?:string)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;
		//TODO: stop using global vars and pass it around instead
		let indexedCategories = indexedData.indexedCategories;

		let colGrmArr:number[];
		let rowGrmArr:number[];

		colGrmArr = [...indexedCategories[indices[1].category].grammemeIds];
		rowGrmArr = [...indexedCategories[indices[0].category].grammemeIds];

		colGrmArr.sort((a:number, b:number) =>
			{
			return indexedData.indexedGrammemes[a].position - 
			       indexedData.indexedGrammemes[b].position;
			});

		rowGrmArr.sort((a:number, b:number) =>
			{
			return indexedData.indexedGrammemes[a].position - 
			       indexedData.indexedGrammemes[b].position;
			});

		let numMorphCols = colGrmArr.length;
		let row = [''];
		for (let i = 0; i < numMorphCols; i++)
			{
			let id = colGrmArr[i];
			row.push(indexedData.indexedGrammemes[id].gloss);
			}
		newTable.table.push(row);

		let numMorphRows = rowGrmArr.length;
		
		for (let i = 0; i < numMorphRows; i++)
			{
			let id = rowGrmArr[i];
			let row = [indexedData.indexedGrammemes[id].gloss];
			for (let j = 0; j < numMorphCols; j++)
				{
				let index = offset + step * (i * numMorphCols + j);
				if (wordToInflect)
					row.push(PARSER.executeCommandsOnASingleWord(
							wordToInflect,
							segments + '\nⱲ = ⱳ\n' + wordforms[index]).word);
					//TODO: figure out why the last segment group is not reversed to surface representation in this case
					//maybe it is if an affix contains it
				else
					row.push(wordforms[index]);
				
				}
			newTable.table.push(row);
			}
		}

	private static constructMultidimPreviewTable(
			newTable: previewTable_t,
			wordforms:string[],
			indices:previewTableIndex_t[],
			segments:string,
			wordToInflect?:string)
		{
		let indexedData:indexedMorphologyData_t = IF_MORPHOLOGY.paradigmDataBackup;
		//TODO: stop using global vars and pass it around instead
		let indexedCategories = indexedData.indexedCategories;
		let offset = 0;
		let step = 1;

		let firstTwoIndices:previewTableIndex_t[] = 
			[
			indices[0],
			indices[1]
			];

		indices.splice(0, 2);

		let cartProdInput:slotGram_t[][] = [];

		for (let i in indices)
			{
			let cartProdRow:slotGram_t[] = [];
			let grmArr:number[];
			grmArr = [...indexedCategories[indices[i].category].grammemeIds];
			grmArr.sort((a:number, b:number) =>
				{
				return indexedData.indexedGrammemes[a].position - 
				       indexedData.indexedGrammemes[b].position;
				});
			step *= grmArr.length;
			for (let j in grmArr)
				cartProdRow.push
					({
					slot:indices[i].slot,
					grammeme:grmArr[j]
					});
			cartProdInput.push(cartProdRow);
			}

		let grmSlotCombos = UTL.cartProdLeft(cartProdInput);

		for (let i in grmSlotCombos)
			{
			let combo = grmSlotCombos[i];
			let title = '';
			let prevSlot = -1;

			for (let j in combo)
				{
				if (combo[j].slot == prevSlot)
					title += '.';
				else if (prevSlot >= 0)
					title += '-';
				title += indexedData.indexedGrammemes[combo[j].grammeme].gloss;
				prevSlot = combo[j].slot;
				}

			let dummyTable = new dummyPreviewTable_t();
			previewTable_t.construct2DPreviewTable(
							dummyTable,
							wordforms,
							firstTwoIndices,
							offset,
							step,
							segments,
							wordToInflect);
			dummyTable.table[0][0] = title;
			newTable.table = newTable.table.concat(dummyTable.table).concat([[]]);

			offset++;
			}
		}

	}