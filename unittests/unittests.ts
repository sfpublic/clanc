/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TEST
	{
	db:DBAL;
	testFN:(db:DBAL)=>Promise<boolean>;

	constructor(ts:DBAL, fn:(db:DBAL)=>Promise<boolean>)
		{
		this.db = ts;
		this.testFN = fn;
		}
	
	async run()
		{
		await this.db.init().catch(LOG.caughtError);
		return await this.testFN(this.db);
		}

	async cleanup()
		{
		return await this.db.deinit();
		}
	}


class UT
	{
	static tests:TEST[] = [];
	static DBfactory:()=>DBAL = ()=>{return new ALA_SQL_DB();};
	static backlog:string[];
	static query:any = null;
	static logtestresults(msg:string)
		{
		lo(g.info, '\n'+msg+'\n');
		this.backlog.push(msg);
		}
	static async run()
		{
		let tl = this.tests.length;
		this.backlog = [];

		for (let i = 0; i < tl; i++)
			{
			this.logtestresults('Running test '+i+'...');
			let result = await this.tests[i].run();
			if (result)
				this.logtestresults('Test '+i+' completed successfully.');
			else
				this.logtestresults('Test '+i+' failed!');
			}
		
		let elem = UTL.getInputElem('log');
		elem && (elem.value = LOG.backlog.join('\n'));
		elem!.scrollTop = elem!.scrollHeight;
		elem = UTL.getInputElem('test-results');
		elem && (elem.value = this.backlog.join('\n'));
		elem!.scrollTop = elem!.scrollHeight;
		}
	static verdict(expectation:any, res:any):boolean
		{
		UT.query = res;
		if (expectation == undefined || res == expectation || JSON.stringify(res) == JSON.stringify(expectation))
			{
			lo(g.info, 'Result: '+JSON.stringify(res));
			return true;
			}
		lo(g.error, `Test result doesn't match expectations: expected ${JSON.stringify(expectation)}, got ${JSON.stringify(res)}`);
		return false;
		}
	static init()
		{
		let elem = UTL.getInputElem('log');
		elem && (elem.value = '');
		elem = UTL.getInputElem('test-results');
		elem && (elem.value = '');
		LOG.interface_errors = false;
		LOG.short_logs = true;
		LOG.logEnabled  =
			{
			error: true,
			UIerror: true,
			warning: true,
			info: true,
			debug: true,
			trace: true,
			DBquery: true,
			SCA: true
			};
		UT.tests.push(new TEST(this.DBfactory(),async (db:DBAL)=>
			{
			let res = true;
			res = res && this.verdict(1, await 
				db.savedata(table_e.conlang, {"name":'clg 1'}).catch(LOG.caughtError));
			res = res && this.verdict(1, await 
				db.getid(table_e.conlang, "name = 'clg 1'").catch(LOG.caughtError));
			return res;
			} ));
		UT.tests.push(new TEST(this.DBfactory(),async (db:DBAL)=>
			{
			let res = true;
			let clgid;
			res = res && this.verdict(1, await 
				db.savedata(table_e.conlang, {"name":'clg 1'}).catch(LOG.caughtError));
			res = res && this.verdict(1, await 
				db.getid(table_e.conlang, "name = 'clg 1'").catch(LOG.caughtError));

			clgid = UT.query;

			res = res && this.verdict(4, await 
				db.savemultiplerows(table_e.morpheme,
					['conlang', 'SCs', 'gloss', 'name'],
					[
					[clgid, 'q', 'SG', 'singular'],
					[clgid, 'w', 'PL', 'plural'],
					[clgid, 's', 'DEF', 'definite'],
					[clgid, 'c', 'Q', 'question'],
					]).catch(LOG.caughtError));

			res = res && this.verdict(undefined, await 
				db.getids(table_e.morpheme, "conlang = "+clgid).catch(LOG.caughtError));
			
			let morphemeids = UT.query;

			res = res && this.verdict(1, await 
				db.savedata(table_e.category, {"name":'plurality', 'conlang':clgid, 'nullable':false}).catch(LOG.caughtError));

			res = res && this.verdict(1, await 
				db.getid(table_e.category, "name = 'plurality'").catch(LOG.caughtError));

			let categoryid = UT.query;
			let mtmcategories = mtm(table_e.morpheme, table_e.category, 'categories');

			res = res && this.verdict(4, await 
				db.MTMaddOTM(mtmcategories, table_e.category, categoryid, morphemeids).catch(LOG.caughtError));

			res = res && this.verdict(1, await 
				db.deleterowsbyid(table_e.morpheme, [morphemeids[0]]).catch(LOG.caughtError));

			res && morphemeids.splice(0,1);

			res = res && this.verdict(
				((item:number[])=>{let a = [];for(let m in item)a.push({'morpheme':item[m]});return a;})(morphemeids),
				await db.MTMgetids(mtmcategories, table_e.category, categoryid).catch(LOG.caughtError));

			res = res && this.verdict(1, await 
				db.deleterowsbyid(table_e.morpheme, [morphemeids[1]]).catch(LOG.caughtError));

			res && morphemeids.splice(1,1);

			res = res && this.verdict(
				((item:number[])=>{let a = [];for(let m in item)a.push({'morpheme':item[m]});return a;})(morphemeids),
				await db.MTMgetids(mtmcategories, table_e.category, categoryid).catch(LOG.caughtError));

			return res;
			} ));
		}
	}