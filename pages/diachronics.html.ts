/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class IF_DIACHRONICS
{
	static protoTableDivId:string = 'protoTableDiv';
	static soundChangesDivId:string = 'soundChangesDiv';
	static segmentsDivId:string = 'segmentsDiv';
	static derivationsTableDivId:string = 'derivationsTableDiv';
	static protoNameDivId:string = 'protoNameDiv';
	static derivationsNameDivId:string = 'derivationsNameDiv';
	static diachronics: DIACHRONICS;

	static htmlpage:html_page_t = 
		{
		html:
			`
			<label class="hideable">
				<input
				type="checkbox"
				id="previewMode"
				class="hideable"
				onClick="UTL.getElem('highlightMode').disabled = !this.checked;"
				> </input>
				 Show effects of sound changes up to the selected line
				</label>
			<label class="hideable">
				<input
				type="checkbox"
				id="highlightMode"
				class="hideable"
				disabled> </input>
				 Show segments affected by sound changes
				</label>
			<div class="clanc-row-container">
				<div
					class='clanc-body'
					style='flex: 3 1 auto; margin: 5px;'>
					<center>
						<b
						id='${IF_DIACHRONICS.protoNameDivId}'
						style='font-size:20px'
						class='hideable'
						></b>
					</center>
					</br>
						<div
						id='${IF_DIACHRONICS.protoTableDivId}'
						style='width:100%;height:100%;'
						class='hideable'
						></div>
					</div>
				<div
					class='clanc-body'
					style='flex: 2 1 auto; margin: 5px;'>
					<textarea
						id='${IF_DIACHRONICS.segmentsDivId}'
						disabled="true"
						style="width:100%;height:auto;resize:none;overflow:hidden;"
						class='hideable'
						></textarea>
					<textarea
						id='${IF_DIACHRONICS.soundChangesDivId}'
						placeholder="sound changes from proto language to derived language"
						style="width:100%;height:100%;resize:none;"
						class='hideable'
						></textarea>
					</div>
				<div
					class='clanc-body'
					style='flex: 3 1 auto; margin: 5px;'>
					<center>
						<b
						id='${IF_DIACHRONICS.derivationsNameDivId}'
						style='font-size:20px'
						class='hideable'
						></b>
					</center>
					</br>
						<div
						id='${IF_DIACHRONICS.derivationsTableDivId}'
						style='width:100%;height:100%;'
						class='hideable'
						></div>
					</div>
			</div>`,
		refresh: IF_DIACHRONICS.refresh,
		load_page: IF_DIACHRONICS.init
		};

	constructor()
		{
		return lo(g.error, "This object is only to be used statically.", null);
		}

	static init(): res_t
		{
		return UTL.res_success;
		}

	static refresh(): res_t
		{
		let SCtextArea = UTL.getElem(IF_DIACHRONICS.soundChangesDivId);

		IF_DIACHRONICS.diachronics = new DIACHRONICS(
				IF_DIACHRONICS.protoTableDivId,
				IF_DIACHRONICS.derivationsTableDivId,
				IF_DIACHRONICS.protoNameDivId,
				IF_DIACHRONICS.derivationsNameDivId,
				IF_DIACHRONICS.segmentsDivId,
				<SCATextAreaElement>SCtextArea);
		IF_DIACHRONICS.diachronics.init();
		return UTL.res_success;
		}
}