/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

interface HTMLDictionaryElement extends HTMLElement
	{
	dictionary?:DICTIONARY|null;
	divId?:string;
	}

class IF_DICTIONARY
{
	static elementHTMLstring:string = 'entries';

	static htmlpage:html_page_t = 
		{
		html:
			`
			<div class="clanc-row-container" style="max-height: 100%;width: 100%">
				<div class="clanc-body" style="max-height: 100%;width: 70%">
					<div id="${IF_DICTIONARY.elementHTMLstring}" class="lexicon" style="width: 100% !important;"></div>
				</div>
				<div style="max-height: 100%;width: 30%;float: left;">
					<div class="clanc-container">
						<div class="clanc-body">
							<div id="inflectionPreview"></div>
						</div>
					</div>
				</div>
			</div>
			`,
		refresh: IF_DICTIONARY.refresh,
		load_page: IF_DICTIONARY.init
		};

	constructor()
		{
		return lo(g.error, "This object is only to be used statically.", null);
		}

	static async init(): Promise<res_t>
		{
		let element:HTMLDictionaryElement|null = UTL.getElem(IF_DICTIONARY.elementHTMLstring);

		if (element == null)
			{
			return {
					error:true,
					msg: "dictionary html element couldn't be created"
					};
			}
		element.divId = IF_DICTIONARY.elementHTMLstring;
		element.dictionary = new DICTIONARY();
		let init_res = await element.dictionary.init(element);
		if (init_res.error)
			{
			element.dictionary = null;
			element = null;
			UTL.getElem('entriescontainer')!.innerHTML = '';
			return init_res;
			}
		else
			return UTL.res_success;
		}

	static refresh():res_t //TODO: maybe convert to promise <res_t>
		{
		let element:HTMLDictionaryElement|null = UTL.getElem(IF_DICTIONARY.elementHTMLstring);

		if (element == null || element.dictionary == null)
			{
			INTERFACE.displayError("Error during page creation");
			return {
					error:true,
					msg: "dictionary html element or its table aren't be created"
					};
			}
		if (!SESSION.getactiveconlang())
			{
			INTERFACE.displayError("No conlang is selected");
			return {
					error:true,
					msg: "no active conlang"
					};
			}
		element.dictionary.conlang = SESSION.getactiveconlang();
		element.dictionary.resyncToDB().catch(LOG.caughtError); // TODO: actually handle errors
		//and display them in the UI
		return UTL.res_success;
		}
}