/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

interface tabSettings
	{
	name:string;
	selected?:boolean;
	id:number;
	idname:string;
	}

class TABS_CONTAINER
	{
	selectedTab:number = -1;
	elemid:string;
	dataFeeder:()=>Promise<any>;
	tabSettingsFeeder:((item:any)=>tabSettings|null)|null;
	asyncTabSettingsFeeder:((item:any)=>Promise<tabSettings|null>)|null;
	onClickCallback:(htmlelem: HTMLElement, dbid:number)=>any;
	static all_tab_containers:{[key:string]:TABS_CONTAINER} = {};

	constructor(
			elemid:string,
			dataFeeder:()=>Promise<any>,
			tabSettingsFeeder:((item:any)=>tabSettings|null)|null,
			onClickCallback:(htmlelem: HTMLElement, dbid:number)=>any,
			asyncTabSettingsFeeder:((item:any)=>Promise<tabSettings|null>)|null = null)
		{
		this.elemid = elemid;
		this.dataFeeder = dataFeeder;
		this.tabSettingsFeeder = tabSettingsFeeder;
		this.onClickCallback = onClickCallback;
		this.asyncTabSettingsFeeder = asyncTabSettingsFeeder;
		if (!this.asyncTabSettingsFeeder && !this.tabSettingsFeeder)
			INTERFACE.displayError("Internal error: neither sync nor async tab settings feeder specified for "+elemid);
		TABS_CONTAINER.all_tab_containers[elemid] = this;
		if (this.tabSettingsFeeder)
			this.refresh();
		else
			this.asyncRefresh();
		}

	refresh()
		{
		this.dataFeeder().then((data)=>
			{
			let pos:tabSettings[] = [];
			let dl = data.length;
			for (let i = 0; i < dl; i++)
				{
				let ts:any;
				ts = this.tabSettingsFeeder!(data[i]);
				if (!ts) continue;
				if (this.selectedTab == ts.id)
					ts.selected = true;
				pos.push(ts);
				}
			UTL.getElem(this.elemid)!.innerHTML = this.buildTabMenu(pos);
			}).catch(LOG.caughtError);
		}

	async asyncRefresh()
		{
		return this.dataFeeder().then(async (data)=>
			{
			let pos:tabSettings[] = [];
			let dl = data.length;
			for (let i = 0; i < dl; i++)
				{
				let ts:any;
				ts = await this.asyncTabSettingsFeeder!(data[i]);
				if (!ts) continue;
				if (this.selectedTab == ts.id)
					ts.selected = true;
				pos.push(ts);
				}
			UTL.getElem(this.elemid)!.innerHTML = this.buildTabMenu(pos);
			});
		}

	private buttonHtml(settings:tabSettings)
		{
		let selected = (settings.selected && 'selected') || '';
		let id = (settings.id && settings.idname && 'id="'+settings.idname!+settings.id!+'"') || '';
		return `<button onclick="TABS_CONTAINER.onClickReroute('${this.elemid}', this, ${settings.id!});"
			class="btn conlangbtn ${selected} ${this.elemid}_class_sel" ${id}>${settings.name}</button>`;
		}

	addTabButton(settings:tabSettings)
		{
		UTL.getElem(this.elemid)!.innerHTML += this.buttonHtml(settings);
		if (settings.selected)
			setTimeout(TABS_CONTAINER.onClickReroute, 100, this.elemid, UTL.getElem(settings.idname!+settings.id!), settings.id);
		}

	buildTabMenu(settings:tabSettings[])
		{
		let html = '';
		for (let i = 0; i < settings.length; i++)
			html += this.buttonHtml(settings[i]);
		return html;
		}

	static onClickReroute(elemid:string, htmlelem: HTMLElement, dbid:number)
		{
		let tc:TABS_CONTAINER = TABS_CONTAINER.all_tab_containers[elemid];
		if (!tc)
			{
			lo(g.error, 'No tabs container with an elem id of '+elemid+' found');
			return;
			}
		tc.onClick(htmlelem, dbid);
		}

	onClick(htmlelem: HTMLElement, dbid:number)
		{
		this.selectedTab = dbid;
		let elements = document.getElementsByClassName(this.elemid+'_class_sel');
		for (let i = 0; i < elements.length; i++)
			elements[i].classList.remove('selected');
		htmlelem.classList.add('selected');

		this.onClickCallback(htmlelem, dbid);
		}
	}