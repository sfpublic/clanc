/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class IF_CONLANGS
	{
	static posContainer: TABS_CONTAINER;
	static inflectionContainer: TABS_CONTAINER;
	static categoryContainer: TABS_CONTAINER;
	static grammemeContainer: ORDERED_LIST_CONTAINER;
	static mtmTables:string[] = 
		[
		'wt_inherent_categories',
		'wt_missing_inherent_categories'
		];
	static mtmDropdownIds:string[] = 
		[
		'wt_inherent_categories_dropdown_container',
		'wt_missing_inherent_categorie_dropdown_containers'
		];
	static mtmButtonIds:string[] = 
		[
		'_b_sd_ic_',
		'_b_sd_mic_'
		];
	static mtmNum = 2;

	static htmlpage:html_page_t =
		{
		html:
			`<i id='deriveButton'><button onclick="IF_CONLANGS.deriveConlang().catch(LOG.caughtError);" class="btn"> <i class="fa fa-fast-forward"></i> Derive conlang</button></i>
			<button onclick="IF_CONLANGS.createConlang().catch(LOG.caughtError);" class="btn"> <i class="fa fa-plus"></i> New conlang</button>
			<i id='deleteButton'><button onclick="IF_CONLANGS.deleteConlang();" class="btn"> <i class="fa fa-times"></i> Delete current conlang</button></i>
			<div id='conlangOptions'>
			<br/><br/>
			<div class="clanc-row-container">
				<div id='settingFields' class="clanc-body" style="width:100%;"></div>
				<div class="clanc-body interface-frame">
					<center>Parts of Speech</center><br/>
					<div id='posmenu'></div>
					<button onclick="IF_CONLANGS.addNewPos();" class="btn conlangbtn"> <i class="fa fa-plus fa-sm"></i> New part of speech</button>
					<button onclick="IF_CONLANGS.deleteSelectedPos();" class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i> Delete selected part of speech</button>
					<br/>
					<div id='possettings'></div>
				</div>
				<div class="clanc-body interface-frame">
					<center>Inflection classes</center><br/>
					<div id='icmenu'></div>
					<button onclick="IF_CONLANGS.addNewIC();" class="btn conlangbtn"> <i class="fa fa-plus fa-sm"></i> New inflection class</button>
					<button onclick="IF_CONLANGS.deleteSelectedIC();" class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i> Delete selected inflection class</button>
					<br/>
					<div id='icsettings'></div>
				</div>
				<div class="clanc-body interface-frame">
					<center>Categories</center><br/>
					<div id='categorymenu'></div>
					<button onclick="IF_CONLANGS.addNewCategory();" class="btn conlangbtn"> <i class="fa fa-plus fa-sm"></i> New category</button>
					<button onclick="IF_CONLANGS.deleteSelectedCategory();" class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i> Delete selected category</button>
					<br/>
					<div id='categorysettings'></div>
				</div>
				<div class="clanc-body interface-frame">
					<center>Grammemes</center><br/>
					<center id='gramcategoryheading'></center><br/>
					<div id='grammememenu'></div>
					<button onclick="IF_CONLANGS.addNewGrammeme();" class="btn conlangbtn"> <i class="fa fa-plus fa-sm"></i> New grammeme</button>
					<button onclick="IF_CONLANGS.deleteSelectedGrammeme();" class="btn conlangbtn"> <i class="fa fa-times fa-sm"></i> Delete selected grammeme</button>
					<br/>
					<div id='grammemesettings'></div>
				</div>
			</div>
			<br/>
			Segments: <br/>
			<textarea id="${INTERFACE_STATICS.pfx}segments"
				oninput="INTERFACE_STATICS.triggerPropSetting(
					this,
					'segments',
					table_e.miscdata,
					null,
					IF_CONLANGS.verifySegments);"
				rows="8" cols="100" style="margin-top:5px;"></textarea>
			<br/>
			</div>`,
		refresh: IF_CONLANGS.refresh,
		load_page: IF_CONLANGS.init
		};

	constructor()
		{
		return lo(g.error, "This object is only to be used statically.", null);
		}

	static buildSettingFields()
		{
		let res = '';
		res += INTERFACE_STATICS.buildASettingField('Name:', 'name', table_e.miscdata, 'name', null, 'IF_CONLANGS.setConlangName');
		res += INTERFACE_STATICS.buildASettingField('Spoken from:', 'spokenfrom', table_e.miscdata, 'spokenfrom');
		res += INTERFACE_STATICS.buildASettingField('Spoken to:', 'spokento', table_e.miscdata, 'spokento');
		res += INTERFACE_STATICS.buildASettingField('Spoken in:', 'place', table_e.miscdata, 'place');
		res += INTERFACE_STATICS.buildASettingField('Setting:', 'setting', table_e.miscdata, 'setting');
		UTL.getElem('settingFields')!.innerHTML = res;
		}

	static init():res_t
		{
		IF_CONLANGS.buildSettingFields();
		IF_CONLANGS.posContainer = IF_CONLANGS.createPOStabsContainer();
		IF_CONLANGS.inflectionContainer = IF_CONLANGS.createInflectionContainer();
		IF_CONLANGS.categoryContainer = IF_CONLANGS.createCategoryTabsContainer();
		IF_CONLANGS.grammemeContainer = IF_CONLANGS.createGrammemeOrderedListContainer();

		return UTL.res_success;
		}

	static refresh():res_t
		{
		UTL.getElem('possettings')!.innerHTML = '';
		UTL.getElem('categorysettings')!.innerHTML = '';
		UTL.getElem('grammemesettings')!.innerHTML = '';
		IF_CONLANGS.buildSettingFields();
		let clg = SESSION.getactiveconlang();
		if (clg && SESSION.file)
			{
			UTL.getElem('conlangOptions')!.hidden = false;
			UTL.getElem('deleteButton')!.hidden = false;
			UTL.getElem('deriveButton')!.hidden = false;
			SESSION.file.db.getdata(table_e.conlang, ['*'], 'id = '+clg.dbid).then((data)=>
				{
				UTL.getInputElem(INTERFACE_STATICS.pfx+"name")!.value=data[0].name;
				}).catch(LOG.caughtError);
			SESSION.file.db.getdata(table_e.miscdata, ['*'], 'conlang = '+clg.dbid).then((data)=>
				{
				UTL.getInputElem(INTERFACE_STATICS.pfx+"spokenfrom")!.value=data[0].spokenfrom;
				UTL.getInputElem(INTERFACE_STATICS.pfx+"spokento")!.value=data[0].spokento;
				UTL.getInputElem(INTERFACE_STATICS.pfx+"place")!.value=data[0].place;
				UTL.getInputElem(INTERFACE_STATICS.pfx+"setting")!.value=data[0].setting;
				UTL.getInputElem(INTERFACE_STATICS.pfx+"segments")!.value=data[0].segments;
				}).catch(LOG.caughtError);
			IF_CONLANGS.posContainer.refresh();
			IF_CONLANGS.inflectionContainer.refresh();
			IF_CONLANGS.categoryContainer.refresh();
			IF_CONLANGS.grammemeContainer.refresh();
			}
		else
			{
			UTL.getElem('conlangOptions')!.hidden = true;
			UTL.getElem('deleteButton')!.hidden = true;
			UTL.getElem('deriveButton')!.hidden = true;
			}
		return UTL.res_success;
		}

	static async deriveConlang()
		{
		return SESSION.deriveConlang().then(()=>
			{
			UTL.getElem('conlangOps')!.hidden = false;
			INTERFACE.refreshInterface();
			});
		}

	static async createConlang()
		{
		return SESSION.createConlang().then(()=>
			{
			UTL.getElem('conlangOps')!.hidden = false;
			INTERFACE.refreshInterface();
			});
		}

	static deleteConlang()
		{
		if (UTL.confirmdatadeletion())
			SESSION.deleteconlang(SESSION.getactiveconlang()).then(()=>
			{
			if (SESSION.getactiveconlang() == null)
				UTL.getElem('conlangOps')!.hidden = true;
			INTERFACE.refreshInterface();
			})
		.catch(()=>
			{
			INTERFACE.refreshInterface();
			LOG.caughtError();
			});
		}

	static setConlangName(prop:string, elem:HTMLInputElement)
		{
		let clg = SESSION.getactiveconlang();
		if (!clg)
			{
			INTERFACE.displayError("No conlang selected");
			return;
			}
		SESSION.file!.renameconlang(clg, elem.value.trim())
		.then((res:any)=>
			{
			INTERFACE.refreshInterface();
			}).catch((res:any)=>
			{
			INTERFACE.displayError("Conlang names should be unique and not empty.");
			INTERFACE.refreshInterface();
			});		
		}

	static verifySegments(prop:string, elem:HTMLInputElement)
		{
		//TODO: check them with the parser
		INTERFACE_STATICS.setConlangProp(prop, elem, table_e.miscdata);
		}
	
	static createPOStabsContainer(): TABS_CONTAINER
		{
		let dataFeeder = ()=>
			{
			let clg = SESSION.getactiveconlang();
			if (!clg || clg.dbid < 0)
				return PresolveT<any>([]);
			return SESSION.file!.db.getdata(table_e.word_type, ['*'], 'conlang = '+clg!.dbid);
			};

		let tabSettingsFeeder = (item:any) =>
			{
			if (!item.part_of_speech) return null;
			let ts:tabSettings =
				{
				name: item.name,
				id: item.id,
				idname: 'postab_'
				};
			return ts;
			};

		let onClickCallback = IF_CONLANGS.paradigmOnClickCallback;

		return new TABS_CONTAINER('posmenu', dataFeeder, tabSettingsFeeder, onClickCallback);
		}

	static addNewPos()
		{
		let newid = UTL.newHTMLId()+'';
		let name = 'pos '+newid;
		SESSION.file && SESSION.file.db.savemultiplerows(table_e.word_type,
			['conlang','inherent_categories', 'assignable', 'part_of_speech', 'name', 'abbreviation', 'morpheme_paradigms', 'morpheme_paradigms_order'],
			[[SESSION.getactiveconlang()!.dbid, null, true, true, name, newid, null, null],])
			// cannot add more than one [id, null] unique key, not sure if it's a bug or feature
		.then(()=>
			{
			return SESSION.file!.db.getid(table_e.word_type, `name="${name}"`);
			})
		.then((data:any)=>
			{
			let ts: tabSettings = 
				{
				name: name,
				id: data,
				idname: 'postab_',
				selected: true
				};
			IF_CONLANGS.posContainer.addTabButton(ts);
			}).catch(LOG.caughtError);
		}

	static deleteSelectedPos()
		{
		if (IF_CONLANGS.posContainer.selectedTab < 0) return;
		if (!UTL.confirmdatadeletion()) return;
		SESSION.file && SESSION.file.db.deleterowsbyid(table_e.word_type, [IF_CONLANGS.posContainer.selectedTab]).
			then(()=>
				{
				UTL.getElem('possettings')!.innerHTML = '';
				IF_CONLANGS.posContainer.selectedTab = -1;
				IF_CONLANGS.posContainer.refresh();
				}).catch(LOG.caughtError);
		}
	
	static createInflectionContainer(): TABS_CONTAINER
		{
		let dataFeeder = ()=>
			{
			let clg = SESSION.getactiveconlang();
			if (!clg || clg.dbid < 0)
				return PresolveT<any>([]);
			return SESSION.file!.db.getdata(table_e.word_type, ['*'], 'conlang = '+clg!.dbid);
			};

		let tabSettingsFeeder = (item:any) =>
			{
			if (item.part_of_speech) return null;
			let ts:tabSettings =
				{
				name: item.name,
				id: item.id,
				idname: 'ictab_'
				};
			return ts;
			};

		let onClickCallback = IF_CONLANGS.paradigmOnClickCallback;

		return new TABS_CONTAINER('icmenu', dataFeeder, tabSettingsFeeder, onClickCallback);
		}

	static addNewIC()
		{
		let newid = UTL.newHTMLId()+'';
		let name = 'class '+newid;
		SESSION.file && SESSION.file.db.getdata(table_e.word_type, ['*']).then((data)=>
			{
			let inh:any = null;
			for (let i = 0; i < data.length; i++)
				{
				if (data[i].part_of_speech)
					{
					inh = data[i].id;
					break;
					}
				}
			return SESSION.file!.db.savemultiplerows(table_e.word_type,
				['conlang', 'inherent_categories', 'assignable', 'part_of_speech', 'name', 'abbreviation', 'morpheme_paradigms', 'morpheme_paradigms_order', 'inherits_from'],
				[[SESSION.getactiveconlang()!.dbid, null, true, false, name, newid, null, null, inh],])
				// cannot add more than one [id, null] unique key, not sure if it's a bug or feature
			})
		.then(()=>
			{
			return SESSION.file!.db.getid(table_e.word_type, `name="${name}"`);
			})
		.then((data:any)=>
			{
			let ts: tabSettings = 
				{
				name: name,
				id: data,
				idname: 'ictab_',
				selected: true
				};
			IF_CONLANGS.inflectionContainer.addTabButton(ts);
			}).catch(LOG.caughtError);
		}

	static deleteSelectedIC()
		{
		if (IF_CONLANGS.inflectionContainer.selectedTab < 0) return;
		if (!UTL.confirmdatadeletion()) return;
		SESSION.file && SESSION.file.db.deleterowsbyid(table_e.word_type, [IF_CONLANGS.inflectionContainer.selectedTab]).
			then(()=>
				{
				UTL.getElem('icsettings')!.innerHTML = '';
				IF_CONLANGS.inflectionContainer.selectedTab = -1;
				IF_CONLANGS.inflectionContainer.refresh();
				}).catch(LOG.caughtError);
		}

	static paradigmOnClickCallback(htmlelem: HTMLElement, dbid:number)
		{
		let pddata:any = {};

		IF_CONLANGS.aggregateParadigmData(dbid, SESSION.getactiveconlang()!.dbid).then((data)=>
			{
			if (data.isPoS) data.frame_pfx = 'pos';
			else data.frame_pfx = 'ic';
			pddata = data;
			let html = '';
			html += INTERFACE_STATICS.buildASettingField(
				'Name:',
				'name',
				table_e.word_type,
				pddata.frame_pfx+'_name',
				dbid,
				'INTERFACE_STATICS.updateNameAndSetProp',
				[pddata.frame_pfx+'tab_'+dbid]);
			html += INTERFACE_STATICS.buildASettingField('Abbreviation:', 'abbreviation', table_e.word_type, pddata.frame_pfx+'_abbrv', dbid);

			// if (!pddata.isPoS)
			// 	html += INTERFACE_STATICS.buildASettingCheckbox('Words can be set to it:', 'assignable', table_e.word_type, pddata.frame_pfx+'assignable', dbid);

			if (pddata.isTop && pddata.isPoS)
				{
				let categoryNames = data.categoriesData.map((x:any)=>x.name);
				let categoryIds = data.categoriesData.map((x:any)=>x.id);
				categoryNames.splice(0, 0, 'add...');
				categoryIds.splice(0, 0, null);
				html += IF_CONLANGS.addCategoryDropdown(
							'Inherent categories: ',
							categoryNames,
							categoryIds,
							'wt_inherent_categories',
							table_e.word_type,
							dbid,
							pddata.frame_pfx,
							IF_CONLANGS.mtmDropdownIds[1]
							);
				}
			else
				{
				if (!pddata.isPoS)
					html += INTERFACE_STATICS.buildALabel('Part of speech: ', '', pddata.PoSname);

				let chtml = '';
				let ichtml = '';

				for (let j = 0; j < pddata.categoriesData.length; j++)
					{
					for (let i = 0; i <pddata.inherent_categories.length; i++)
						if (pddata.categoriesData[j].id == pddata.inherent_categories[i])
							ichtml += INTERFACE_STATICS.buildAButton(pddata.categoriesData[j].name);
					}

				html += INTERFACE_STATICS.buildALabel('Inherent categories: ', '', ichtml);

				let categoryIds = [];
				let categoryNames:string[] = [];

				categoryIds = data.categoriesData.map((x:any)=>x.id);
				categoryIds = categoryIds.filter((x:any) => !pddata.inherent_categories.includes(x));
				for (let i = 0; i < pddata.categoriesData.length; i++)
					if (categoryIds.includes(pddata.categoriesData[i].id))
						categoryNames.push(pddata.categoriesData[i].name);
				categoryNames.splice(0, 0, 'add...');
				categoryIds.splice(0, 0, null);

				html += IF_CONLANGS.addCategoryDropdown(
							'Additional inherent categories: ',
							categoryNames,
							categoryIds,
							'wt_inherent_categories',
							table_e.word_type,
							dbid,
							pddata.frame_pfx,
							IF_CONLANGS.mtmDropdownIds[0]
							);

				categoryNames = [];
				categoryIds = [];
				categoryIds = pddata.inherent_categories.slice();
				for (let i = 0; i < pddata.categoriesData.length; i++)
					if (categoryIds.includes(pddata.categoriesData[i].id))
						categoryNames.push(pddata.categoriesData[i].name);
				categoryNames.splice(0, 0, 'add...');
				categoryIds.splice(0, 0, null);

				html += IF_CONLANGS.addCategoryDropdown(
							'Missing inherent categories: ',
							categoryNames,
							categoryIds,
							'wt_missing_inherent_categories',
							table_e.word_type,
							dbid,
							pddata.frame_pfx,
							IF_CONLANGS.mtmDropdownIds[1]
							);
				}

			UTL.getElem(pddata.frame_pfx+'settings')!.innerHTML = html;
			return SESSION.file!.db.getdata(table_e.word_type, ['*'], 'conlang = '+SESSION.getactiveconlang()!.dbid);
			})
		.then((data)=>
			{
			if (pddata.isTop && !pddata.isPoS)
				{
				let names:string[] = [];
				let ids:number[] = [];
				for (let i = 0; i < data.length; i++)
					if (data[i].part_of_speech == true)
						{
						names.push(data[i].name);
						ids.push(data[i].id);
						}
				names.splice(0, 0, 'set...');
				ids.splice(0, 0, -1);
				let html = INTERFACE_STATICS.buildASettingDropdown(
				'Part of speech: ',
				names,
				ids,
				'IF_CONLANGS.addParadigmInheritance('+dbid+', this.value, this);'+
				'document.getElementById(\''+pddata.frame_pfx+'tab_'+dbid+'\').click();',
				'',
				pddata.frame_pfx + 'inherit_dropdownPOS',
				false);
				UTL.getElem(pddata.frame_pfx+'settings')!.innerHTML += html;
				}

			let names:string[] = [];
			let ids:number[] = [];
			for (let i = 0; i < data.length; i++)
				if (data[i].part_of_speech == pddata.isPoS)
					{
					names.push(data[i].name);
					ids.push(data[i].id);
					}
			names.splice(0, 0, '(nothing)');
			ids.splice(0, 0, -1);

			let html = INTERFACE_STATICS.buildASettingDropdown(
				'Inherits from: ',
				names,
				ids,
				'IF_CONLANGS.addParadigmInheritance('+dbid+', this.value, this);'+
				'document.getElementById(\''+pddata.frame_pfx+'tab_'+dbid+'\').click();',
				'',
				pddata.frame_pfx + 'inherit_dropdown',
				true);
			UTL.getElem(pddata.frame_pfx+'settings')!.innerHTML += html;
			return SESSION.file!.db.getdata(table_e.word_type, ['*'], 'id = '+dbid);
			})
		.then((data)=>
			{
			let assignable = UTL.getInputElem(pddata.frame_pfx+"assignable", 3, true);
			if (assignable) assignable.checked=data[0].assignable;
			UTL.getInputElem(INTERFACE_STATICS.pfx+pddata.frame_pfx+"_name")!.value=data[0].name;
			UTL.getInputElem(INTERFACE_STATICS.pfx+pddata.frame_pfx+"_abbrv")!.value=data[0].abbreviation || '';
			if (data[0].inherits_from)
				{
				let ddl = <HTMLSelectElement>UTL.getElem(pddata.frame_pfx + 'inherit_dropdown');
				if (ddl) for (let i = 0; i < ddl.options.length; i++)
						if (data[0].inherits_from == ddl.options[i].value)
							{
							ddl.selectedIndex = i;
							let poselem = UTL.getElem(pddata.frame_pfx + 'inherit_dropdownPOS', 2, true);
							if (poselem)
								poselem.outerHTML = ''; //already inherits from something, so that something
								//would have determined the part of speech
							break;
							}
				}
			let promises = [];
			for (let i = 0; i < IF_CONLANGS.mtmNum; i++)
				promises.push(SESSION.file!.db.MTMgetids(DBMTMs[IF_CONLANGS.mtmTables[i]], table_e.word_type, dbid));
			return Promise.all(promises);
			})
		.then((data)=>
			{
			let ids:number[][] = [];
			for (let i = 0; i < IF_CONLANGS.mtmNum; i++)
				ids[i] = Object.values(data[i]).map((x:any)=>x.category);
			let promises = [];
			for (let i = 0; i < IF_CONLANGS.mtmNum; i++)
				promises.push(SESSION.file!.db.getdata(table_e.category, ['*'], 'id IN ('+ids[i].join(', ')+')'));
			return Promise.all(promises);
			})
		.then((dataArr)=>
			{
			for (let j = 0; j < IF_CONLANGS.mtmNum; j++)
				{
				let data = dataArr[j];
				let html = '';
				for (let i = 0; i < data.length; i++)
					{
					html += '</br>'+INTERFACE_STATICS.buildASelfDestructButton(
							data[i].name,
							IF_CONLANGS.mtmButtonIds[j]+data[i].id,
							'SESSION.file.db.MTMdeleterows(' +
							JSON.stringify(DBMTMs[IF_CONLANGS.mtmTables[j]]).replace(/(\")/g, "'") + ', ' +
							'\'word_type = ' + dbid +
							' AND category = ' + data[i].id + '\')');
					}
				let elem = UTL.getElem(pddata.frame_pfx+IF_CONLANGS.mtmDropdownIds[j], 2, true);
				elem && (elem.innerHTML += html);
				}
			}).catch(LOG.caughtError);
		}

	static addCategoryDropdown(
			label:string,
			options:string[],
			optionIds:number[],
			mtmTable:string,
			mtm_column:table_e,
			rowId:number,
			prefix:string,
			dropdownid:string,
			click:boolean|string = true
			):string
		{
		let add='';
		if (click===true) add = 'document.getElementById(\''+prefix+'tab_'+rowId+'\').click();';
		else if (click !== false) add = click;
		return INTERFACE_STATICS.buildASettingDropdown(
			label,
			options,
			optionIds,
			'SESSION.file.db.MTMaddOTM(' +
				JSON.stringify(DBMTMs[mtmTable]).replace(/(\")/g, "'") + ', ' +
				"'" + mtm_column + '\', ' +
				rowId +
				', [this.value])' +
				'.catch(()=>{INTERFACE.displayInfo(\'This category is already added.\');});' +
				add,
			prefix+dropdownid);
		}

	static async addParadigmInheritance(inheritee:number, inheritFrom:number, dropdown:HTMLSelectElement)
		{
		inheritFrom = Number(inheritFrom);
		if (isNaN(inheritee) || isNaN(inheritFrom))
			return INTERFACE.displayError('Wrong data input to verify inheritance for.');
		if (inheritee == inheritFrom)
			return INTERFACE.displayInfo('An element cannot inherit from itself.');
		
		lo(g.debug, '  inh from  '+inheritFrom);
		
		if (inheritFrom < 0 || Number(inheritFrom) < 0)
		// for some reason ids.splice(0, 0, -1); adds "-1" and not -1....
			return SESSION.file!.db.updatedata(table_e.word_type, {'inherits_from':null}, 'id = '+inheritee);

		let currentTop = inheritFrom;
		while (true)
			{
			let data = (await SESSION.file!.db.getdata(table_e.word_type, ['*'], 'id = '+currentTop))[0];
			if (data.inherits_from == null) break;
			if (inheritee == data.inherits_from)
				{
				dropdown.selectedIndex = 0;
				return INTERFACE.displayInfo('This action would result in circular dependency.');
				}
			currentTop = data.inherits_from;
			}
		
		return SESSION.file!.db.updatedata(table_e.word_type, {'inherits_from':inheritFrom}, 'id = '+inheritee);
		}
	
	static async aggregateParadigmData(paradigm:number, conlang:number):Promise<any>
		{
		let isPoS: boolean = false;
		let PoS: number = -1;
		let PoSlist: number[] = [];
		let PoSname: string = '';
		let isTop: boolean = true;
		let inherent_categories: number[] = [];
		let currentTop = paradigm;
		let numInflections = 0;
		while (currentTop != null)
			{
			let data = (await SESSION.file!.db.getdata(table_e.word_type, ['*'], 'id = '+currentTop))[0];
			if (currentTop == paradigm)
				isPoS = <boolean>data.part_of_speech;
			if (data.part_of_speech)
				{
				PoS = currentTop;
				PoSname = data.name;
				PoSlist.push(PoS);
				}
			if (isPoS == false)
				numInflections++;

			let icdata = await SESSION.file!.db.MTMgetids(DBMTMs['wt_inherent_categories'], table_e.word_type, currentTop);

			icdata = icdata.map((x:any)=>x.category);

			inherent_categories = [...new Set([...inherent_categories, ...icdata])];

			let previousTop = currentTop;
			currentTop = data.inherits_from;
			if (currentTop == paradigm) break; // this should never happen
			if (currentTop != null)
				{
				if (numInflections != 1) // isTop signifiies if the inquired about paradigm is either the top pos OR the top inflection
					isTop = false;
				let micdata = await SESSION.file!.db.MTMgetids(DBMTMs['wt_missing_inherent_categories'], table_e.word_type, previousTop);
				micdata = micdata.map((x:any)=>x.category);
				inherent_categories = inherent_categories.filter(x => !micdata.includes(x));
				}
			}

		let data = await SESSION.file!.db.getdata(table_e.category, ['*'], 'conlang = '+conlang);

		return PresolveT<any>(
			{
			isPoS: isPoS,
			PoS: PoS,
			PoSlist: PoSlist,
			PoSname: PoSname,
			isTop: isTop,
			inherent_categories: inherent_categories,
			categoriesData: data
			});
		}

	static createCategoryTabsContainer(): TABS_CONTAINER
		{
		let dataFeeder = ()=>
			{
			let clg = SESSION.getactiveconlang();
			if (!clg || clg.dbid < 0)
				return PresolveT<any>([]);
			return SESSION.file!.db.getdata(table_e.category, ['*'], 'conlang = '+clg!.dbid);
			};

		let tabSettingsFeeder = (item:any) =>
			{
			let ts:tabSettings =
				{
				name: item.name,
				id: item.id,
				idname: 'categorytab_'
				};
			return ts;
			};

		let onClickCallback = (htmlelem: HTMLElement, dbid:number) =>
			{
			let html = '';
			html += INTERFACE_STATICS.buildASettingField(
				'Name:',
				'name',
				table_e.category,
				'category_name',
				dbid,
				'INTERFACE_STATICS.updateNameAndSetProp',
					[
					'categorytab_'+dbid,
					'gramcategoryheading',
					IF_CONLANGS.mtmButtonIds[0]+dbid,
					IF_CONLANGS.mtmButtonIds[1]+dbid
					]);
			
			// this feature will be done at best sometime later
			// html += INTERFACE_STATICS.buildASettingCheckbox('Inflections and words can lack it:', 'nullable', table_e.category, 'nullable', dbid);

			UTL.getElem('categorysettings')!.innerHTML = html;
			UTL.getElem('grammemesettings')!.innerHTML = '';
			IF_CONLANGS.grammemeContainer.selectedItem = -1;
			IF_CONLANGS.grammemeContainer.refresh();

			SESSION.file!.db.getdata(table_e.category, ['*'], 'id = '+dbid).then((data)=>
				{
				UTL.getInputElem(INTERFACE_STATICS.pfx+"category_name")!.value=data[0].name;
				// this feature will be done at best sometime later
				// UTL.getInputElem(INTERFACE_STATICS.pfx+"nullable")!.checked=data[0].nullable;
				}).catch(LOG.caughtError);
			};

		return new TABS_CONTAINER('categorymenu', dataFeeder, tabSettingsFeeder, onClickCallback);
		}

	static addNewCategory()
		{
		let name = 'category '+UTL.newHTMLId();
		SESSION.file && SESSION.file.db.savemultiplerows(table_e.category,
			['conlang', 'nullable', 'name'],
			[[SESSION.getactiveconlang()!.dbid, false, name]])
		.then(()=>
			{
			return SESSION.file!.db.getid(table_e.category, `name="${name}"`);
			})
		.then((data:any)=>
			{
			let ts: tabSettings = 
				{
				name: name,
				id: data,
				idname: 'categorytab_',
				selected: true
				};
			IF_CONLANGS.categoryContainer.addTabButton(ts);
			IF_CONLANGS.posContainer.refresh();
			IF_CONLANGS.inflectionContainer.refresh();
			}).catch(LOG.caughtError);
		}

	static deleteSelectedCategory()
		{
		if (IF_CONLANGS.categoryContainer.selectedTab < 0) return;
		if (!UTL.confirmdatadeletion()) return;
		SESSION.file && SESSION.file!.db.MTMgetwhere(
				DBMTMs['slot_categories'],
				'category = ' + IF_CONLANGS.categoryContainer.selectedTab).then(async (data) =>
			{
			if (data && data.length)
				for (let i in data)
					await IF_MORPHOLOGY.onCategoryDestroyCallback(null, data[i].id, data[i].slot, false);
			return SESSION.file!.db.deleterowsbyid(
				table_e.category,
				[IF_CONLANGS.categoryContainer.selectedTab]);
			}).then(()=>
				{
				UTL.getElem('categorysettings')!.innerHTML = '';
				IF_CONLANGS.categoryContainer.selectedTab = -1;
				IF_CONLANGS.resetGrammemeFrame();
				IF_CONLANGS.categoryContainer.refresh();
				IF_CONLANGS.posContainer.refresh();
				IF_CONLANGS.inflectionContainer.refresh();
				}).catch(LOG.caughtError);
		}
	
	static createGrammemeOrderedListContainer(): ORDERED_LIST_CONTAINER
		{
		let dataFeeder = ()=>
			{
			let clg = SESSION.getactiveconlang();
			if (!clg || clg.dbid < 0 || IF_CONLANGS.categoryContainer.selectedTab < 0)
				return PresolveT<any>([]);
			let newVal = '';
			if (UTL.getElem('categorytab_'+IF_CONLANGS.categoryContainer.selectedTab))
				newVal = UTL.getElem('categorytab_'+IF_CONLANGS.categoryContainer.selectedTab)!.innerHTML;
			UTL.getElem('gramcategoryheading')!.innerHTML = newVal;
			return SESSION.file!.db.getdata(
				table_e.grammeme,
				['*'],
				'category = '+IF_CONLANGS.categoryContainer.selectedTab);
			};

		let orderedListSettingsFeeder = (item:any) =>
			{
			let ts:orderedListSettings =
				{
				name: item.name,
				id: item.id,
				idname: 'gramtab_',
				position: item.position
				};
			return ts;
			};

		let onClickCallback = (htmlelem: HTMLElement, dbid:number) =>
			{
			let html = '';
			html += INTERFACE_STATICS.buildASettingField(
				'Name:',
				'name',
				table_e.grammeme,
				'gram_name',
				dbid,
				'INTERFACE_STATICS.updateNameAndSetProp',
				['gramtab_'+dbid]);
			html += INTERFACE_STATICS.buildASettingField('Gloss:', 'gloss', table_e.grammeme, 'gloss', dbid);
			UTL.getElem('grammemesettings')!.innerHTML = html;

			SESSION.file!.db.getdata(table_e.grammeme, ['*'], 'id = '+dbid).then((data)=>
				{
				UTL.getInputElem(INTERFACE_STATICS.pfx+"gram_name")!.value=data[0].name;
				UTL.getInputElem(INTERFACE_STATICS.pfx+"gloss")!.value=data[0].gloss;
				}).catch(LOG.caughtError);
			};

		let onReorderCallback = (htmlelem: HTMLElement, moveUpEl: number, moveDownEl: number) =>
			{
			SESSION.file!.db.getdata(table_e.grammeme, ['*'], 'id = '+moveUpEl).then(async (data)=>
				{
				await SESSION.file!.db.updatedata(
					table_e.grammeme,
					{'position':data[0].position-1},
					'id = '+data[0].id);
				return SESSION.file!.db.getdata(table_e.grammeme, ['*'], 'id = '+moveDownEl);
				}).then(async (data)=>
				{
				await SESSION.file!.db.updatedata(
					table_e.grammeme,
					{'position':data[0].position+1},
					'id = '+data[0].id);
				IF_CONLANGS.grammemeContainer.refresh();
				}).catch(LOG.caughtError);
			};

		return new ORDERED_LIST_CONTAINER(
				'grammememenu',
				dataFeeder,
				orderedListSettingsFeeder,
				onClickCallback,
				onReorderCallback);
		}

	static resetGrammemeFrame()
		{
		UTL.getElem('grammemesettings')!.innerHTML = '';
		UTL.getElem('grammememenu')!.innerHTML = '';
		UTL.getElem('gramcategoryheading')!.innerHTML = '';
		IF_CONLANGS.grammemeContainer.selectedItem = -1;
		}

	static addNewGrammeme()
		{
		if (IF_CONLANGS.categoryContainer.selectedTab < 0)
			{
			INTERFACE.displayInfo("Select a category first.");
			return;
			}
		let newid = UTL.newHTMLId();
		let name = 'grammeme '+newid;
		let gloss = 'G'+newid;
		let position = 0;

		SESSION.file && SESSION.file!.db.getdata(
				table_e.grammeme,
				['*'],
				'category = '+IF_CONLANGS.categoryContainer.selectedTab).then((data:any)=>
			{
			for (let g in data)
				position = Math.max(position, data[g].position);
			position++;
			return SESSION.file!.db.savemultiplerows(table_e.grammeme,
				['category', 'name', 'gloss', 'position'],
				[
					[
					IF_CONLANGS.categoryContainer.selectedTab,
					name,
					gloss,
					position]
				])
			})
		.then(()=>
			{
			return SESSION.file!.db.MTMgetwhere(
					DBMTMs['slot_categories'],
					'category = ' + IF_CONLANGS.categoryContainer.selectedTab);
			})
		.then(async (data)=>
			{
			if (data)
				{
				let id = await SESSION.file!.db.getid(
						table_e.grammeme,
						'category = '+IF_CONLANGS.categoryContainer.selectedTab + 
						' AND gloss = \''+gloss+'\'');
				for (let i in data)
					MORPHOLOGY.updateSlotCategoryWithANewGrammeme(
						data[i].slot,
						IF_CONLANGS.categoryContainer.selectedTab,
						id);
				}
			})
		.then(()=>
			{
			IF_CONLANGS.grammemeContainer.refresh();
			}).catch(LOG.caughtError);
		}

	static async deleteSelectedGrammeme()
		{
		if (IF_CONLANGS.categoryContainer.selectedTab < 0) return;
		if (IF_CONLANGS.grammemeContainer.selectedItem < 0) return;
		if (!UTL.confirmdatadeletion()) return;
		let position = -1;

		SESSION.file && SESSION.file!.db.getdata(
				table_e.grammeme,
				['*'],
				'id = '+IF_CONLANGS.grammemeContainer.selectedItem).then((data:any)=>
			{
			position = data[0].position;
			return SESSION.file!.db.getdata(
				table_e.grammeme,
				['*'],
				'position > '+position +
				' AND category = '+IF_CONLANGS.categoryContainer.selectedTab);
			}).then(async (data)=>
			{
			for (let g in data)
				await SESSION.file!.db.updatedata(
					table_e.grammeme,
					{'position':data[g].position-1},
					'id = '+data[g].id);
			let res = await SESSION.file!.db.MTMgetwhere(
					DBMTMs['m_grammemes'],
					'grammeme = ' + IF_CONLANGS.grammemeContainer.selectedItem);
			await SESSION.file!.db.deleterowsbyid(table_e.grammeme, [IF_CONLANGS.grammemeContainer.selectedItem]);
			return res;
			}).then((data)=>
			{
			if (data && data.length)
				{
				data = data.map(x=>x.morpheme);
				return SESSION.file!.db.deleterowsbyid(table_e.morpheme, data);
				}
			}).then(()=>
			{
			UTL.getElem('grammemesettings')!.innerHTML = '';
			IF_CONLANGS.grammemeContainer.selectedItem = -1;
			IF_CONLANGS.grammemeContainer.refresh();
			}).catch(LOG.caughtError);
		}
	}