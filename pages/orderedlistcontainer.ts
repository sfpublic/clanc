/*
    CLanC - Conlang Constructor
    Copyright (C) 2021 Zju @ ZBB (http://www.verduria.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

interface orderedListSettings
	{
	name:string;
	selected?:boolean;
	id:number;
	idname:string;
	position:number;
	}

class ORDERED_LIST_CONTAINER
	{
	selectedItem:number = -1;
	elemid:string;
	dataFeeder:()=>Promise<any>;
	orderedListSettingsFeeder:((item:any)=>orderedListSettings|null)|null;
	asyncOrderedListSettingsFeeder:((item:any)=>Promise<orderedListSettings|null>)|null;
	onClickCallback:(htmlelem: HTMLElement, dbid:number)=>any;
	onReorderCallback:(htmlelem: HTMLElement, moveUpEl: number, moveDownEl: number)=>any;
	onDestroyCallback:undefined|((htmlelem: HTMLElement, elem: number)=>any);
	static all_ordered_list_containers:{[key:string]:ORDERED_LIST_CONTAINER} = {};

	constructor(
			elemid:string,
			dataFeeder:()=>Promise<any>,
			orderedListSettingsFeeder:((item:any)=>orderedListSettings|null)|null,
			onClickCallback:(htmlelem: HTMLElement, dbid:number)=>any,
			onReorderCallback:(htmlelem: HTMLElement, moveUpEl: number, moveDownEl: number)=>any,
			onDestroyCallback?:(htmlelem: HTMLElement, elem: number)=>any,
			asyncOrderedListSettingsFeeder:((item:any)=>Promise<orderedListSettings|null>)|null = null)
		{
		this.elemid = elemid;
		this.onDestroyCallback = onDestroyCallback;
		this.dataFeeder = dataFeeder;
		this.orderedListSettingsFeeder = orderedListSettingsFeeder;
		this.onClickCallback = onClickCallback;
		this.asyncOrderedListSettingsFeeder = asyncOrderedListSettingsFeeder;
		this.onReorderCallback = onReorderCallback;
		if (!this.asyncOrderedListSettingsFeeder && !this.orderedListSettingsFeeder)
			INTERFACE.displayError("Internal error: neither sync nor async ordered list settings feeder specified for "+elemid);
		ORDERED_LIST_CONTAINER.all_ordered_list_containers[elemid] = this;
		if (this.orderedListSettingsFeeder)
			this.refresh();
		else
			this.asyncRefresh();
		}

	refresh()
		{
		this.dataFeeder().then((data)=>
			{
			let pos:orderedListSettings[] = [];
			let dl = data.length;
			for (let i = 0; i < dl; i++)
				{
				let ts:any;
				ts = this.orderedListSettingsFeeder!(data[i]);
				if (!ts) continue;
				if (this.selectedItem == ts.id)
					ts.selected = true;
				pos.push(ts);
				}
			UTL.getElem(this.elemid)!.innerHTML = this.buildOrderedListMenu(pos);
			}).catch(LOG.caughtError);
		}

	async asyncRefresh()
		{
		return this.dataFeeder().then(async (data)=>
			{
			let pos:orderedListSettings[] = [];
			let dl = data.length;
			for (let i = 0; i < dl; i++)
				{
				let ts:any;
				ts = await this.asyncOrderedListSettingsFeeder!(data[i]);
				if (!ts) continue;
				if (this.selectedItem == ts.id)
					ts.selected = true;
				pos.push(ts);
				}
			UTL.getElem(this.elemid)!.innerHTML = this.buildOrderedListMenu(pos);
			});
		}

	private elementHtml(
			settings:orderedListSettings,
			canBeMovedDown:boolean,
			canBeMovedUp:boolean,
			pos:number,
			prev:number,
			next:number)
		{
		let selected = (settings.selected && 'selected') || '';
		let id = (settings.id && settings.idname && 'id="'+settings.idname!+settings.id!+'"') || '';
		let moveUpHtml = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		let moveDownHtml = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		let sdHtml = '';
		let sdHtmlWrapper = '';

		if (this.onDestroyCallback)
			{
			sdHtml = 
					`<i id="_self_destruct_btn_${this.elemid}_${pos}">
				<button class="btn conlangbtn"
					onclick=
						"{
						document.getElementById('_self_destruct_btn_${this.elemid}_${pos}').outerHTML = '';
						ORDERED_LIST_CONTAINER.onDestroyReroute(
									'${this.elemid}',
									this,
									${settings.id!});
						}"
					><i class="fa fa-times fa-sm"></i></button>`;
			
			sdHtmlWrapper = '</i>';
			}

		if (canBeMovedUp)
			moveUpHtml = `
				<button
					onclick="ORDERED_LIST_CONTAINER.onReorderReroute(
								'${this.elemid}',
								this,
								${settings.id!},
								${prev});"
					class="btn conlangbtn">
					<i class="fa fa-chevron-up fa-sm"></i>
				</button>`;

		if (canBeMovedDown)
			moveDownHtml = `
				<button
					onclick="ORDERED_LIST_CONTAINER.onReorderReroute(
								'${this.elemid}',
								this,
								${next},
								${settings.id!});"
					class="btn conlangbtn">
					<i class="fa fa-chevron-down fa-sm"></i>
				</button>`;

		return `
			${sdHtml}
			${pos}.&nbsp;
			${moveUpHtml}
			${moveDownHtml}
			<button
				onclick="ORDERED_LIST_CONTAINER.onClickReroute('${this.elemid}', this, ${settings.id!});"
				class="btn conlangbtn ${selected} ${this.elemid}_class_sel"
				${id}
				>
				${settings.name}
			</button></br>
			${sdHtmlWrapper}`;
		}

	buildOrderedListMenu(settings:orderedListSettings[])
		{
		let html = '';
		settings.sort((a:orderedListSettings, b:orderedListSettings)=>{return a.position - b.position});
		let settingsLength = settings.length;
		for (let i = 0; i < settingsLength; i++)
			html += this.elementHtml(
					settings[i],
					(i < settingsLength - 1),
					(i > 0),
					i + 1,
					(settings[i-1] && settings[i-1].id) || -1,
					(settings[i+1] && settings[i+1].id) || -1);
		return html;
		}

	static onClickReroute(elemid:string, htmlelem: HTMLElement, dbid:number)
		{
		let tc:ORDERED_LIST_CONTAINER = ORDERED_LIST_CONTAINER.all_ordered_list_containers[elemid];
		if (!tc)
			{
			lo(g.error, 'No ordered list container with an elem id of '+elemid+' found');
			return;
			}
		tc.onClick(htmlelem, dbid);
		}

	static onReorderReroute(elemid:string, htmlelem: HTMLElement, moveUp:number, moveDown:number)
		{
		let tc:ORDERED_LIST_CONTAINER = ORDERED_LIST_CONTAINER.all_ordered_list_containers[elemid];
		if (!tc)
			{
			lo(g.error, 'No ordered list container with an elem id of '+elemid+' found');
			return;
			}
		tc.onReorderCallback(htmlelem, moveUp, moveDown);
		}

	static onDestroyReroute(elemid:string, htmlelem: HTMLElement, dbid:number)
		{
		let tc:ORDERED_LIST_CONTAINER = ORDERED_LIST_CONTAINER.all_ordered_list_containers[elemid];
		if (!tc)
			{
			lo(g.error, 'No ordered list container with an elem id of '+elemid+' found');
			return;
			}
		tc.onDestroyCallback && tc.onDestroyCallback(htmlelem, dbid);
		}

	onClick(htmlelem: HTMLElement, dbid:number)
		{
		this.selectedItem = dbid;
		let elements = document.getElementsByClassName(this.elemid+'_class_sel');
		for (let i = 0; i < elements.length; i++)
			elements[i].classList.remove('selected');
		htmlelem.classList.add('selected');

		this.onClickCallback(htmlelem, dbid);
		}
	}